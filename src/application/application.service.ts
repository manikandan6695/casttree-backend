import { CurrencyService } from "./../shared/currency/currency.service";
import { ItemDocumentService } from "./../item-document/item-document.service";
import { ApplicationDTO, ApplicationPaymentDTO } from "./dto/application.dto";
import { HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { IApplicationModel } from "./schema/application.schema";
import { UserToken } from "src/user/dto/usertoken.dto";
import { AppException } from "src/shared/app-exception";
import { PaymentService } from "src/service-provider/payment.service";
import { ICouponModel } from "src/coupon/schema/coupon.schema";
import { IProjectModel } from "src/project/schema/project.schema";
import { InvoiceService } from "src/invoice/invoice.service";
import { PaymentRequestService } from "src/payment-request/payment-request.service";
import { ICouponUsageModel } from "src/coupon/schema/coupon-usage.schema";
import { NominationsService } from "src/nominations/nominations.service";

@Injectable()
export class ApplicationService {
  constructor(
    @InjectModel("application")
    private readonly applicationModel: Model<IApplicationModel>,
    @InjectModel("couponUsage")
    private readonly couponUsageModel: Model<ICouponUsageModel>,
    @InjectModel("coupon")
    private readonly couponModel: Model<ICouponModel>,
    @InjectModel("project")
    private readonly projectModel: Model<IProjectModel>,
    private currency_service: CurrencyService,
    private paymentService: PaymentService,
    private paymentRequestService: PaymentRequestService,
    private invoiceService: InvoiceService,
    private itemDocumentService: ItemDocumentService,
    private nominationService: NominationsService,
    private shared_service: SharedService
  ) {}

  async saveApplication(body: ApplicationDTO, token: any) {
    try {
      let fv = {
        ...body,
        submittedBy: token.id,
        createdBy: token.id,
        updatedBy: token.id,
      };
      let data;
      if (body.application_id)
        data = await this.applicationModel.updateOne(
          { _id: body.application_id },
          { $set: fv }
        );
      else data = await this.applicationModel.create(fv);
      return { data };
    } catch (err) {
      throw err;
    }
  }

  async getApplication(project_id: string, token: any) {
    try {
      let data = await this.applicationModel
        .findOne({ projectId: project_id, createdBy: token.id })
        .populate("projectId")
        .populate("awards.awardId")
        .populate({
          path: "awards.awardId",
          model: "awards",
          populate: [
            {
              path: "media.media_id",
              model: "media",
              select: { media_url: 1, location: 1 },
            },
          ],
        })
        .lean();
      console.log("data is", data);

      if (!data) {
        throw new AppException("Application not found", HttpStatus.FORBIDDEN);
      } else {
        const nominations =
          await this.nominationService.getNominationsByApplication(data._id);
        data["nominations"] = nominations;
      }

      return { data };
    } catch (err) {
      throw err;
    }
  }

  async applicationPayment(body: ApplicationPaymentDTO, token: UserToken) {
    try {
      let application = await this.applicationModel
        .findOne({
          _id: body.application_id,
        })
        .populate("awards.awardId");
      await this.validateAmount(application, body.totalAmount);
      let currency = await this.currency_service.getSingleCurrency(
        "6091525bf2d365fa107635e2"
      );
      let order_detail = await this.paymentService.createPGOrder(
        body.userId.toString(),
        currency,
        body.totalAmount,
        body.application_id.toString()
      );
      let invoice = await this.invoiceService.createInvoice(
        {
          source_id: body.application_id,
          source_type: "application",
          item_count: application.awards.length,
          sub_total: body.amount,
          discount: body.discountAmount,
          document_status: "Pending",
          grand_total: body.totalAmount,
        },
        token
      );
      let items = [];
      application.awards.forEach((e) => {
        var obj = {};
        obj["source_id"] = invoice._id;
        obj["source_type"] = "invoice";
        obj["item_id"] = e.awardId;
        obj["amount"] = e.awardId["price"];
        obj["quantity"] = e.count;
        items.push(obj);
      });
      await this.itemDocumentService.createItemDocuments(items);
      let paymentRequest =
        await this.paymentRequestService.createPaymentRequest(
          {
            source_id: body.application_id,
            source_type: "application",
            amount: body.totalAmount,
            transaction_type: "OUT",
            document_status: "Pending",
          },
          token
        );
      await this.applicationModel.updateOne(
        { _id: body.application_id },
        {
          $set: {
            payment_order_id: order_detail?.order_id,
            invoice_id: invoice._id,
            payment_id: paymentRequest._id,
          },
        }
      );
      return {
        order: order_detail?.order_id,
        pg_type: order_detail?.pg_type,
        pg_meta: order_detail?.pg_meta,
      };
    } catch (err) {
      throw err;
    }
  }

  async validateAmount(application, amount) {
    try {
      let totalAmount = application.awards.reduce(
        (accum, curr) => accum + curr.awardId.price * curr.count,
        0
      );
      if (application.coupon) {
        let couponData = await this.couponModel.findOne({
          _id: application.coupon,
        });
        totalAmount -= parseInt(couponData.discountValue);
      }
      if (amount != totalAmount) {
        throw new AppException("Enter valid amount", HttpStatus.NOT_ACCEPTABLE);
      }
      return totalAmount;
    } catch (err) {
      throw err;
    }
  }

  async paymentSuccess(body, token) {
    try {
      let payment_fv = {
        order_id: body.order_detail?.order_id,
        razorpay_order_id: body.razorpay_order_id,
        razorpay_payment_id: body.razorpay_payment_id,
        razorpay_signature: body.razorpay_signature,
      };
      let is_valid_payment =
        await this.paymentService.validatePayment(payment_fv);
      if (!is_valid_payment)
        throw new AppException(
          "Payment not matched",
          HttpStatus.NOT_ACCEPTABLE
        );

      let application = await this.applicationModel.findOne({
        _id: body.application_id,
      });

      if (application.coupon) {
        await this.couponUsageModel.create({
          sourceId: application._id,
          sourceType: "application",
          userId: token.id,
          couponId: application.coupon,
          redeemedAt: new Date().toISOString(),
          appliedBy: token.id,
        });
      }
      await this.nominationService.addNominations(body, token);
      await this.applicationModel.updateOne(
        { _id: body.application_id },
        { $set: { appStatus: "Submitted" } }
      );
      await this.projectModel.updateOne(
        { _id: application.projectId },
        { $set: { documentStatus: "Completed" } }
      );
      await this.invoiceService.updateInvoice(
        application.invoice_id,
        "Completed"
      );
      await this.paymentRequestService.updatePaymentRequest(
        application.payment_id,
        "Completed"
      );
      return { message: "Your payment is success" };
    } catch (err) {
      throw err;
    }
  }
}
