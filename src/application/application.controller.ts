import { Response } from "express";
import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { ApplicationService } from "./application.service";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { UserToken } from "src/user/dto/usertoken.dto";
import {
  ApplicationDTO,
  ApplicationPaymentDTO,
  ApplicationSuccessDTO,
} from "./dto/application.dto";

@Controller("application")
export class ApplicationController {
  constructor(
    private readonly applicationService: ApplicationService,
    private sservice: SharedService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post("save-application")
  async saveApplication(
    @GetToken() token: UserToken,
    @Body(new ValidationPipe({ whitelist: true })) body: ApplicationDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.applicationService.saveApplication(body, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get("get-application/:project_id")
  async getApplication(
    @GetToken() token: UserToken,
    @Param("project_id") project_id: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.applicationService.getApplication(
        project_id,
        token
      );
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Patch("payment")
  async applicationPayment(
    @GetToken() token: UserToken,
    @Body(new ValidationPipe({ whitelist: true })) body: ApplicationPaymentDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.applicationService.applicationPayment(body, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
  @UseGuards(JwtAuthGuard)
  @Post("success")
  async paymentSuccess(
    @GetToken() token: UserToken,
    @Body(new ValidationPipe({ whitelist: true })) body: ApplicationSuccessDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.applicationService.paymentSuccess(body, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
