import { NominationsModule } from "./../nominations/nominations.module";
import { MongooseModule } from "@nestjs/mongoose";
import { Module } from "@nestjs/common";
import { ApplicationController } from "./application.controller";
import { ApplicationService } from "./application.service";
import { SharedModule } from "src/shared/shared.module";
import { AuthModule } from "src/auth/auth.module";
import { ApplicationSchema } from "./schema/application.schema";
import { PaymentService } from "src/service-provider/payment.service";
import { UserModule } from "src/user/user.module";
import { InvoiceModule } from "src/invoice/invoice.module";
import { PaymentRequestModule } from "src/payment-request/payment-request.module";
import { couponSchema } from "src/coupon/schema/coupon.schema";
import { projectsSchema } from "src/project/schema/project.schema";
import { ItemDocumentService } from "src/item-document/item-document.service";
import { ItemDocumentSchema } from "src/item-document/item-document.schema";
import { couponUsageSchema } from "src/coupon/schema/coupon-usage.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "application", schema: ApplicationSchema },
      { name: "coupon", schema: couponSchema },
      { name: "couponUsage", schema: couponUsageSchema },
      { name: "project", schema: projectsSchema },
      { name: "itemDocument", schema: ItemDocumentSchema },
    ]),
    SharedModule,
    AuthModule,
    UserModule,
    InvoiceModule,
    PaymentRequestModule,
    NominationsModule,
  ],
  controllers: [ApplicationController],
  providers: [ApplicationService, PaymentService, ItemDocumentService],
  exports: [ApplicationService],
})
export class ApplicationModule {}
