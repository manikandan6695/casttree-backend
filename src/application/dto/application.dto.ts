import { Type } from "class-transformer";
import {
  IsArray,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
import { NomineesDTO } from "src/nominations/dto/nominations.dto";

export class AwardsDTO {
  @IsNotEmpty()
  @IsMongoId()
  awardId: string;

  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @IsNumber()
  count: number;
}
export class ApplicationDTO {
  @IsOptional()
  @IsMongoId()
  application_id: string;

  @IsNotEmpty()
  @IsMongoId()
  projectId: string;

  @IsNotEmpty()
  @IsNumber()
  total: number;

  @IsNotEmpty()
  @IsNumber()
  grandTotal: number;

  @IsNotEmpty()
  @IsString()
  appStatus: string;

  @IsOptional()
  @IsMongoId()
  coupon: string;

  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => AwardsDTO)
  awards: AwardsDTO[];
}

export class ApplicationSuccessDTO {
  @IsNotEmpty()
  @IsMongoId()
  application_id: string;

  @IsOptional()
  @IsMongoId()
  coupon: string;

  @IsNotEmpty()
  @IsString()
  razorpay_order_id: string;

  @IsNotEmpty()
  @IsString()
  razorpay_payment_id: string;

  @IsNotEmpty()
  @IsString()
  razorpay_signature: string;

  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => NomineesDTO)
  nominations: NomineesDTO[];
}

export class ApplicationPaymentDTO {
  @IsNotEmpty()
  @IsMongoId()
  application_id: string;

  @IsNotEmpty()
  @IsNumber()
  amount: number;

  @IsOptional()
  @IsMongoId()
  coupon: string;

  @IsOptional()
  @IsNumber()
  discountAmount: number;

  @IsNotEmpty()
  @IsMongoId()
  userId: string;

  @IsNotEmpty()
  @IsNumber()
  totalAmount: number;
}
