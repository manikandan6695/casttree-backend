import * as mongoose from "mongoose";
import { IUserModel } from "src/user/schema/user.schema";
import { IProjectModel } from "src/project/schema/project.schema";

export interface IAwardsModel {
  awardId: any;
  count: number;
}
export interface IApplicationModel extends mongoose.Document {
  projectId: any;
  total: number;
  coupon: any;
  payment_order_id: string;
  invoice_id: string;
  payment_id: string;
  grandTotal: number;
  currency: string;
  appStatus: string;
  awards: IAwardsModel[];
  status: string;
  submittedBy: IUserModel;
  createdBy: IUserModel;
  updatedBy: IUserModel;
}
export const AwardsSchema = new mongoose.Schema<any>({
  awardId: { type: mongoose.Schema.Types.ObjectId, ref: "awards" },
  count: { type: Number },
});
export const ApplicationSchema = new mongoose.Schema(
  {
    projectId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "project",
    },
    total: {
      type: Number,
    },
    coupon: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "coupon",
    },
    grandTotal: {
      type: Number,
    },
    payment_order_id: {
      type: String,
    },
    invoice_id: {
      type: String,
    },
    payment_id: {
      type: String,
    },
    currency: {
      type: String,
    },
    appStatus: {
      type: String,
    },
    awards: [AwardsSchema],
    submittedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    status: {
      type: String,
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    updatedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  {
    collection: "application",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
