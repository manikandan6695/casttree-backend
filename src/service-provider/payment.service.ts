import { HttpStatus, Injectable } from "@nestjs/common";
import { EPaymentProvider } from "./enum/payment-providers.enum";
import { ICurrencyModel } from "src/shared/schema/currency.schema";
import { ConfigService } from "@nestjs/config";
import { UserService } from "src/user/user.service";
import { ValidateRPPaymentId } from "./dto/validate-rp-payment.dto";
import { AppException } from "src/shared/app-exception";
const crypto = require("crypto");
const Razorpay = require("razorpay");

@Injectable()
export class PaymentService {
  constructor(
    private configService: ConfigService,
    private userService: UserService
  ) {}

  async getPGInstance() {
    try {
      let pg_instance;
      let pg_type = this.configService.get("PAYMENT_TYPE");
      let RAZORPAY_API_KEY = this.configService.get("RAZORPAY_API_KEY");
      let RAZORPAY_SECRET_KEY = this.configService.get("RAZORPAY_SECRET_KEY");
      switch (pg_type) {
        case EPaymentProvider.razorpay:
          pg_instance = new Razorpay({
            key_id: RAZORPAY_API_KEY,
            key_secret: RAZORPAY_SECRET_KEY,
          });
          break;
        default:
          break;
      }
      return {
        pg_instance,
        pg_type,
        pg_credentials: {
          RAZORPAY_API_KEY,
          RAZORPAY_SECRET_KEY,
        },
      };
    } catch (err) {
      throw err;
    }
  }
  async createPGOrder(
    user_id: string,
    currency: ICurrencyModel,
    amount: number,
    reference_no: string
  ) {
    try {
      let order_id: string;
      let pg_detail = await this.getPGInstance();
      let pg_instance = pg_detail.pg_instance;
      let pg_type: string = pg_detail.pg_type;
      let user = await this.userService.getUserById(user_id);
      let pg_meta = { name: user.userName };
      switch (pg_type) {
        case EPaymentProvider.razorpay:
          var options = {
            amount: amount * currency.base_conversion_factor,
            receipt: reference_no,
          };
          let order_detail: any = await new Promise((res, rej) => {
            pg_instance.orders.create(options, function (err, order) {
              if (err) rej(err);
              res(order);
            });
          });
          order_id = order_detail.id;
          break;
        default:
          break;
      }
      console.log("order_id", order_id, pg_type);

      return { pg_meta, pg_type, order_id };
    } catch (err) {
      throw err;
    }
  }

  async validatePayment(body: ValidateRPPaymentId) {
    let pg_type = this.configService.get("PAYMENT_TYPE");
    let RAZORPAY_API_KEY = this.configService.get("RAZORPAY_API_KEY");
    let is_valid: boolean = false;
    switch (pg_type) {
      case EPaymentProvider.razorpay:
        let RAZORPAY_SECRET_KEY = this.configService.get("RAZORPAY_SECRET_KEY");
        const hmac = crypto.createHmac("sha256", RAZORPAY_SECRET_KEY);
        hmac.update(body.razorpay_order_id + "|" + body.razorpay_payment_id);
        let generatedSignature = hmac.digest("hex");
        let isSignatureValid = generatedSignature == body.razorpay_signature;
        if (!isSignatureValid) {
          throw new AppException("Payment not matched", HttpStatus.FORBIDDEN);
        }
        is_valid = true;
        break;
      default:
        break;
    }
    return is_valid;
  }
}
