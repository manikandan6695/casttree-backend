import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";
import { ScheduleModule } from "@nestjs/schedule";
import { AuthModule } from "./auth/auth.module";
import { ConfigurationModule } from "./configuration/configuration.module";
import { IndexModule } from "./index/index.module";
import { LoggerModule } from "./logger/logger.module";
import { MediaModule } from "./media/media.module";
import { PreferencesModule } from "./preferences/preferences.module";
import { RoleModule } from "./role/role.module";
import { ScopeModule } from "./scope/scope.module";
import { SharedModule } from "./shared/shared.module";
import { SocketModule } from "./socket/socket.module";
import { UploaderModule } from "./uploader/uploader.module";
import { UserModule } from "./user/user.module";
import { APP_GUARD } from "@nestjs/core";
import { ThrottlerBehindProxyGuard } from "./auth/guards/throttler-behind-proxy.guard";
import { SkillsModule } from "./skills/skills.module";
import { ProjectModule } from "./project/project.module";
import { CategoryModule } from "./category/category.module";
import { GenresModule } from "./genres/genres.module";
import { AwardsModule } from "./awards/awards.module";
import { ApplicationModule } from "./application/application.module";
import { NominationsModule } from "./nominations/nominations.module";
import { EventEmitterModule } from "@nestjs/event-emitter";
import { ThrottlerModule } from "@nestjs/throttler";
import { CouponModule } from "./coupon/coupon.module";
import { PaymentRequestModule } from "./payment-request/payment-request.module";
import { InvoiceModule } from "./invoice/invoice.module";
import { CtApiModule } from './ct-api/ct-api.module';
import { SurveyModule } from './survey/survey.module';
import { ProfileModule } from './profile/profile.module';
import { EndorsementModule } from './endorsement/endorsement.module';
import { ConnectionModule } from './connection/connection.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: ".env" }),
    LoggerModule,
    UploaderModule,
    ThrottlerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => [
        {
          ttl: config.get("THROTTLE_TTL"),
          limit: config.get("THROTTLE_LIMIT"),
        },
      ],
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => {
        return {
          uri: config.get("DB_URL"),
        };
      },
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    AuthModule,
    SharedModule,
    ConfigurationModule,
    RoleModule,
    UserModule,
    MediaModule,
    EventEmitterModule.forRoot(),
    PreferencesModule,
    ScopeModule,
    SocketModule,
    IndexModule,
    SkillsModule,
    ProjectModule,
    CategoryModule,
    GenresModule,
    AwardsModule,
    ApplicationModule,
    NominationsModule,
    CouponModule,
    PaymentRequestModule,
    InvoiceModule,
    CtApiModule,
    SurveyModule,
    ProfileModule,
    EndorsementModule,
    ConnectionModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerBehindProxyGuard,
    },
  ],
})
export class AppModule {}
