import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { IQuestionsModel } from "../schema/questions.schema";

@Injectable()
export class QuestionsService {
  constructor(
    @InjectModel("questions")
    private readonly questionsModel: Model<IQuestionsModel>,
    private shared_service: SharedService
  ) {}

  async getQuestions(surveyKey: string) {
    try {
      let data = await this.questionsModel
        .find({ surveyKey: surveyKey })
        .sort({ _id: -1 });
      return data;
    } catch (err) {
      throw err;
    }
  }
}
