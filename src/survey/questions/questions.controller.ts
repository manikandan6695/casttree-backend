import { Controller, Get, Query, Res } from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { QuestionsService } from "./questions.service";
import { Response } from "express";

@Controller("questions")
export class QuestionsController {
  constructor(
    private readonly questionsService: QuestionsService,
    private sservice: SharedService
  ) {}

  @Get()
  async getQuestions(
    @Query("surveyKey") surveyKey: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.questionsService.getQuestions(surveyKey);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
