import { Type } from "class-transformer";
import {
  IsArray,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";

export class ResponseAnswerDTO {
  @IsNotEmpty()
  @IsMongoId()
  questionId: string;

  @IsNotEmpty()
  @IsString()
  answer: string;
}
export class ResponseDTO {
  @IsNotEmpty()
  @IsString()
  surveyKey: string;

  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ResponseAnswerDTO)
  response: ResponseAnswerDTO[];
}
