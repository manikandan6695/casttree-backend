import { Controller, Get, ParseIntPipe, Query, Res } from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { SurveyService } from "./survey.service";
import { Response } from "express";

@Controller("survey")
export class SurveyController {
  constructor(
    private readonly surveyService: SurveyService,
    private sservice: SharedService
  ) {}

  @Get()
  async getSurveys(
    @Query("skip", ParseIntPipe) skip: number,
    @Query("limit", ParseIntPipe) limit: number,
    @Res() res: Response
  ) {
    try {
      let data = await this.surveyService.getSurveys(skip,limit);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
