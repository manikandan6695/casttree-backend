import { Module } from "@nestjs/common";
import { SurveyService } from "./survey.service";
import { SurveyController } from "./survey.controller";
import { QuestionsController } from "./questions/questions.controller";
import { QuestionsService } from "./questions/questions.service";
import { QuestionOptionController } from "./question-option/question-option.controller";
import { QuestionOptionService } from "./question-option/question-option.service";
import { MongooseModule } from "@nestjs/mongoose";
import { SharedModule } from "src/shared/shared.module";
import { AuthModule } from "src/auth/auth.module";
import { surveySchema } from "./schema/survey.schema";
import { responseSchema } from "./schema/response.schema";
import { questionsSchema } from "./schema/questions.schema";
import { questionOptionSchema } from "./schema/questionOption.schema";
import { ResponseController } from "./response/response.controller";
import { ResponseService } from "./response/response.service";
import { responseAnswerSchema } from "./schema/responseAnswer.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "survey", schema: surveySchema },
      { name: "questions", schema: questionsSchema },
      { name: "questionOption", schema: questionOptionSchema },
      { name: "response", schema: responseSchema },
      { name: "responseAnswer", schema: responseAnswerSchema },
    ]),
    SharedModule,
    AuthModule,
  ],
  providers: [
    SurveyService,
    QuestionsService,
    QuestionOptionService,
    ResponseService,
  ],
  controllers: [
    SurveyController,
    QuestionsController,
    QuestionOptionController,
    ResponseController,
  ],
})
export class SurveyModule {}
