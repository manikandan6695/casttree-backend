import { Controller, Get, Query, Res } from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { QuestionOptionService } from "./question-option.service";
import { Response } from "express";

@Controller("question-option")
export class QuestionOptionController {
  constructor(
    private readonly questionOptionService: QuestionOptionService,
    private sservice: SharedService
  ) {}

  @Get()
  async getQuestionOption(
    @Query("questionId") questionId: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.questionOptionService.getQuestionOption(questionId);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
