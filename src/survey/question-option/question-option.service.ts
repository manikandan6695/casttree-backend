import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { IQuestionOptionModel } from "../schema/questionOption.schema";

@Injectable()
export class QuestionOptionService {
  constructor(
    @InjectModel("questionOption")
    private readonly questionOptionModel: Model<IQuestionOptionModel>,
    private shared_service: SharedService
  ) {}

  async getQuestionOption(questionId: string) {
    try {
      let data = await this.questionOptionModel.find({
        questionId: questionId,
      });
      return data;
    } catch (err) {
      throw err;
    }
  }
}
