import * as mongoose from "mongoose";

export interface IQuestionOptionModel extends mongoose.Document {
  questionId: string;
  optionName: string;
  optionDescription: string;
  status: string;
  created_by: string;
  updated_by?: string;
}
export const questionOptionSchema = new mongoose.Schema(
  {
    questionId: { type: mongoose.Schema.Types.ObjectId, ref: "questions" },
    optionName: { type: String },
    optionDescription: { type: String },
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  },
  {
    collection: "questionOption",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
