import * as mongoose from "mongoose";

export interface IResponseAnswerModel extends mongoose.Document {
  surveyKey: string;
  questionId: string;
  answer: string;
  status: string;
  created_by: string;
  updated_by?: string;
}
export const responseAnswerSchema = new mongoose.Schema(
  {
    surveyKey: { type: String },
    questionId: { type: mongoose.Schema.Types.ObjectId, ref: "question" },
    answer: { type: String },
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  },
  {
    collection: "responseAnswer",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
