import * as mongoose from "mongoose";

export interface ISurveyModel extends mongoose.Document {
  name: string;
  surveyKey: string;
  status: string;
  created_by: string;
  updated_by?: string;
}
export const surveySchema = new mongoose.Schema(
  {
    name: { type: String },
    surveyKey: { type: String },
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  },
  {
    collection: "survey",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
