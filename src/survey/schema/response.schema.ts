import * as mongoose from "mongoose";

export interface IResponseModel extends mongoose.Document {
  userId: string;
  submittedAt: Date;
  status: string;
  created_by: string;
  updated_by?: string;
}
export const responseSchema = new mongoose.Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    submittedAt: { type: Date },
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  },
  {
    collection: "response",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
