import * as mongoose from "mongoose";

export interface IQuestionsModel extends mongoose.Document {
  surveyKey: string;
  question: string;
  questionType: string;
  isMandatory: boolean;
  order: number;
  status: string;
  created_by: string;
  updated_by?: string;
}
export const questionsSchema = new mongoose.Schema(
  {
    surveyKey: { type: String },
    question: { type: String },
    questionType: { type: String },
    isMandatory: { type: Boolean },
    order: { type: Number },
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  },
  {
    collection: "questions",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
