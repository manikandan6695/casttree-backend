import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { ISurveyModel } from "./schema/survey.schema";

@Injectable()
export class SurveyService {
  constructor(
    @InjectModel("survey")
    private readonly surveyModel: Model<ISurveyModel>,
    private shared_service: SharedService
  ) {}

  async getSurveys(skip: number, limit: number) {
    try {
      let filter = { status: "Active" };
      let data = await this.surveyModel
        .find(filter)
        .skip(skip)
        .limit(limit)
        .sort({ _id: -1 });
      return data;
    } catch (err) {
      throw err;
    }
  }
}
