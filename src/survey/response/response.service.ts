import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { UserToken } from "src/user/dto/usertoken.dto";
import { ResponseDTO } from "../dto/response.dto";
import { IResponseModel } from "../schema/response.schema";
import { IResponseAnswerModel } from "../schema/responseAnswer.schema";

@Injectable()
export class ResponseService {
  constructor(
    @InjectModel("response")
    private readonly responseModel: Model<IResponseModel>,
    @InjectModel("responseAnswer")
    private readonly responseAnswerModel: Model<IResponseAnswerModel>,
    private shared_service: SharedService
  ) {}

  async submitResponse(body: ResponseDTO, token: UserToken) {
    try {
      await this.responseModel.create({
        userId: token.id,
        submittedAt: new Date().toISOString(),
      });
      for (let i = 0; i < body.response.length; i++) {
        let curr_response = body.response[i];
        await this.responseAnswerModel.create({
          questionId: curr_response.questionId,
          surveyKey: body.surveyKey,
          answer: curr_response.answer,
        });
      }

      return { message: "Submitted Successfully" };
    } catch (err) {
      throw err;
    }
  }
}
