import {
  Body,
  Controller,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { ResponseService } from "./response.service";
import { Response } from "express";
import { ResponseDTO } from "../dto/response.dto";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { UserToken } from "src/user/dto/usertoken.dto";

@Controller("response")
export class ResponseController {
  constructor(
    private readonly responseService: ResponseService,
    private sservice: SharedService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async submitResponse(
    @Body(new ValidationPipe({ whitelist: true })) body: ResponseDTO,
    @GetToken() token: UserToken,
    @Res() res: Response
  ) {
    try {
      let data = await this.responseService.submitResponse(body, token);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
