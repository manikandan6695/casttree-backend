import * as mongoose from "mongoose";

export interface IPreferenceModel extends mongoose.Document {
  key?: string;
  value?: any;
  created_by?: any;
  updated_by?: any;
}

export const PreferenceSchema = new mongoose.Schema(
  {
    key: { type: String },
    value: { type: mongoose.Schema.Types.Mixed },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  },
  {
    collection: "preference",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
