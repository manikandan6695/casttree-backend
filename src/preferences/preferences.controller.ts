import {
  Controller,
  Get,
  Put,
  Query,
  Body,
  Delete,
  UseGuards,
  Res,
  Post,
  UploadedFile,
  UseInterceptors,
  Param,
} from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import { SharedService } from "src/shared/shared.service";
import { query, Response } from "express";
import { PreferencesService } from "./preferences.service";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { UserToken } from "src/user/dto/usertoken.dto";

@Controller("preferences")
export class PreferencesController {
  constructor(
    private sservice: SharedService,
    private pservice: PreferencesService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post("save-preference")
  async saveCategory(@Res() res: Response, @Body() body: any,@GetToken() token: UserToken) {
    try {
      let data = await this.pservice.savePreference(body,token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("get-my-preference")
  async getMyPreference(@Res() res: Response, @Body() body: any,@GetToken() token: UserToken) {
    try {
      let data = await this.pservice.getMyPreference(body,token);
      return data;
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
