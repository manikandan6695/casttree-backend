import { AuthModule } from "./../auth/auth.module";
import { SharedModule } from "./../shared/shared.module";
import { MongooseModule } from "@nestjs/mongoose";
import { Module } from "@nestjs/common";
import { PreferencesController } from "./preferences.controller";
import { PreferencesService } from "./preferences.service";
import { PreferenceSchema } from "./schema/preferences.schema";
import { UserSchema } from "src/user/schema/user.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "preference", schema: PreferenceSchema },
      { name: "user", schema: UserSchema },
    ]),
    SharedModule,
    AuthModule,
  ],
  controllers: [PreferencesController],
  providers: [PreferencesService],
})
export class PreferencesModule {}
