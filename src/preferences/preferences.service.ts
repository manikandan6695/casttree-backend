import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IUserModel } from "src/user/schema/user.schema";
import { IPreferenceModel } from "./schema/preferences.schema";

@Injectable()
export class PreferencesService {
  constructor(
    @InjectModel("preference")
    private readonly preferenceModel: Model<IPreferenceModel>,
    @InjectModel("user")
    private readonly userModel: Model<IUserModel>
  ) {}

  async savePreference(body, token) {
    try {
      let data = await this.preferenceModel.updateOne(
        { _id: body.id, key: body.key },
        {
          $set: {
            value: body.value,
            updated_by: token.id,
          },
        }
      );

      return data;
    } catch (err) {
      throw err;
    }
  }

  async getMyPreference(body,token) {
    try {
      let data = await this.preferenceModel.findOne(
        { _id: token.id ,key : body.key}
      );
      return data;
    } catch (err) {
      throw err;
    }
  }
}
