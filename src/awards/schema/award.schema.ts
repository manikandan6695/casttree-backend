import { IMedia } from "./../../media/schema/media.schema";
import * as mongoose from "mongoose";
import { ICategoryModel } from "src/category/schema/category.schema";
import { MediaSchema } from "src/user/schema/user.schema";

export interface IAwardsModel extends mongoose.Document {
  title: string;
  description: string;
  awardType: string;
  price: string;
  currency: string;
  media: IMedia[];
  status: string;
  category: ICategoryModel;
}
export const awardsSchema = new mongoose.Schema(
  {
    title: {
      type: String,
    },
    description: {
      type: String,
    },
    awardType: {
      type: String,
    },
    price: {
      type: String,
    },
    currency: {
      type: String,
    },
    media: [MediaSchema],
    status: {
      type: String,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "category",
    },
  },
  {
    collection: "awards",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
