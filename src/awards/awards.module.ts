import { MongooseModule } from "@nestjs/mongoose";
import { Module } from "@nestjs/common";
import { AwardsController } from "./awards.controller";
import { AwardsService } from "./awards.service";
import { awardsSchema } from "./schema/award.schema";
import { SharedModule } from "src/shared/shared.module";
import { AuthModule } from "src/auth/auth.module";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "awards", schema: awardsSchema }]),
    SharedModule,
    AuthModule,
  ],
  controllers: [AwardsController],
  providers: [AwardsService],
})
export class AwardsModule {}
