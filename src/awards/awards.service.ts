import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { IAwardsModel } from "./schema/award.schema";

@Injectable()
export class AwardsService {
  constructor(
    @InjectModel("awards")
    private readonly awardsModel: Model<IAwardsModel>,
    private shared_service: SharedService
  ) {}

  async getAwards(category: any) {
    try {
      let filter = { category: category };

      let data = await this.awardsModel
        .find(filter)
        .populate("category")
        .populate("media.media_id", "media_url");
      return { data };
    } catch (err) {
      throw err;
    }
  }
}
