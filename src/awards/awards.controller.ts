import { Response } from "express";
import { Controller, Get, Query, Res } from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { AwardsService } from "./awards.service";

@Controller("awards")
export class AwardsController {
  constructor(
    private readonly awardsService: AwardsService,
    private sservice: SharedService
  ) {}

  @Get("get-awards")
  async getGenres(@Query("category") category: string, @Res() res: Response) {
    try {
      let roles = await this.awardsService.getAwards( category);
      return res.json(roles);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
