import { IMedia } from "src/media/schema/media.schema";
import * as mongoose from "mongoose";
import { MediaSchema } from "src/user/schema/user.schema";
export interface IProjectModel extends mongoose.Document {
  title: string;
  recognition: string;
  selfRole: string;
  category: string;
  description: string;
  documentStatus: string;
  genre: any;
  media: any[];
  completionDate: Date;
  submittedBy: string;
  status: string;
  createdBy: string;
  updatedBy: string;
}
export const projectsSchema = new mongoose.Schema(
  {
    title: {
      type: String,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "category",
    },
    recognition: {
      type: String,
    },
    selfRole: {
      type: String,
    },
    description: {
      type: String,
    },
    genre: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "genre",
    },
    media: [MediaSchema],
    completionDate: {
      type: String,
    },
    submittedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    documentStatus: {
      type: String,
    },
    status: {
      type: String,
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    updatedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  {
    collection: "projects",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
