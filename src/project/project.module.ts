import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthModule } from "src/auth/auth.module";
import { SharedModule } from "src/shared/shared.module";
import { ProjectController } from "./project.controller";
import { ProjectService } from "./project.service";
import { projectsSchema } from "./schema/project.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "project", schema: projectsSchema }]),
    SharedModule,
    AuthModule,
  ],
  controllers: [ProjectController],
  providers: [ProjectService],
  exports: [ProjectService],
})
export class ProjectModule {}
