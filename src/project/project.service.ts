import { UserToken } from "src/user/dto/usertoken.dto";
import { ProjectDTO } from "./dto/project.dto";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { IProjectModel } from "./schema/project.schema";

@Injectable()
export class ProjectService {
  constructor(
    @InjectModel("project")
    private readonly projectModel: Model<IProjectModel>,
    private shared_service: SharedService
  ) {}

  async saveProject(body: any, token: UserToken) {
    try {
      let data;
      let project = [body];

      for (let i = 0; i < project.length; i++) {
        let curr_data = project[i];
        let fv = {
          ...curr_data,
          submittedBy: token.id,
          createdBy: token.id,
          updatedBy: token.id,
        };

        if (curr_data.project_id)
          data = await this.projectModel.updateOne(
            { _id: curr_data.project_id },
            { $set: fv }
          );
        else data = await this.projectModel.create(fv);
      }

      return data;
    } catch (err) {
      throw err;
    }
  }
  async getProject(project_id: string, token: UserToken) {
    try {
      let data = await this.projectModel
        .findOne({ _id: project_id })
        .populate("category")
        .populate("genre")
        .populate("media.media_id");

      return { data };
    } catch (err) {
      throw err;
    }
  }
  async getProjects(
    token: UserToken,
    search?: string,
    skip?: number,
    limit?: number
  ) {
    try {
      let filter = { createdBy: token.id };
      if (search) {
        filter["title"] = search;
      }
      let data = await this.projectModel
        .find(filter)
        .populate("category")
        .populate("genre")
        .populate("media.media_id")
        .sort({ _id: -1 })
        .skip(skip)
        .limit(limit);
      return data;
    } catch (err) {
      throw err;
    }
  }
}
