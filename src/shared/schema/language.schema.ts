import * as mongoose from "mongoose";

export interface ILanguage extends mongoose.Document {
  language_id: string;
  language_code: string;
  language_name: string;
  language_native_name: string;
}

export const LanguageSchema = new mongoose.Schema(
  {
    language_id: { type: String },
    language_code: { type: String },
    language_name: { type: String },
    language_native_name: { type: String },
  },
  {
    collection: "language",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
