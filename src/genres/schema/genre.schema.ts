import * as mongoose from "mongoose";
import { ICategoryModel } from "src/category/schema/category.schema";

export interface IGenreModel extends mongoose.Document {
  name: string;
  status: string;
  category: ICategoryModel;
}
export const genreSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    status: {
      type: String,
    },
    categroy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "category",
    },
  },
  {
    collection: "genre",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
