import { IGenreModel } from "./schema/genre.schema";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";

@Injectable()
export class GenresService {
  constructor(
    @InjectModel("genre")
    private readonly genreModel: Model<IGenreModel>,
    private shared_service: SharedService
  ) {}

  async getGenres(search: string, category: any) {
    try {
      console.log("category is", category);

      let filter = { category: category };
      if (search) {
        filter["name"] = search;
      }
      console.log("filter is", filter);

      let data = await this.genreModel.find(filter);
      return data;
    } catch (err) {
      throw err;
    }
  }
}
