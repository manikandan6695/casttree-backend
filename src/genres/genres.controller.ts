import { GenresService } from "./genres.service";
import { Response } from "express";
import { Controller, Get, Query, Res } from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";

@Controller("genres")
export class GenresController {
  constructor(
    private readonly genreService: GenresService,
    private sservice: SharedService
  ) {}

  @Get("get-genres")
  async getGenres(
    @Query("search") search: string,
    @Query("category") category: string,
    @Res() res: Response
  ) {
    try {
      let roles = await this.genreService.getGenres(search, category);
      return res.json(roles);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
