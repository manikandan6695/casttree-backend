import { genreSchema } from "./schema/genre.schema";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthModule } from "src/auth/auth.module";
import { SharedModule } from "src/shared/shared.module";
import { GenresController } from "./genres.controller";
import { GenresService } from "./genres.service";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "genre", schema: genreSchema }]),
    SharedModule,
    AuthModule,
  ],
  controllers: [GenresController],
  providers: [GenresService],
  exports: [GenresService],
})
export class GenresModule {}
