import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";

export class FilterDTO {

  @IsOptional()
  @IsString()
  search : string;
  
  @IsNotEmpty()
  @IsNumber()
  skip : number

  @IsNotEmpty()
  @IsNumber()
  limit : number

  @IsOptional()
  @IsArray()
  role: string[];
}
