import * as mongoose from "mongoose";
import { IMedia } from "src/media/schema/media.schema";
import { MediaSchema } from "src/user/schema/user.schema";

export interface ISkillsModel extends mongoose.Document {
  skill_name: string;
  role: string[];
  media: IMedia[];
  status: string;
  created_by: string;
  updated_by?: string;
  is_system?: boolean;
}
export const skillsSchema = new mongoose.Schema(
  {
    skill_name: { type: String },
    media: [MediaSchema],
    role: [{ type: mongoose.Schema.Types.ObjectId, ref: "category" }],
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    is_system: { type: Boolean, default: false },
  },
  {
    collection: "skills",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
