import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { FilterDTO } from "./dto/filter-role.dto";
import { ISkillsModel } from "./schema/skills.schema";

@Injectable()
export class SkillsService {
  constructor(
    @InjectModel("skills")
    private readonly skillsModel: Model<ISkillsModel>,
    private shared_service: SharedService
  ) {}

  async getSkills(query: FilterDTO) {
    try {
      let filters = { status: "Active" };

      if (query.search) {
        filters["skill_name"] = new RegExp(query.search, "i");
      }

      if (query.role.length) {
        filters["role"] = { $in: query.role };
      }

      let data = await this.skillsModel
        .find(filters)
        .populate("media.media_id")
        .sort({ skill_name: 1 })
        .skip(query.skip)
        .limit(query.limit);
      let count = await this.skillsModel.countDocuments(filters);
      return { data, count };
    } catch (err) {
      throw err;
    }
  }
}
