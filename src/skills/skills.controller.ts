import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Res,
  ValidationPipe,
} from "@nestjs/common";
import { Response } from "express";
import { SharedService } from "src/shared/shared.service";
import { FilterDTO } from "./dto/filter-role.dto";
import { SkillsService } from "./skills.service";

@Controller("skills")
export class SkillsController {
  constructor(
    private readonly skillsService: SkillsService,
    private sservice: SharedService
  ) {}
  @Post()
  async getSkills(
    @Body(new ValidationPipe()) query: FilterDTO,
    @Res() res: Response
  ) {
    try {
      let skills = await this.skillsService.getSkills(query);
      return res.json(skills);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
