import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { IConnectionModel } from "./schema/connection.schema";
import { IConnectionRequestModel } from "./schema/connectionRequest.schema";

@Injectable()
export class ConnectionService {
  constructor(
    @InjectModel("connection")
    private readonly connectionModel: Model<IConnectionModel>,
    @InjectModel("connectionRequest")
    private readonly connectionRequestModel: Model<IConnectionRequestModel>,
    private shared_service: SharedService
  ) {}

  async createConnectionRequest(body, token) {
    try {
      let fv = {
        ...body,
        fromUserId: token.id,
        requestStatus: "Pending",
        requestStatusHistory: ["Pending"],
        createdBy: token.id,
        updatedBy: token.id,
      };

      let data = await this.connectionRequestModel.create(fv);
      return data;
    } catch (err) {
      throw err;
    }
  }

  async getConnectionRequestList(skip, limit, token, type) {
    try {
      let data = await this.connectionRequestModel
        .find({
          type: type,
          $or: [{ fromUserId: token.id }, { toUserId: token.id }],
        })
        .skip(skip)
        .limit(limit)
        .populate("fromUserId")
        .populate("toUserId");

      return data;
    } catch (err) {
      throw err;
    }
  }

  async getConnections(skip, limit, token, type) {
    try {
      let data = await this.connectionModel
        .find({
          type: type,
          $or: [{ fromUserId: token.id }, { toUserId: token.id }],
        })
        .skip(skip)
        .limit(limit)
        .populate("fromUserId")
        .populate("toUserId");

      return data;
    } catch (err) {
      throw err;
    }
  }

  async updateConnectionRequest(body, token) {
    try {
      console.log("requestId", body.requestId);

      let connection = await this.connectionRequestModel
        .findOne({
          _id: body.requestId,
        })
        .lean();

      let status = connection["requestStatus"];
      await this.connectionRequestModel.updateOne(
        { _id: body.requestId },
        {
          $set: {
            requestStatus: body.requestStatus,
            requestStatusHistory: [status, body.requestStatus],
          },
        }
      );

      await this.connectionModel.create({
        fromUserId: token.id,
        toUserId: body.toUserId,
        type: body.type,
        connectionStatus: "Active",
      });

      return { message: "Updated Successfully" };
    } catch (err) {
      throw err;
    }
  }
}
