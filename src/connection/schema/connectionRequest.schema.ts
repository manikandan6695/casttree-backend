import * as mongoose from "mongoose";
import { IUserModel } from "src/user/schema/user.schema";

export interface IConnectionRequestModel extends mongoose.Document {
  fromUserId: string | IUserModel;
  toUserId: string | IUserModel;
  requestStatus: string;
  requestStatusHistory: string[];
  type: string;
  status: string;
  createdBy: string | IUserModel;
  updatedBy: string | IUserModel;
}
export const connectionRequestSchema = new mongoose.Schema(
  {
    fromUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    toUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    requestStatus: {
      type: String,
    },
    requestStatusHistory: [
      {
        type: String,
      },
    ],
    type: {
      type: String,
    },
    status: {
      type: String,
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    updatedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  {
    collection: "connectionRequest",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
