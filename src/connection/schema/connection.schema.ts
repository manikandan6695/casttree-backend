import * as mongoose from "mongoose";
import { IUserModel } from "src/user/schema/user.schema";

export interface IConnectionModel extends mongoose.Document {
  fromUserId: string | IUserModel;
  toUserId: string | IUserModel;
  connectionStatus: string;
  type: string;
  status: string;
  createdBy: string | IUserModel;
  updatedBy: string | IUserModel;
}
export const connectionSchema = new mongoose.Schema(
  {
    fromUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    toUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    connectionStatus: {
      type: String,
    },
    type: {
      type: String,
    },
    status: {
      type: String,
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    updatedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  {
    collection: "connection",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
