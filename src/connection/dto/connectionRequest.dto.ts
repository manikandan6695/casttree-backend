import { IsMongoId, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class ConnectionRequestDTO {
  @IsNotEmpty()
  @IsMongoId()
  toUserId: string;

  @IsNotEmpty()
  @IsString()
  type: string;
}

export class UpdateConnectionDTO {
  @IsNotEmpty()
  @IsString()
  requestStatus: string;

  @IsNotEmpty()
  @IsMongoId()
  toUserId: string;

  @IsNotEmpty()
  @IsMongoId()
  requestId: string;

  @IsNotEmpty()
  @IsString()
  type: string;
}
