import * as mongoose from "mongoose";
import { EStatus } from "src/shared/enum/privacy.enum";

export interface IRoleAssignmentModel extends mongoose.Document {
  organization_id?: any;
  product_key: string;
  user_id?: any;
  role_id?: any;
  status: EStatus;
  is_system: boolean;
  created_by?: any;
  updated_by?: any;
  created_at: string | Date;
  updated_at: string | Date;
}
export const roleAssignmentSchema = new mongoose.Schema(
  {
    organization_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "organization",
    },
    product_key: {
      type: String,
    },
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    role_id: { type: mongoose.Schema.Types.ObjectId, ref: "role" },
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    is_system: { type: Boolean, default: false },
  },
  {
    collection: "roleAssignment",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
