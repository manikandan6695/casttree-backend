import * as mongoose from "mongoose";
import { ERoleType, ESRoleType } from "../enum/role-type.enum";

export interface IRoleModel extends mongoose.Document {
  role_name: string;
  role_description: string;
  is_default?: boolean;
  is_promoter_role?: boolean;
  status: string;
  created_by: string;
  updated_by?: string;
  is_system?: boolean;
}
export const roleSchema = new mongoose.Schema(
  {
    role_name: { type: String },
    role_description: { type: String },
    is_default: { type: Boolean },
    is_promoter_role: { type: Boolean },
    status: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    is_system: { type: Boolean, default: false },
  },
  {
    collection: "role",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
