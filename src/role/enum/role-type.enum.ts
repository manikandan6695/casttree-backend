export enum ERoleType {
  feature = "feature",
  record = "record",
  default = "default",
}
export const ESRoleType = ["feature", "record", "default"];
