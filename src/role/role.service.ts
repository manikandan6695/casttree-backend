import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { IRoleModel } from "./schema/role.schema";
import { IRoleAssignmentModel } from "./schema/role-assignment.schema";
import { ERoleType } from "./enum/role-type.enum";
import { SharedService } from "src/shared/shared.service";
import { UserToken } from "src/user/dto/usertoken.dto";
import { ConfigurationService } from "src/configuration/configuration.service";
import { EStatus } from "src/shared/enum/privacy.enum";
import { EProductKey } from "src/auth/enum/product-key.enum";

@Injectable()
export class RoleService {
  constructor(
    @InjectModel("role")
    private readonly roleModel: Model<IRoleModel>,
    @InjectModel("roleAssignment")
    private readonly roleAssignmentModel: Model<IRoleAssignmentModel>,
    private shared_service: SharedService,
    private configuration_service: ConfigurationService
  ) {}

  async roleCreate(body: any) {
    try {
      await this.roleModel.create(body);
      return { message: "record created successfully" };
    } catch (err) {
      throw err;
    }
  }

  async roleUpdate(id: string, body: any) {
    try {
      await this.roleModel.findByIdAndUpdate(id, body, { new: true });
      let updatedData = await this.roleModel.findById(id);
      return updatedData;
    } catch (err) {
      throw err;
    }
  }

  async roleDelete(id: string) {
    try {
      await this.roleModel.findByIdAndDelete(id);
      return { message: "record deleted successfully" };
    } catch (err) {
      throw err;
    }
  }

  async roleList(organization_id: string, product_key: string) {
    try {
      let data = await this.roleModel.find(
        {
          organization_id: organization_id,
          product_key,
          role_type: ERoleType.feature,
          status: EStatus.Active,
        },
        { _id: 1, role_description: 1, role_code: 1 }
      );
      return data;
    } catch (err) {
      throw err;
    }
  }

  async roleDetail(id: string) {
    try {
      let data = await this.roleModel.findById(id);
      return data;
    } catch (err) {
      throw err;
    }
  }

  async createRoleAssignment(
    organization_id: string,
    user_id: string,
    role_id: string,
    product_key: string,
    created_by: string
  ) {
    try {
      let data = await this.roleAssignmentModel.create({
        organization_id,
        product_key,
        user_id,
        role_id,
        created_by,
      });

      return data;
    } catch (err) {
      throw err;
    }
  }

  async getUserRole(
    organization_id: string,
    user_id: string,
    product_key: string
  ) {
    try {
      let data = await this.roleAssignmentModel.find({
        organization_id: organization_id,
        product_key,
        user_id: user_id,
      });

      return data;
    } catch (err) {
      throw err;
    }
  }
  async getRoleAssignmentDetail(id: string, organization_id: string) {
    try {
      console.log("id is", id, organization_id);

      let roleAssignment = await this.roleAssignmentModel
        .findOne({
          user_id: id,
          organization_id: organization_id,
        })
        .populate("role_id", "_id role_description");
      // .select("role.role_id.role_description role.role_id._id");

      console.log("role assignment", roleAssignment);

      return roleAssignment;
    } catch (err) {
      throw err;
    }
  }
  async getUserIdsFromRole(organization_id: string, filters: any) {
    let role = await this.roleModel.findOne({ organization_id, ...filters });
    return this.roleAssignmentModel.find({ organization_id, role_id: role });
  }

  async getRoleByFilter(filterObj) {
    try {
      let data = await this.roleModel.find(filterObj);
      return data;
    } catch (err) {
      throw err;
    }
  }
  async getDefaultRoles() {
    try {
      let data = await this.roleModel.find({ is_default: true }).lean();
      return data;
    } catch (err) {
      throw err;
    }
  }
async getRolesList(search : string,skip : number,limit:number){
  try {
    let filters = {status : "Active"};
   
    if (search) {
      filters["role_name"] = new RegExp(search, "i");
    }
    let data = await this.roleModel
      .find(filters).sort({ role_name: 1 })
      .skip(skip)
      .limit(limit);
    let count = await this.roleModel.countDocuments(filters);
    return { data, count };
  } catch (err) {
    throw err;
  }
}
}
