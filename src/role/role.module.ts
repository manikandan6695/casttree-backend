import { AuthModule } from "./../auth/auth.module";
import { SharedModule } from "./../shared/shared.module";
import { MongooseModule } from "@nestjs/mongoose";
import { forwardRef, Module } from "@nestjs/common";
import { RoleController } from "./role.controller";
import { RoleService } from "./role.service";
import { roleSchema } from "./schema/role.schema";
import { roleAssignmentSchema } from "./schema/role-assignment.schema";
import { ConfigurationModule } from "src/configuration/configuration.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "role", schema: roleSchema },
      { name: "roleAssignment", schema: roleAssignmentSchema },
    ]),
    SharedModule,
    AuthModule,
    ConfigurationModule,
  ],
  controllers: [RoleController],
  providers: [RoleService],
  exports: [RoleService],
})
export class RoleModule {}
