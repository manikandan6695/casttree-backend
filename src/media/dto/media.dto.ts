import {
  IsNotEmpty,
  IsArray,
  ValidateNested,
  IsString,
  IsNumber,
  IsOptional,
  IsBoolean,
  IsISO8601,
  IsNotEmptyObject,
  IsMongoId,
} from "class-validator";
import { Type } from "class-transformer";

export class Description {
  @IsNotEmpty()
  @IsString()
  content: string;

  @IsNotEmpty()
  @IsString()
  status: string;

  @IsNotEmpty()
  @IsString()
  version: string;
}

export class MediaDTO {
  @IsOptional()
  @IsString()
  id?: string;

  @IsNotEmpty()
  @IsString()
  media_type: string;

  @IsNotEmpty()
  @IsString()
  media_format: string;

  @IsNotEmpty()
  @IsNumber()
  media_size: number;

  @IsNotEmpty()
  @IsString()
  media_url: string;

  @IsNotEmpty()
  @IsString()
  privacy: string;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Description)
  description?: Description;

  @IsNotEmpty()
  @IsString()
  location: string;

  @IsNotEmpty()
  @IsString()
  file_name: string;

  @IsNotEmpty()
  @IsString()
  local_file_name: string;

  @IsOptional()
  @IsArray()
  tag?: string[];
}

export class RefMediaDTO {
  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  media_id: string;

  @IsOptional()
  @IsString()
  visibility?: string;

  
}
