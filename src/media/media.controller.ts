import {
  Controller,
  Get,
  Query,
  Body,
  UseGuards,
  Res,
  Post,
  UploadedFile,
  UseInterceptors,
  Param,
} from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import { SharedService } from "src/shared/shared.service";
import { Response } from "express";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { MediaDTO } from "./dto/media.dto";
import { MediaService } from "./media.service";
import { UserToken } from "src/user/dto/usertoken.dto";

@Controller("media")
export class MediaController {
  constructor(
    private mediaService: MediaService,
    private sservice: SharedService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post("save-media")
  async saveMedia(
    @GetToken() token: UserToken,
    @Res() res: Response,
    @Body() body: MediaDTO
  ) {
    try {
      let data = await this.mediaService.saveMedia(body, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get("get-single-media")
  async getSingleMedia(
    @GetToken() token: UserToken,
    @Query("id") query,
    @Res() res: Response
  ) {
    try {
      let data = await this.mediaService.getSingleMedia(query, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("get-tag-media")
  async getTagMedia(
    @GetToken() token: UserToken,
    @Body() body: any,
    @Res() res: Response
  ) {
    try {
      let data = await this.mediaService.getTagMedia(body.tag, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @Post("download-file")
  async downloadFile(
    @Body() body: any,
    @Res() res: Response,
    @Query() query: any
  ) {
    try {
      let data = await this.mediaService.downloadFile(body, res);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor("file"))
  @Post("upload-document")
  async uploadDocument(
    @UploadedFile() file,
    @GetToken() token: UserToken,
    @Body() body: any,
    @Res() res: Response
  ) {
    try {
      let data = await this.mediaService.UploadFile(file, token, body);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get("get-list")
  async getList(@Res() res: Response, @GetToken() token: UserToken) {
    try {
      let data = await this.mediaService.getList(token);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get("get-tag-media")
  async getTagMedias(@Res() res: Response, @GetToken() token: UserToken) {
    try {
      let data = await this.mediaService.getList(token);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get("delete-media")
  async deleteMedia(
    @Res() res: Response,
    @Param("id") id: any,
    @GetToken() token: UserToken
  ) {
    try {
      let data = await this.mediaService.deleteMedia(id, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }
  @Post("delete-file")
  async deleteFile(@Body() body: any, @Res() res: Response) {
    try {
      let data = await this.mediaService.deleteFile(body.files);
      return res.json(data);
    } catch (err) {
      const { code, response } = this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }
}
