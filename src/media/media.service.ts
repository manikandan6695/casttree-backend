import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import * as AWS from "aws-sdk";
import { MediaDTO } from "./dto/media.dto";
import { IMediaModel } from "src/media/schema/media.schema";
import { AppException } from "src/shared/app-exception";
import { UploaderService } from "src/uploader/uploader.service";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class MediaService {
  constructor(
    @InjectModel("media") private mediaModel: Model<IMediaModel>,
    private configService: ConfigService,
    private uservice: UploaderService
  ) {}

  async saveMedia(body: MediaDTO, token) {
    try {
      console.log("media is", body);

      let fv = {
        // created_by: token.id,
        media_type: body.media_type,
        media_format: body.media_format,
        media_size: body.media_size,
        media_url: body.media_url,
        privacy: body.privacy,
        location: body.location,
        file_name: body.file_name,
        local_file_name: body.local_file_name,
        description: [
          {
            content: body.description.content,
            version: body.description.version,
          },
        ],
        tag: body.tag,
        created_by: token.id,
      };
      let murl;
      let mid;
      if (body.id) {
        await this.mediaModel.updateOne(
          { _id: body.id },
          { $set: { "description.status": "Inactive" } },
          {
            $push: {
              description: {
                content: body.description.content,
                version: body.description.version,
              },
            },
          }
        );
        mid = body.id;
      } else {
        let crdata = await this.mediaModel.create(fv);
        mid = crdata._id;
        murl = crdata.media_url;
      }

      return { message: "Saved successfully", mediaId: mid, mediaURL: murl };
    } catch (err) {
      throw err;
    }
  }

  async getSingleMedia(query, token) {
    try {
      console.log("query", query);
      if (!query) {
        throw new AppException(
          "Media id is required",
          HttpStatus.NOT_ACCEPTABLE
        );
      }
      let data = await this.mediaModel.findOne({
        _id: query,
        created_by: token.id,
        status: "Active",
      });
      if (!data) {
        throw new AppException("No record found", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (err) {
      throw err;
    }
  }

  async downloadFile(body, res) {
    try {
      let media = await this.mediaModel.findOne({ _id: body.mediaId });
      let location = media.location;
      var s3bucket = new AWS.S3({
        accessKeyId: this.configService.get("UPLOADER_KEY"),
        secretAccessKey: this.configService.get("UPLOADER_SECRET"),
      });
      let params = {
        Bucket: `${this.configService.get("UPLOADER_BUCKET_NAME")}`,
        Key: location,
      };

      s3bucket.getObject(params, function(err, data) {
        if (err) console.log("err", err);
        res.writeHead(200, {
          "Content-Type": "application/octet-stream",
          // "Content-disposition": `inline;filename= "article1593484517654sample.pdf"`,
        });
        res.write(data.Body, "binary");
        res.end(null, "binary");
      });
    } catch (err) {
      throw err;
    }
  }

  async UploadFile(file, token, body) {
    try {
      let data = await this.uservice.UploadFile(file);
      console.log("after upload file", data);

      let splitter = data.type.split(/\//);

      let fileObject: MediaDTO = {
        media_type: splitter[0],
        media_format: splitter[1],
        media_size: file.size,
        media_url: data.location,
        privacy: data.acl,
        location: data.location,
        file_name: data.key,
        local_file_name: data.name,
        tag: body.tag,
        description: { content: "", status: "", version: "" },
      };

      let imageSaver = await this.saveMedia(fileObject, token);
      return imageSaver;
    } catch (err) {
      throw err;
    }
  }

  async deleteFile(files) {
    try {
      var s3bucket = new AWS.S3({
        accessKeyId: this.configService.get("UPLOADER_KEY"),
        secretAccessKey: this.configService.get("UPLOADER_SECRET"),
      });
      let Objects = [];
      files.forEach((ele) => {
        Objects.push({ Key: ele.location });
      });
      let params: AWS.S3.DeleteObjectsRequest = {
        Bucket: `${this.configService.get("UPLOADER_BUCKET_NAME")}`,
        Delete: { Objects },
      };
      let data = await s3bucket.deleteObjects(params).promise();
      return data;
    } catch (err) {
      throw err;
    }
  }

  async deleteMedia(id, token) {
    const data = await this.mediaModel.findOneAndUpdate(
      { _id: id, created_by: token.id },
      { $set: { status: "Inactive" } }
    );
    if (!data) {
      return new HttpException("Media does not exist", HttpStatus.NOT_FOUND);
    }
    return { message: "Deleted Successfully" };
  }

  async getList(token) {
    try {
      let data = await this.mediaModel.find({
        created_by: token.id,
        status: "Active",
      });
      return data;
    } catch (err) {
      throw err;
    }
  }
  async getTagMedia(tag, token) {
    try {

      let data = await this.mediaModel.find({ tag: { $in: tag } });
      return { data: data };
    } catch (err) {
      throw err;
    }
  }
  async getMediaLocationById(media_id: string) {
    try {
      let media = await this.mediaModel.findOne({ _id: media_id });
      return media?.location;
    } catch (err) {
      throw err;
    }
  }
}
