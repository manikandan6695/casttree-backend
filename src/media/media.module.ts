import { AuthModule } from "./../auth/auth.module";
import { SharedModule } from "./../shared/shared.module";
import { MongooseModule } from "@nestjs/mongoose";
import { Module } from "@nestjs/common";
import { MediaController } from "./media.controller";
import { MediaService } from "./media.service";
import { MediaSchema } from "./schema/media.schema";
import { UploaderService } from "src/uploader/uploader.service";
import { LoggerModule } from "src/logger/logger.module";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "media", schema: MediaSchema }]),
    LoggerModule,
    SharedModule,
    AuthModule,
  ],
  controllers: [MediaController],
  providers: [MediaService, UploaderService],
  exports: [MediaService],
})
export class MediaModule {}
