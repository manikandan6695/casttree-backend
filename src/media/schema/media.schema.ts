import * as mongoose from "mongoose";

export interface IDescriptionModel {
  content?: string;
  status?: string;
  version?: string;
}
export interface IMedia {
  type?: string;
  media_id?: any;
  visibility?: string;
}
export interface IMediaModel extends mongoose.Document {
  media_type?: string;
  media_format?: string;
  media_size?: number;
  media_url?: string;
  privacy?: string;
  description: IDescriptionModel[];
  location?: string;
  file_name?: string;
  local_file_name?: string;
  status?: string;
  tag?: string[];
  created_by?: any;
  updated_by?: any;
}
export const DescriptionSchema = new mongoose.Schema(
  {
    content: { type: String },
    status: { type: String, default: "Active" },
    version: { type: String },
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

export const MediaSchema = new mongoose.Schema(
  {
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
    },
    updated_by: {
      type: mongoose.Schema.Types.ObjectId,
    },
    media_type: {
      type: String,
    },
    media_format: {
      type: String,
    },
    media_size: {
      type: String,
    },
    media_url: {
      type: String,
    },
    privacy: {
      type: String,
      default: "Public",
    },
    tag: [
      {
        type: String,
      },
    ],
    description: [DescriptionSchema],
    location: {
      type: String,
    },
    file_name: {
      type: String,
    },
    local_file_name: {
      type: String,
    },
    status: {
      type: String,
      default: "Active",
    },
  },
  {
    collection: "media",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
