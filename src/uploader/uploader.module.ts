import { Module } from '@nestjs/common';
import { LoggerModule } from 'src/logger/logger.module';
import { UploaderService } from './uploader.service';

@Module({
  imports: [LoggerModule],
  providers: [UploaderService],
  exports: [UploaderService],
})
export class UploaderModule {}
