import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { IPaymentModel } from "./schema/payment.schema";

@Injectable()
export class PaymentRequestService {
  constructor(
    @InjectModel("payment")
    private readonly paymentModel: Model<IPaymentModel>,
    private sharedService: SharedService
  ) {}

  async createPaymentRequest(body, token) {
    try {
      let fv = {
        ...body,
        created_by: token.id,
        user_id: token.id,
      };
      let payment_sequence = await this.sharedService.getNextNumber(
        "payment",
        token.id,
        "PMT",
        5,
        null
      );
      let payment_number = payment_sequence.toString();
      let payment_document_number = payment_number.padStart(5, "0");
      fv["doc_id_gen_type"] = body.doc_id_gen_type;
      fv["payment_document_number"] = payment_document_number;
      fv["document_number"] = payment_document_number;
      let data = await this.paymentModel.create(fv);
      return data;
    } catch (err) {
      throw err;
    }
  }

  async updatePaymentRequest(body, token) {
    try {
      await this.paymentModel.updateOne(
        { _id: body.id },
        {
          $set: { document_status: body.document_status, updated_by: token.id },
        }
      );
      return { message: "Updated Successfully" };
    } catch (err) {
      throw err;
    }
  }
}
