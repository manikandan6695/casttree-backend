import { Module } from '@nestjs/common';
import { PaymentRequestService } from './payment-request.service';
import { PaymentRequestController } from './payment-request.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PaymentSchema } from './schema/payment.schema';
import { SharedModule } from 'src/shared/shared.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "payment", schema: PaymentSchema },
    ]),
    SharedModule,
    AuthModule,
  ],
  providers: [PaymentRequestService],
  controllers: [PaymentRequestController],
  exports : [PaymentRequestService],
})
export class PaymentRequestModule {}
