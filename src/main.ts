import { NestFactory } from "@nestjs/core";
import * as bodyParser from "body-parser";
import "dotenv/config";
import helmet from "helmet";
import { AppModule } from "./app.module";


const port = process.env.SERVER_PORT || 3000;
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.use((req, res, next) => {
    res.header("Access-Control-Allow-Headers", "application-key");
    next();
  });
  app.use(bodyParser.json({ limit: "50mb" }));
  app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
  app.setGlobalPrefix(process.env.API_PREFIX);
  app.enableCors({
    origin: "*", // process.env.CORS_ORIGIN,
  });
  await app.listen(port);
  console.log(`${new Date()}====>App started in port ${port}<====`);
}
bootstrap();


