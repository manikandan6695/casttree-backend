import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthModule } from "src/auth/auth.module";
import { EndorsementModule } from "src/endorsement/endorsement.module";
import { ProjectModule } from "src/project/project.module";
import { SharedModule } from "src/shared/shared.module";
import { ProfileController } from "./profile.controller";
import { ProfileService } from "./profile.service";
import { profileSchema } from "./schema/profile.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "profile", schema: profileSchema }]),
    SharedModule,
    AuthModule,
    EndorsementModule,
    ProjectModule,
  ],
  controllers: [ProfileController],
  providers: [ProfileService],
  exports: [ProfileService],
})
export class ProfileModule {}
