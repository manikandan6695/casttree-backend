import { HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { EndorsementService } from "src/endorsement/endorsement.service";
import { ProjectService } from "src/project/project.service";
import { AppException } from "src/shared/app-exception";
import { SharedService } from "src/shared/shared.service";
import { IProfileModel } from "./schema/profile.schema";
const { ObjectId } = require("mongodb");
@Injectable()
export class ProfileService {
  constructor(
    @InjectModel("profile")
    private readonly profileModel: Model<IProfileModel>,
    private endorsementService: EndorsementService,
    private projectService: ProjectService,
    private shared_service: SharedService
  ) {}

  async submitProfile(body, token) {
    try {
      let profile = await this.profileModel.findOne({ userId: token.id });
      if (profile) {
        throw new AppException(
          "Profile already exists",
          HttpStatus.NOT_ACCEPTABLE
        );
      }
      let fv = {
        ...body,
        createdBy: token.id,
        updatedBy: token.id,
      };
      await this.profileModel.create(fv);
      return { message: "Submitted Successfully" };
    } catch (err) {
      throw err;
    }
  }
  async getProfile(id, token) {
    try {
      let data = await this.profileModel
        .findOne({ _id: id })
        .populate("roles")
        .populate("skills")
        .populate("documents.media_id")
        .populate("workExperience")
        .populate("media.media_id")
        .lean();
      if (data) {
        let endorsement = await this.endorsementService.getEndorsement(
          token.id
        );
        let project = await this.projectService.getProjects(token);
        data["endorsement"] = endorsement;
        data["project"] = project;
      }
      return data;
    } catch (err) {
      throw err;
    }
  }

  async getSuggestions(body, token) {
    try {
      let aggregation_pipeline = [];
      aggregation_pipeline.push(
        {
          $lookup: {
            from: "user",
            localField: "userId",
            foreignField: "_id",
            as: "userId",
          },
        },
        {
          $unwind: { path: "$userId" },
        }
      );
      if (body.city) {
        aggregation_pipeline.push({
          $match: {
            "userId.city": new ObjectId(body.city),
          },
        });
      }

      if (body.skills) {
        let data = body.skills.map((e) => new ObjectId(e));
        aggregation_pipeline.push({
          $match: {
            skills: data,
          },
        });
      }

      aggregation_pipeline.push(
        {
          $lookup: {
            from: "role",
            localField: "roles",
            foreignField: "_id",
            as: "roles",
          },
        },
        {
          $unwind: {
            path: "$roles",
            preserveNullAndEmptyArrays: true,
          },
        }
      );

      aggregation_pipeline.push(
        {
          $lookup: {
            from: "media",
            localField: "media.media_id",
            foreignField: "_id",
            as: "media.media_id",
          },
        },
        {
          $unwind: {
            path: "$media.media_id",
            preserveNullAndEmptyArrays: true,
          },
        }
      );
      aggregation_pipeline.push({
        $lookup: {
          from: "skills",
          localField: "skills",
          foreignField: "_id",
          as: "skills",
        },
      });
      let countPipe = [...aggregation_pipeline];
      aggregation_pipeline.push(
        {
          $sort: {
            _id: -1,
          },
        },
        {
          $skip: body.skip,
        },
        {
          $limit: body.limit,
        }
      );
      countPipe.push({
        $group: {
          _id: null,
          count: { $sum: 1 },
        },
      });
      let profileData = await this.profileModel.aggregate(aggregation_pipeline);
      let total_count = await this.profileModel.aggregate(countPipe);

      let count;
      if (total_count.length) {
        count = total_count[0].count;
      }
      return { profileData, count };
    } catch (err) {
      throw err;
    }
  }

  async updateProfile(id, body, token) {
    try {
      if (body.endorsement.length) {
        await this.endorsementService.addEndorsement(body.endorsement, token);
      }
      if (body.project.length) {
        await this.projectService.saveProject(body.project, token);
      }
      await this.profileModel.updateOne({ _id: id }, { $set: body });
      return { message: "Updated Successfully" };
    } catch (err) {
      throw err;
    }
  }
}
