import * as mongoose from "mongoose";
import { IMedia } from "src/media/schema/media.schema";
import { IUserModel, MediaSchema } from "src/user/schema/user.schema";
export interface IEducationModel {
  school: string;
  degree: string;
  fieldOfStudy: string;
  startDate: Date;
  endDate: Date;
  description: string;
}
export interface IWorkExperienceModel {
  roleTitle: string;
  employmentType: string;
  companyName: string;
  location: string;
  startDate: Date;
  endDate: Date;
  roleDescription: string;
}
export interface ILanguageModel {
  languageName: string;
  ability: string[];
}

export interface ISocialMediaModel {
  socialMediaType: string;
  socialMediaLink: string;
}
export interface IAwardModel {
  title: string;
  issuerName: string;
  issueDate: Date;
  description: string;
  projectId: string;
}
export interface IProfileModel extends mongoose.Document {
  userId: string | IUserModel;
  roles: string[];
  skills: string[];
  media: IMedia[];
  about: string;
  documents : IMedia[];
  language: ILanguageModel[];
  education: IEducationModel[];
  socialMedia: ISocialMediaModel[];
  workExperience: IWorkExperienceModel[];
  createdBy: string | IUserModel;
  updatedBy: string | IUserModel;
}

export const languageSchema = new mongoose.Schema<any>({
  languageName: {
    type: String,
  },
  ability: [
    {
      type: String,
    },
  ],
});

export const awardsSchema = new mongoose.Schema<any>({
  title: {
    type: String,
  },
  issuerName: {
    type: String,
  },
  issueDate: {
    type: Date,
  },
  description: {
    type: String,
  },
  media: [MediaSchema],
});
export const workExperienceSchema = new mongoose.Schema<any>({
  roleTitle: {
    type: String,
  },
  employmentType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "employmentType",
  },
  companyName: {
    type: String,
  },
  location: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "city",
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  roleDescription: {
    type: String,
  },
});
export const educationSchema = new mongoose.Schema<any>({
  school: {
    type: String,
  },
  degree: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "degree",
  },
  fieldOfStudy: {
    type: String,
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  description: {
    type: String,
  },
});

export const awardSchema = new mongoose.Schema<any>({
  title: {
    type: String,
  },
  issuerName: {
    type: String,
  },
  issueDate: {
    type: Date,
  },
  description: {
    type: Date,
  },
  projectId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "project",
  },
});

export const socialMediaSchema = new mongoose.Schema<any>({
  platform: {
    type: String,
  },
  link: {
    type: String,
  },
  description: {
    type: String,
  },
});

export const profileSchema = new mongoose.Schema<any>(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    about: {
      type: String,
    },
    language: [languageSchema],
    education: [educationSchema],
    roles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "category",
      },
    ],
    workExperience: [workExperienceSchema],
    skills: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "skills",
      },
    ],
    documents : [MediaSchema],
    media: [MediaSchema],
    awards: [awardSchema],
    socialMedia: [socialMediaSchema],
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    updatedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  {
    collection: "profile",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
