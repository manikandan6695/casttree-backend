import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Res,
  UseGuards,
  UseInterceptors,
  ValidationPipe,
} from "@nestjs/common";
import { Response } from "express";
import { GetFilters } from "src/auth/decorators/param/get-filter.decorator";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { SharedService } from "src/shared/shared.service";
import { UserToken } from "src/user/dto/usertoken.dto";
import { DoAuthorityCheck } from "./../auth/decorators/docheck.decorator";
import { GetOrganization } from "./../auth/decorators/param/get-organization.decorator";
import {
  AttrType,
  RequestDetails,
} from "./../auth/decorators/request-details.decorator";
import {
  EActionType,
  EUserActions,
  UserActionType,
} from "./../auth/decorators/user-action.decorator";
import { CategoryService } from "./category.service";
import { CategorySearchDTO } from "./dto/category-search.dto";

@Controller("category")
export class CategoryController {
  constructor(
    private sservice: SharedService,
    private cservice: CategoryService
  ) {}

 
  @Post("get-category-list")
  async getCategoryList(
    @GetFilters() filters: any,
    @Res() res: Response,
    @Body() body: CategorySearchDTO
  ) {
    try {
      let data = await this.cservice.getCategoryList(body, filters);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @Get("get-list/:type")
  async getCategoryListByType(
    @Param("type") type: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.cservice.getCategoriesByType(type);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
