import { Type } from "class-transformer";
import {
  IsArray,
  IsEnum,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
import { RefMediaDTO } from "src/media/dto/media.dto";
import { EStatus } from "src/shared/enum/privacy.enum";

export class CategoryCharacteristicDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsMongoId()
  value: string;
}
export class CategoryDTO {
  @IsNotEmpty()
  @IsString()
  category_name: string;

  @IsOptional()
  @IsString()
  seo_url?: string;

  @IsNotEmpty()
  @IsString()
  category_type: string;

  @IsOptional()
  @IsString()
  category_desc: string;

  @IsOptional()
  @IsMongoId()
  parent_category: string;

  @IsOptional()
  @IsString()
  category_icon: string;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CategoryCharacteristicDto)
  category_characteristic: [CategoryCharacteristicDto];

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => RefMediaDTO)
  media: RefMediaDTO[];

  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  article?: string[];

  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  tags?: string[];

  @IsOptional()
  @IsString()
  @IsEnum(EStatus)
  status?: EStatus;
}
