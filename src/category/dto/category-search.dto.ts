import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CategorySearchDTO {
  @IsOptional()
  @IsString()
  search: string;

  @IsOptional()
  @IsString()
  parent_category: string;

  @IsNotEmpty()
  @IsString()
  category_type: string;

  @IsOptional()
  @IsString()
  type: string;
}
