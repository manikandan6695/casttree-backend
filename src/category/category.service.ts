import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { time } from "console";
import { Model } from "mongoose";
import { RefMediaDTO } from "src/media/dto/media.dto";
import { EStatus } from "src/shared/enum/privacy.enum";
import { SharedService } from "src/shared/shared.service";
import { UserToken } from "./../user/dto/usertoken.dto";
import { CategorySearchDTO } from "./dto/category-search.dto";
import { CategoryDTO } from "./dto/category.dto";
import { ICategoryModel } from "./schema/category.schema";

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel("category")
    private readonly categoryModel: Model<ICategoryModel>,
    private shared_service: SharedService
  ) {}

  async saveCategory(
    body: CategoryDTO,
    token: UserToken,
    organization_id: string,
    dpi: string
  ) {
    try {
      let fv = {
        organization_id,
        ...body,
        created_by: token.id,
        updated_by: token.id,
      };
      if (fv.hasOwnProperty("media")) {
        let gdata = this.checkGalleryExist(fv.media, dpi);
        if (fv.media == null) {
          fv.media = gdata;
        }
      }

      if (!fv.seo_url) {
        fv.seo_url = this.shared_service.formatText(body.category_name, "-");
      }
      fv.parent_category = fv.parent_category || null;
      let data = await this.categoryModel.create(fv);
      return data;
    } catch (err) {
      throw err;
    }
  }
  checkGalleryExist(media: RefMediaDTO[], dpi) {
    try {
      if (media && !media.length) {
        media.push({ type: "gallery", media_id: dpi });
      } else if (media && media.length) {
        let x = media.find((e) => e.type == "gallery");
        if (!x) {
          media.push({ type: "gallery", media_id: dpi });
        }
      } else {
        media = [{ type: "gallery", media_id: dpi }];
      }
      return media;
    } catch (err) {
      throw err;
    }
  }
  async updateCategory(
    token: UserToken,
    body: CategoryDTO,
    id: string,
    dpi: string
  ) {
    try {
      body["updated_by"] = token.id;
      if (body.hasOwnProperty("media")) {
        let gdata = this.checkGalleryExist(body.media, dpi);
        if (body.media == null) {
          body.media = gdata;
        }
      }
      if (!body.seo_url) {
        body.seo_url = this.shared_service.formatText(body.category_name, "-");
      }
      body.parent_category = body.parent_category || null;
      let data = await this.categoryModel.findByIdAndUpdate(id, body, {
        new: true,
      });
      return data;
    } catch (err) {
      throw err;
    }
  }

  async getCategoryList(body: CategorySearchDTO, filters: any) {
    try {
      let filter = {
        status: EStatus.Active,
        category_type: body.category_type,
      };
      if (body.search) {
        filter["category_name"] = body.search;
      }
      let data = await this.categoryModel
        .find(filter)
        .populate("media.media_id")
        .populate("created_by", "first_name last_name");
      return { data };
    } catch (err) {
      throw err;
    }
  }

  async getPaymentTerms(body) {
    try {
      let filter = {};
      filter["status"] = "Active";
      let data = await this.categoryModel
        .find(filter, { category_characteristic: 1 })
        .sort("-created_at");

      return data;
    } catch (err) {
      throw err;
    }
  }

  async getCategories(
    organization_id: string,
    category_type: string,
    parent_category: string,
    skip?,
    limit?
  ) {
    try {
      let skp = skip * limit;
      let filter = {
        organization_id,
        category_type,
        parent_category,
        status: EStatus.Active,
      };
      let data = await this.categoryModel
        .find(filter)
        .populate({
          path: "media.media_id",
          model: "media",
          select: "location",
        })
        .skip(skp || 0)
        .limit(+limit || 100)
        .populate("child_category");
      if (!data.length) {
        let parent_of_parent = await this.categoryModel.findOne({
          _id: filter.parent_category,
          organization_id,
          category_type,
          status: EStatus.Active,
        });
        filter.parent_category = parent_of_parent.parent_category;
        parent_category = parent_of_parent.parent_category;
        data = await this.categoryModel
          .find(filter)
          .populate({
            path: "media.media_id",
            model: "media",
            select: "location",
          })
          .skip(skp || 0)
          .limit(+limit || 100)
          .populate("child_category");
      }
      let count = await this.categoryModel.find(filter).countDocuments();
      let parent_data;
      if (parent_category) {
        parent_data = await this.categoryModel
          .findById(parent_category, "category_name parent_category media.type")
          .populate({
            path: "media.media_id",
            model: "media",
            select: "location",
          })
          .populate({
            path: "parent_category",
            populate: {
              path: "media.media_id",
              model: "media",
              select: "location",
            },
            select: "category_name media.type",
          });
      }
      return { data, count, parent_data };
    } catch (err) {
      throw err;
    }
  }
  async getCategoryById(id: string) {
    try {
      let data = await this.categoryModel.findOne({ _id: id });
      return data;
    } catch (err) {
      throw err;
    }
  }
  async getAncestorsCategory(parent_category: string, sub_category: string) {
    try {
      let result: string[] = [];
      if (!parent_category && !sub_category) return result;
      if (sub_category) result.push(sub_category);
      let category = await this.categoryModel.findById(parent_category);
      result.push(category._id);
      /*
        fashion
          womens
            tops
              salvar - parent
      */
      while (category.parent_category) {
        result.push(category.parent_category);
        category = await this.categoryModel.findById(category.parent_category);
      }
      result = Array.from(new Set(result));
      return result;
    } catch (err) {
      throw err;
    }
  }

  async getCategoriesByType(type: string) {
    try {
      let sort;

      if (type == "incoterm" || "processes-and-documentation") {
        // console.log("inside if is", type);

        sort = { order: 1 };
      } else {
        // console.log("inside else");
        sort = { _id: -1 };
      }

      let data = await this.categoryModel
        .find(
          {
            category_type: type,
            status: EStatus.Active,
          },
          { category_name: 1, category_value: 1, category_type: 1 }
        )
        .sort(sort);

      return data;
    } catch (err) {
      throw err;
    }
  }
}
