import * as mongoose from "mongoose";
import { IMedia } from "src/media/schema/media.schema";
import { ESStatus, EStatus } from "src/shared/enum/privacy.enum";
import { MediaSchema } from "src/user/schema/user.schema";

export interface ICategoryCharactericticModel {
  name?: string;
  value?: any;
}
export const CategoryCharacteristicSchema = new mongoose.Schema<any>({
  name: { type: String, example: "football" },
  value: { type: mongoose.Schema.Types.Mixed, example: 15 },
});

export interface ICategoryModel extends mongoose.Document {
  category_name: string;
  category_value: string;
  category_type: string;
  category_desc?: string;
  parent_category?: string;
  media: IMedia[];
  tags?: any[];
  status?: EStatus;
  created_by: any;
  updated_by: any;
}
const ObjectId = mongoose.Schema.Types.ObjectId;
export const CategorySchema = new mongoose.Schema<any>(
  {
    category_name: { type: String },
    category_value: { type: String },
    category_type: { type: String },
    category_desc: { type: String },
    media: [MediaSchema],
    parent_category: { type: mongoose.Schema.Types.ObjectId, ref: "category" },
    category_icon: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    updated_by: { type: ObjectId, ref: "user" },
    status: { type: String, enum: ESStatus, default: "Active" },
  },
  {
    collection: "category",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);

CategorySchema.virtual("child_category", {
  ref: "category",
  localField: "_id",
  foreignField: "parent_category",
});

CategorySchema.set("toJSON", { virtuals: true });
