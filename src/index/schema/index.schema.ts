import * as mongoose from "mongoose";
import { ESStatus } from "src/shared/enum/privacy.enum";

export interface IIndexModel extends mongoose.Document {
  organization_id: any;
  source_id: any;
  source_type: string;
  name: string;
  description: string;
  meta: any;
  status: string;
}
export const IndexSchema = new mongoose.Schema(
  {
    organization_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "organization",
    },
    source_id: {
      type: mongoose.Schema.Types.ObjectId,
    },
    source_type: {
      type: String,
    },
    name: {
      type: String,
    },
    description: {
      type: String,
    },
    meta: {
      type: mongoose.Schema.Types.Mixed,
    },
    status: { type: String, enum: ESStatus, default: "Active" },
  },
  {
    collection: "index",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
