export class CreateIndexDto {
  organization_id: string;
  source_id: string;
  source_type: string;
  name: string;
  description?: string;
  meta?: any;
  status?: string;
}
