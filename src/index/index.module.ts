import { Module } from "@nestjs/common";
import { IndexService } from "./index.service";
import { IndexController } from "./index.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { IndexSchema } from "./schema/index.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "index", schema: IndexSchema }]),
  ],
  controllers: [IndexController],
  providers: [IndexService],
  exports: [IndexService],
})
export class IndexModule {}
