import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateIndexDto } from "./dto/create-index.dto";
import { IIndexModel } from "./schema/index.schema";

@Injectable()
export class IndexService {
  constructor(
    @InjectModel("index")
    private readonly index_model: Model<IIndexModel>
  ) {}
  async create(body: CreateIndexDto) {
    try {
      let prev_data = await this.index_model.findOne({
        organization_id: body.organization_id,
        source_id: body.source_id,
        source_type: body.source_type,
      });
      if (!prev_data) {
        await this.index_model.create(body);
      } else {
        await this.index_model.updateOne({ _id: prev_data._id }, body);
      }
    } catch (e) {
      throw e;
    }
  }
  getIndexDetail(
    organization_id: string,
    source_id: string,
    source_type: string
  ) {
    try {
      return this.index_model.findOne({
        organization_id,
        source_id,
        source_type,
      });
    } catch (err) {
      throw err;
    }
  }
}
