// import { HttpModule, Module } from "@nestjs/common";
// import { MailerService } from "./mailer.service";
// import { BullModule } from "@nestjs/bull";
// import { ConfigService } from "@nestjs/config";
// import { MailConsumer } from "./mail.consumer";
// import { ConfigModule } from "nestjs-config";
// import { LoggerModule } from "src/logger/logger.module";
// import { MongooseModule } from "@nestjs/mongoose";
// import { ApplicationCredentialSchema } from "src/service-provider/schema/app-credential.schema";
// import { DefaultApplicationCredentialSchema } from "src/service-provider/schema/default-app-credential.schema";

// @Module({
//   imports: [
//     BullModule.registerQueueAsync({
//       name: "mail",
//       imports: [ConfigModule],
//       useFactory: async (configService: ConfigService) => {
//         return {
//           redis: {
//             host: "localhost",
//             port: 6379,
//           },
//         };
//       },
//       inject: [ConfigService],
//     }),
//     MongooseModule.forFeature([
//       { name: "applicationCredential", schema: ApplicationCredentialSchema },
//       {
//         name: "default-application-credential",
//         schema: DefaultApplicationCredentialSchema,
//       },
//     ]),
//     LoggerModule,
//     HttpModule,
//   ],
//   providers: [MailerService, MailConsumer],
//   exports: [MailerService],
// })
// export class MailerModule {}
