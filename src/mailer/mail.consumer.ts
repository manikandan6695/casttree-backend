// import { Process, Processor } from "@nestjs/bull";
// import { HttpService } from "@nestjs/common";
// import { ConfigService } from "@nestjs/config";
// import * as AWS from "aws-sdk";
// import { Job } from "bull";
// import * as ejs from "ejs";
// import { CustomLogger } from "src/logger/customlogger.service";
// import { EMailerType } from "./enum/mailer-type.enum";
// var ses = require("node-ses");
// var fs = require("fs");

// @Processor("mail")
// export class MailConsumer {
//   transporter: any;
//   type: string = "";
//   constructor(
//     private configService: ConfigService,
//     private logger: CustomLogger,
//     private http_service: HttpService
//   ) {
//     // this.type = configService.get("MAILING_TYPE");
//     // if (this.type == "SES") {
//     //   this.transporter = ses.createClient({
//     //     key: configService.get("EMAIL_KEY"),
//     //     secret: configService.get("EMAIL_SECRET"),
//     //   });
//     // } else {
//     //   this.transporter = nodemailer.createTransport({
//     //     service: configService.get("MAILER_SERVICE"),
//     //     auth: {
//     //       user: configService.get("MAILER_USER"),
//     //       pass: configService.get("MAILER_PASSWORD"),
//     //     },
//     //   });
//     // }
//   }

//   @Process("sample")
//   async sampleMail(job: Job) {
//     try {
//       let data = job.data;
//       var htmlContent = fs.readFileSync(
//         `${__dirname}/views/${data.template}`,
//         "utf8"
//       );
//       let html = ejs.render(
//         htmlContent,
//         { data: data.inputdata },
//         { async: false }
//       );
//       let mailConfig = {
//         from: this.configService.get("MAILER_USER"),
//         to: data.to,
//         subject: data.subject,
//       };
//       let info;
//       if (this.type == "SES") {
//         mailConfig["message"] = html;
//         info = await new Promise((resolve, reject) => {
//           this.transporter.sendMail(mailConfig, function(err, data, res) {
//             if (err) {
//               reject();
//             }
//             resolve({ message: "mail sent", data, res });
//           });
//         });
//       } else {
//         mailConfig["html"] = html;
//         info = await this.transporter.sendMail(mailConfig);
//       }
//       this.logger.log(info, "Mailer Response");
//       return info;
//     } catch (err) {
//       console.log("err", err);
//       this.logger.error(err, { label: "Mailer Response" });
//     }
//   }
//   @Process("forgotpassword")
//   async sendForgotPasswordMail(job) {
//     try {
//       let data = job;
//       var htmlContent = fs.readFileSync(
//         `${__dirname}/views/${data.template}`,
//         "utf8"
//       );
//       let html = ejs.render(
//         htmlContent,
//         { data: data.inputdata },
//         { async: false }
//       );
//       let info = await this.transporter.sendMail({
//         from: process.env.MAILER_FROM
//           ? process.env.MAILER_FROM
//           : process.env.MAILER_USER,
//         to: data.to,
//         subject: data.subject,
//         html,
//       });
//       this.logger.log(info, "Mailer Response");
//       return info;
//     } catch (err) {
//       console.log("err", err);
//       this.logger.error(err, { label: "Mailer Response" });
//     }
//   }

//   @Process("user-invite")
//   async sendInvitation(job) {
//     try {
//       let data = job;
//       var htmlContent = fs.readFileSync(
//         `${__dirname}/views/${data.template}`,
//         "utf8"
//       );
//       let html = ejs.render(
//         htmlContent,
//         { data: data.inputdata },
//         { async: false }
//       );
//       let info = await this.transporter.sendMail({
//         from: process.env.MAILER_FROM
//           ? process.env.MAILER_FROM
//           : process.env.MAILER_USER,
//         to: data.to,
//         subject: data.subject,
//         html,
//       });
//       this.logger.log(info, "Mailer Response");
//       return info;
//     } catch (err) {
//       console.log("err", err);
//       this.logger.error(err, { label: "Mailer Response" });
//     }
//   }

//   @Process("bid-status")
//   async sendBidInvitaitonMail(job) {
//     try {
//       let data = job;
//       var htmlContent = fs.readFileSync(
//         `${__dirname}/views/${data.template}`,
//         "utf8"
//       );
//       let html = ejs.render(
//         htmlContent,
//         { data: data.inputdata },
//         { async: false }
//       );
//       let info = await this.transporter.sendMail({
//         from: process.env.MAILER_FROM
//           ? process.env.MAILER_FROM
//           : process.env.MAILER_USER,
//         to: data.to,
//         subject: data.subject,
//         html,
//       });
//       this.logger.log(info, "Mailer Response");
//       return info;
//     } catch (err) {
//       console.log("err", err);
//       this.logger.error(err, { label: "Mailer Response" });
//     }
//   }

//   @Process("send-email")
//   async sendEmail(job) {
//     try {
//       let data = job.data;
//       var htmlContent = fs.readFileSync(
//         `${__dirname}/views/${data.template}`,
//         "utf8"
//       );
//       let html = ejs.render(
//         htmlContent,
//         { data: data.inputdata },
//         { async: false }
//       );
//       let info = await this.transporter.sendMail({
//         from: process.env.MAILER_FROM
//           ? process.env.MAILER_FROM
//           : process.env.MAILER_USER,
//         to: data.to,
//         subject: data.subject,
//         html,
//       });
//       this.logger.log(info, "Mailer Response");
//       return info;
//     } catch (err) {
//       this.logger.error(err, { label: "Mailer Response" });
//     }
//   }

//   @Process("send-login-otp")
//   async sendLoginOTP(job, transporter, app_provider, mail_provider_values) {
//     try {
//       let data = job;
//       var htmlContent = fs.readFileSync(
//         `${__dirname}/views/${data.template}`,
//         "utf8"
//       );
//       let html = ejs.render(htmlContent, { data: data }, { async: false });
//       if (app_provider == EMailerType.gmail) {
//         let info = await transporter.sendMail({
//           from: transporter.options.auth.user,
//           to: data.email,
//           subject: data.subject,
//           html,
//         });
//         this.logger.log(info, "Mailer Response");
//       }
//       if (app_provider == EMailerType.ninja) {
//         let body = [
//           {
//             gatewayType: "EMAIL",
//             templateName: "TECXPRT-NINJA-OTP",
//             entitiesMap: {
//               OTP: data.otp + "",
//             },
//             sender: "no-reply@ninjacart.com",
//             destinations: [
//               {
//                 destination: data.email,
//               },
//             ],
//             subject: data.subject,
//           },
//         ];
//         let nina_mailer_res = await this.http_service
//           .post(
//             `http://${this.configService.get(
//               "NINJA_MAILER_ENGINE_BASE_URL"
//             )}/mercury/notification`,
//             body
//           )
//           .toPromise();
//         this.logger.log(
//           `http://${this.configService.get(
//             "NINJA_MAILER_ENGINE_BASE_URL"
//           )}/mercury/notification`,
//           "Mailer Response"
//         );
//         this.logger.log(nina_mailer_res.data, "Mailer Response");
//         this.logger.log(JSON.stringify(body), "Mailer Response");
//       }
//       if (app_provider == EMailerType.amazon_ses) {
//         const ses = new AWS.SES({
//           apiVersion: "2010-12-01",
//           accessKeyId: mail_provider_values["AMAZON_KEY"],
//           secretAccessKey: mail_provider_values["AMAZON_SECRET"],
//           region: mail_provider_values["REGION"],
//         });
//         let params = {
//           Source: mail_provider_values["EMAIL"],
//           Destination: {
//             ToAddresses: [data.email],
//           },
//           ReplyToAddresses: [],
//           Message: {
//             Body: {
//               Html: {
//                 Charset: "UTF-8",
//                 Data: html,
//               },
//               Text: {
//                 Charset: "UTF-8",
//                 Data: "",
//               },
//             },
//             Subject: {
//               Charset: "UTF-8",
//               Data: data.subject,
//             },
//           },
//         };
//         let info = await ses.sendEmail(params).promise();
//         this.logger.log(info, "Mailer Response");
//       }
//     } catch (err) {
//       console.log("err", err);
//     }
//   }

//   async sendMail(transporter, job, email, app_provider, region) {
//     try {
//       let data = job;
//       var htmlContent = fs.readFileSync(
//         `${__dirname}/views/${data.template}`,
//         "utf8"
//       );
//       let html = ejs.render(
//         htmlContent,
//         { data: data.inputdata },
//         { async: false }
//       );
//       if (app_provider == EMailerType.gmail) {
//         let info = await transporter.sendMail({
//           from: transporter.options.auth.user,
//           to: data.to,
//           subject: data.subject,
//           html,
//         });
//         this.logger.log(info, "Mailer Response");
//       }

//       if (app_provider == EMailerType.amazon_ses) {
//         const ses = new AWS.SES({
//           apiVersion: "2010-12-01",
//           accessKeyId: transporter.key,
//           secretAccessKey: transporter.secret,
//           region: region,
//         });

//         let params = {
//           Source: email,
//           Destination: {
//             ToAddresses: [data.to],
//           },
//           ReplyToAddresses: [],
//           Message: {
//             Body: {
//               Html: {
//                 Charset: "UTF-8",
//                 Data: html,
//               },
//               Text: {
//                 Charset: "UTF-8",
//                 Data: `${data.inputdata.content} `,
//               },
//             },
//             Subject: {
//               Charset: "UTF-8",
//               Data: data.subject,
//             },
//           },
//         };
//         let info = await ses.sendEmail(params).promise();
//         this.logger.log(info, "Mailer Response");
//       }
//     } catch (err) {
//       console.log("err", err);
//     }
//   }

//   async sendSalesDocumentMail(transporter, job, email, app_provider, region) {
//     try {
//       let data = job;
//       // var htmlContent = fs.readFileSync(
//       //   `${__dirname}/views/${data.template}`,
//       //   "utf8"
//       // );
//       // let html = ejs.render(
//       //   htmlContent,
//       //   { data: data.body },
//       //   { async: false }
//       // );

//       if (app_provider == EMailerType.gmail) {
//         let html = data.data;
//         let info = await transporter.sendMail({
//           from: transporter.options.auth.user,
//           cc: data.cc,
//           to: data.to,
//           subject: data.subject,
//           html,
//         });
//         this.logger.log(info, "Mailer Response");
//       }

//       if (app_provider == EMailerType.amazon_ses) {
//         const ses = new AWS.SES({
//           apiVersion: "2010-12-01",
//           accessKeyId: transporter.key,
//           secretAccessKey: transporter.secret,
//           region: region,
//         });
//         let params = {
//           Source: email,
//           Destination: {
//             ToAddresses: data.to,
//             CcAddresses: data.cc,
//           },
//           ReplyToAddresses: [],
//           Message: {
//             Body: {
//               Html: {
//                 Charset: "UTF-8",
//                 Data: data.data,
//               },
//               Text: {
//                 Charset: "UTF-8",
//                 Data: `${data.data} `,
//               },
//             },
//             Subject: {
//               Charset: "UTF-8",
//               Data: data.subject,
//             },
//           },
//         };
//         let info = await ses.sendEmail(params).promise();
//         this.logger.log(info, "Mailer Response");
//       }
//     } catch (err) {
//       console.log("err", err);
//     }
//   }
// }
