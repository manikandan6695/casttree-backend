export enum EMailerType {
  amazon_ses = "Amazon SES",
  gmail = "Gmail",
  ninja = "Ninja",
}
export enum EMailer {
  email = "",
}
export const ESMailerType = [
  EMailerType.amazon_ses,
  EMailerType.gmail,
  EMailerType.ninja,
];
