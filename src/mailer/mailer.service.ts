// import { InjectQueue } from "@nestjs/bull";
// import { HttpStatus, Injectable } from "@nestjs/common";
// import { InjectModel } from "@nestjs/mongoose";
// import { Queue } from "bull";
// import { Model } from "mongoose";
// import * as nodemailer from "nodemailer";
// import { CustomLogger } from "src/logger/customlogger.service";
// import { SendEmailModel } from "src/service-provider-helper/model/amazon-ses-envelop.model";
// import { ESPAppType } from "src/service-provider/enum/app-type.enum";
// import { IDefaultApplicationCredentialModel } from "src/service-provider/schema/default-app-credential.schema";
// import { AppException } from "src/shared/app-exception";
// import { SharedService } from "src/shared/shared.service";
// import { SendEMailFormat } from "./entity/mail-format";
// import { EMailerType } from "./enum/mailer-type.enum";
// import { MailConsumer } from "./mail.consumer";

// var ses = require("node-ses");
// @Injectable()
// export class MailerService {
//   /**
//    * TODO app_credential_model andp default_credential_model reference need to be changed
//    */
//   constructor(
//     @InjectQueue("mail") private mailQueue: Queue,
//     private logger: CustomLogger,
//     private mailConsumer: MailConsumer,
//     @InjectModel("default-application-credential")
//     private default_app_credential_model: Model<
//       IDefaultApplicationCredentialModel
//     >,
//     private readonly shared_service: SharedService
//   ) {}
//   sendSampleMail() {
//     this.mailQueue.add("sample", { message: "hello world" });
//     this.logger.log("Sending sample mail", "Mailer");
//   }

//   sendEmail(data: SendEMailFormat) {
//     this.mailQueue.add("send-email", data);
//     this.logger.verbose(data, "Mailer");
//   }

//   sendLoginOTP(
//     data: SendEmailModel,
//     mail_instance,
//     mail_instance_type,
//     mail_provider_values
//   ) {
//     this.mailConsumer.sendLoginOTP(
//       data,
//       mail_instance,
//       mail_instance_type,
//       mail_provider_values
//     );
//   }
//   sendForgotPasswordMail(data) {
//     // this.mailQueue.add("forgotpassword", data);
//     this.logger.log("Sending forgot password mail", "Mailer");
//     this.mailConsumer.sendForgotPasswordMail(data);
//   }
//   sendInvitation(data) {
//     this.mailConsumer.sendInvitation(data);
//   }

//   sendBidInvitaitonMail(data: SendEMailFormat) {
//     this.mailConsumer.sendBidInvitaitonMail(data);
//   }

//   async sendMail(data, type) {
//     let transporter = await this.getTransporter();
//     var REGION;
//     var EMAIL;
//     if (transporter.pg_type == EMailerType.amazon_ses) {
//       transporter.pg.field_values.forEach((e) => {
//         if (e.field_key == "REGION") {
//           REGION = this.shared_service.decryptMessage(e.field_value);
//         }
//         if (e.field_key == "EMAIL") {
//           EMAIL = this.shared_service.decryptMessage(e.field_value);
//         }
//       });
//     }
   
//   }

//   async getTransporter() {
//     let cred = await this.default_app_credential_model.findOne({
//       type_key: ESPAppType.email,
//       is_primary: true,
//     });
//     if (!cred)
//       throw new AppException("Email not configured", HttpStatus.NO_CONTENT);

//     let pg_instance;
//     let pg_type = cred.application_key;
//     switch (cred.application_key) {
//       case EMailerType.gmail:
//         var GMAIL_KEY;
//         var GMAIL_SECRET;

//         cred.field_values.forEach((e) => {
//           if (e.field_key == "GMAIL_KEY") {
//             GMAIL_KEY = this.shared_service.decryptMessage(e.field_value);
//           }
//           if (e.field_key == "GMAIL_SECRET") {
//             GMAIL_SECRET = this.shared_service.decryptMessage(e.field_value);
//           }
//         });

//         pg_instance = nodemailer.createTransport({
//           service: EMailerType.gmail,
//           auth: {
//             user: GMAIL_KEY,
//             pass: GMAIL_SECRET,
//           },
//         });
//         break;
//       case EMailerType.amazon_ses:
//         let AMAZON_KEY;
//         let AMAZON_SECRET;
//         let REGION;
//         cred.field_values.forEach((e) => {
//           if (e.field_key == "AMAZON_KEY") {
//             AMAZON_KEY = this.shared_service.decryptMessage(e.field_value);
//           }
//           if (e.field_key == "AMAZON_SECRET") {
//             AMAZON_SECRET = this.shared_service.decryptMessage(e.field_value);
//           }
//           if (e.field_key == "REGION") {
//             REGION = this.shared_service.decryptMessage(e.field_value);
//           }
//         });
//         pg_instance = ses.createClient({
//           key: AMAZON_KEY,
//           secret: AMAZON_SECRET,
//           region: REGION,
//         });
//         break;
//       default:
//         break;
//     }
//     return { pg_instance, pg_type, pg: cred };
//   }
// }
