export enum EDiscountType {
  percentage = "Percentage",
  value = "Value",
}

export const ESDiscountType = [EDiscountType.percentage, EDiscountType.value];
