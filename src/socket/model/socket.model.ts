import { Socket } from "socket.io";
import { UserToken } from "src/user/dto/usertoken.dto";

export class SocketModel {
  auction_id?: any;
  client?: Socket;
  decoded?: UserToken;
  organization_portal_name: string;
  productkey: string;
}
