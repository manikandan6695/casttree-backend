import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Room } from "socket.io-adapter";
import { Socket } from "socket.io";
var jwt = require("jsonwebtoken");

@Injectable()
export class SocketService {
  room_details: RoomDetails = {};
  socket_id_user_mapping = {};
  constructor(private configService: ConfigService) {}
  decodeSocketToken(bearerToken) {
    try {
      const decoded = jwt.verify(
        bearerToken,
        this.configService.get("JWT_SECRET")
      ) as any;
      return decoded;
    } catch (err) {
      throw err;
    }
  }
  getClientToken(client: Socket) {
    try {
      let token = client.handshake.query["token"];
      if (!token) throw new Error("Token not found");
      let decoded = this.decodeSocketToken(token);
      if (!decoded) throw new Error("Decoded failed");
      return decoded;
    } catch (err) {
      throw err;
    }
  }
  getRoomUserCount(client: Socket, roomId: string): number {
    let room: Room = client.nsp.adapter.rooms[roomId];
    if (!room) return 0;
    return room.length;
  }
  getRoomLiveUserCount(room_id: string) {
    try {
      let room_sockets = this.room_details[room_id];
      let room_socket_ids = [];
      if (room_sockets) {
        room_socket_ids = Object.keys(room_sockets);
      }
      let room_users = {};
      let room_user_ids = room_socket_ids.map((e) => {
        let user_id = this.socket_id_user_mapping[e];
        room_users[user_id] = ++room_users[user_id] || 1;
        return user_id;
      });

      return {
        live_users: room_socket_ids.length,
        room_user_ids,
        room_users,
      };
    } catch (err) {
      throw err;
    }
  }
  removeUserFromRoom(room_id: string, socket_id: string) {
    try {
      if (!socket_id || !room_id) return false;
      let room_users = this.room_details[room_id];
      if (room_users) {
        delete room_users[socket_id];
      }
    } catch (err) {
      throw err;
    }
  }
  addUserToRoom(room_id: string, socket_id: string) {
    try {
      if (!room_id || !socket_id) return false;
      if (this.room_details.hasOwnProperty(room_id)) {
        this.room_details[room_id][socket_id] = 1;
      } else {
        this.room_details[room_id] = {
          [socket_id]: 1,
        };
      }
    } catch (err) {
      throw err;
    }
  }
  addMapping(client: Socket) {
    let token = this.getClientToken(client);
    this.socket_id_user_mapping[client.id] = token.id;
  }
  removeMapping(client: Socket) {
    delete this.socket_id_user_mapping[client.id];
    Object.keys(this.room_details).forEach((room_id) => {
      delete this.room_details[room_id][client.id];
      client.broadcast.to(room_id).emit("ROOM_MEMBERS_UPDATED", {
        live_users: Object.keys(this.room_details[room_id]).length,
      });
    });
  }
}
export class RoomDetails {
  [key: string]: RoomUsers;
}
export class RoomUsers {
  [key: string]: number;
}
