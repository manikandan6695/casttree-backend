import { UseGuards } from "@nestjs/common";
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from "@nestjs/websockets";
import { Subject } from "rxjs";
import { Server, Socket } from "socket.io";
import { CustomLogger } from "src/logger/customlogger.service";
import { WsGuard } from "./guards/ws-jwt.guard";
import { SocketModel } from "./model/socket.model";
import { SocketService } from "./socket.service";

@WebSocketGateway({ path: "/tecxprt/socket" })
export class EventsGateway implements OnGatewayDisconnect, OnGatewayConnection {
  constructor(
    private custom_logger: CustomLogger,
    private socket_service: SocketService
  ) {}
  publisher = new Subject<SocketModel>();

  @WebSocketServer()
  io: Server;

  handleConnection(@ConnectedSocket() client: Socket) {
    try {
      this.socket_service.addMapping(client);
    } catch (err) {}
  }

  handleDisconnect(@ConnectedSocket() client: Socket) {
    try {
      this.socket_service.removeMapping(client);
    } catch (err) {}
  }

  async publishToRoom(room_id, event_name, data) {
    this.io.to(room_id).emit(event_name, data);
  }

  async updateAuctionBidStatus(auction, data) {
    this.io.to(auction).emit("auction_update_response", data);
  }

  @UseGuards(WsGuard)
  @SubscribeMessage("ENTER_AUCTION_ROOM")
  async enterAuctionRoom(
    @ConnectedSocket() client: Socket,
    @MessageBody() body: any
  ) {
    try {
      this.custom_logger.debug(
        `Enter auction room called ${JSON.stringify(body)}`,
        this.constructor.name
      );
      body["client"] = client;
      this.publisher.next(body);
    } catch (err) {
      client.send({
        event: "ENTER_AUCTION_ROOM_RESPONSE",
        status: "failed",
        data: { message: err.message },
      });
    }
  }

  @UseGuards(WsGuard)
  @SubscribeMessage("LEAVE_ROOM")
  async leaveRoom(@ConnectedSocket() client: Socket, @MessageBody() body: any) {
    try {
      await client.leave(body.room_id);
      this.socket_service.removeUserFromRoom(body.room_id, client.id);
      client.send({
        event: "LEAVE_ROOM_RESPONSE",
        status: "success",
      });
      let room_details = this.socket_service.getRoomLiveUserCount(body.room_id);
      client.broadcast.to(body.room_id).emit("ROOM_MEMBERS_UPDATED", {
        live_users: room_details.live_users,
      });
    } catch (err) {
      this.custom_logger.error(err, { label: this.constructor.name });
      client.send({
        event: "LEAVE_ROOM_RESPONSE",
        status: "failed",
        data: { message: err.message },
      });
    }
  }
}
