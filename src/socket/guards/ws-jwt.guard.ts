import { CanActivate, Injectable } from "@nestjs/common";
import { Observable } from "rxjs";
import { SocketService } from "../socket.service";
import { CustomLogger } from "src/logger/customlogger.service";

@Injectable()
export class WsGuard implements CanActivate {
  constructor(
    private custom_logger: CustomLogger,
    private socket_service: SocketService
  ) {}

  canActivate(
    context: any
  ): boolean | any | Promise<boolean | any> | Observable<boolean | any> {
    try {
      const bearerToken = context.args[0].handshake.query.token;
      const productkey = context.args[0].handshake.query.productkey;
      this.custom_logger.debug(
        `productkey got in query is ${productkey}`,
        this.constructor.name
      );
      if (!bearerToken) return false;
      let decoded = this.socket_service.decodeSocketToken(bearerToken);
      if (!decoded) return false;
      context.switchToWs().getData().decoded = decoded;
      context.switchToWs().getData().productkey = productkey;
      this.custom_logger.debug(
        `data formed in guard ${JSON.stringify(
          context.switchToWs().getData()
        )}`,
        this.constructor.name
      );
      return true;
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }
}
