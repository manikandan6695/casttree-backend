import { Module } from "@nestjs/common";
import { LoggerModule } from "src/logger/logger.module";
import { SharedModule } from "src/shared/shared.module";
import { EventsGateway } from "./socket.gateway";
import { SocketService } from "./socket.service";

@Module({
  imports: [LoggerModule,SharedModule],
  providers: [EventsGateway, SocketService],
  controllers: [],
  exports: [EventsGateway, SocketService],
})
export class SocketModule {}
