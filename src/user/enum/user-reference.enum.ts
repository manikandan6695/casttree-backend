export enum EUserReference {
  vendor = "vendor",
  user = "user",
  customer = "customer",
  employee = "employee",
}

export const ESUserReference = [
  EUserReference.vendor,
  EUserReference.user,
  EUserReference.employee,
  EUserReference.customer,
];
