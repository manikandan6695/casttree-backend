export enum EPhoneNumberType {
  primary = "Primary",
  secondary = "Secondary",
}

export const ESPhoneNumberType = [
  EPhoneNumberType.primary,
  EPhoneNumberType.secondary,
];
