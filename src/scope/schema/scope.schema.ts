import * as mongoose from "mongoose";

export interface IscopeInterface extends mongoose.Document {
  scope_id: number;
  scope_icon: string;
  scope_description: string;
  applicable_scope: [number];
}

export const scopeSchema = new mongoose.Schema(
  {
    scope_id: { type: "Number" },
    scope_icon: {
      type: "String",
    },
    scope_description: { type: "String" },
    applicable_scope: [{ type: "Number", _id: false }],
  },
  {
    collection: "scope",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
