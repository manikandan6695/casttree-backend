import { scopeSchema } from "./schema/scope.schema";
import { MongooseModule } from "@nestjs/mongoose";
import { Module } from "@nestjs/common";
import { ScopeController } from "./scope.controller";
import { ScopeService } from "./scope.service";
import { SharedModule } from "src/shared/shared.module";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "scope", schema: scopeSchema }]),
    SharedModule,
  ],
  controllers: [ScopeController],
  providers: [ScopeService],
  exports: [ScopeService],
})
export class ScopeModule {}
