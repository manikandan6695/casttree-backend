import { IscopeInterface } from "./schema/scope.schema";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Injectable } from "@nestjs/common";

@Injectable()
export class ScopeService {
  constructor(
    @InjectModel("scope") private readonly scope_model: Model<IscopeInterface>
  ) {}

  async createScope(body) {
    try {
      let data = await this.scope_model.create(body);
      return data;
    } catch (err) {
      throw err;
    }
  }

  async getScopeList() {
    try {
      let data = await this.scope_model.find();
      return data;
    } catch (err) {
      throw err;
    }
  }
}
