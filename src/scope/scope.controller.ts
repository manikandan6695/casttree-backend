import { SharedService } from "src/shared/shared.service";
import { Response } from "express";
import { ScopeService } from "./scope.service";
import { Body, Controller, Get, Post, Res, UseGuards } from "@nestjs/common";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";

@Controller("scope")
export class ScopeController {
  constructor(
    private readonly scope_service: ScopeService,
    private readonly sservice: SharedService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async roleCreate(@Res() res: Response, @Body() body: any) {
    try {
      let data = await this.scope_service.createScope(body);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async getScopeList(@Res() res: Response) {
    try {
      let data = await this.scope_service.getScopeList();
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
