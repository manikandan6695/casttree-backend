import { Type } from "class-transformer";
import {
  IsArray,
  IsEmail,
  IsISO8601,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
export class NominationsDTO {
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => NomineesDTO)
  nominations: NomineesDTO[];
}
export class UserDetailDTO {
  @IsNotEmpty()
  @IsString()
  phoneCountryCode: string;

  @IsNotEmpty()
  @IsString()
  phoneNumber: string;

  @IsOptional()
  @IsString()
  emailId: string;

  @IsNotEmpty()
  @IsString()
  userName: string;
}
export class NomineesDTO {
  @IsNotEmpty()
  @IsMongoId()
  projectId: string;

  @IsNotEmpty()
  @IsMongoId()
  applicationId: string;

  @IsNotEmpty()
  @IsMongoId()
  awardId: string;

  @IsNotEmpty()
  @IsString()
  nomineeName: string;

  @IsNotEmpty()
  @IsString()
  nomineePhoneNumber: string;

  @IsOptional()
  @IsString()
  nomineeEmail: string;

  @IsNotEmpty()
  @IsString()
  nomineeDescription: string;

  @IsNotEmpty()
  @IsString()
  status: string;

  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => UserDetailDTO)
  userDetail: UserDetailDTO;
}
