import { NominationsDTO } from "./dto/nominations.dto";
import { INominationsModel } from "./schema/nominations.schema";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { OnEvent } from "@nestjs/event-emitter";
import { SharedService } from "src/shared/shared.service";
import { EVENT_USER_CREATED } from "./constants/constants";
import { IUserCreationEvent } from "./events/user-creation.interface";
import { ECommandProcessingStatus } from "src/shared/enum/command-source.enum";
import { UserService } from "src/user/user.service";

@Injectable()
export class NominationsService {
  constructor(
    @InjectModel("nominations")
    private readonly nominationsModel: Model<INominationsModel>,
    private shared_service: SharedService,
    private userService: UserService
  ) {}

  async addNominations(body: NominationsDTO, token) {
    try {
      console.log("body is", body);

      let nominations = [];
      for (let i = 0; i < body.nominations.length; i++) {
        const nominee = body.nominations[i];
        nominations.push({
          projectId: nominee.projectId,
          applicationId: nominee.applicationId,
          awardId: nominee.awardId,
          nomineeName: nominee.nomineeName,
          nomineePhoneNumber: nominee.nomineePhoneNumber,
          nomineeEmail: nominee.nomineeEmail,
          nomineeDescription: nominee.nomineeDescription,
          status: nominee.status,
          createdBy: token.id,
          updatedBy: token.id,
        });

        await this.shared_service.trackAndEmitEvent(
          EVENT_USER_CREATED,
          nominee.userDetail,
          true,
          {
            userId: token.id,
            resourceUri: null,
            action: null,
          }
        );
      }

      let data = await this.nominationsModel.insertMany(nominations);

      return { data };
    } catch (err) {
      throw err;
    }
  }

  async getNominationsByApplication(applicationId: string) {
    try {
      let nominations = await this.nominationsModel
        .find({
          applicationId: applicationId,
        })
        .populate("awardId");

      return nominations;
    } catch (err) {
      throw err;
    }
  }

  @OnEvent(EVENT_USER_CREATED)
  async userCreation(userCreationPayload: IUserCreationEvent): Promise<any> {
    try {
      console.log("userCreationPayload", userCreationPayload);

      await this.shared_service.updateEventProcessingStatus(
        userCreationPayload?.commandSource,
        ECommandProcessingStatus.InProgress
      );
      console.log("inside event user created");
      await this.userService.validateAndCreateUser(userCreationPayload);
      await await this.shared_service.updateEventProcessingStatus(
        userCreationPayload?.commandSource,
        ECommandProcessingStatus.Complete
      );
    } catch (err) {
      console.error(err);
      await this.shared_service.updateEventProcessingStatus(
        userCreationPayload?.commandSource,
        ECommandProcessingStatus.Failed
      );
    }
  }
}
