import { NominationsService } from "./nominations.service";
import {
  Body,
  Controller,
  Get,
  ParseIntPipe,
  Post,
  Query,
  Res,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { Response } from "express";
import { SharedService } from "src/shared/shared.service";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { UserToken } from "src/user/dto/usertoken.dto";
import { NominationsDTO } from "./dto/nominations.dto";

@Controller("nominations")
export class NominationsController {
  constructor(
    private readonly nominationsService: NominationsService,
    private sservice: SharedService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post("add-nominations")
  async addNominations(
    @GetToken() token: UserToken,
    @Body(new ValidationPipe({ whitelist: true })) body: NominationsDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.nominationsService.addNominations(body, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
