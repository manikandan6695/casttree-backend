import { Module } from "@nestjs/common";
import { AuthModule } from "src/auth/auth.module";
import { SharedModule } from "src/shared/shared.module";
import { NominationsController } from "./nominations.controller";
import { NominationsService } from "./nominations.service";
import { nominationsSchema } from "./schema/nominations.schema";
import { MongooseModule } from "@nestjs/mongoose";
import { UserModule } from "src/user/user.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "nominations", schema: nominationsSchema },
    ]),
    SharedModule,
    AuthModule,
    UserModule,
  ],
  controllers: [NominationsController],
  providers: [NominationsService],
  exports: [NominationsService],
})
export class NominationsModule {}
