import { IBaseEventEmitter } from "src/shared/interfaces/base-event.interface";
import { UserToken } from "src/user/dto/usertoken.dto";

export interface IUserCreationEvent extends IBaseEventEmitter {
  userName: string;
  phoneNumber: string;
  emailId: string;
  phoneCountryCode: string;
}

export interface IPeerTubeUserCreationEvent extends IBaseEventEmitter {
  userName: string;
  phoneNumber: string;
  emailId: string;
  phoneCountryCode: string;
  token: UserToken;
}
