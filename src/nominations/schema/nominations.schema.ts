import * as mongoose from "mongoose";
export interface INominationsModel extends mongoose.Document {
  projectId: string;
  applicationId: string;
  awardId: string;
  nomineeName: string;
  nomineePhoneNumber: string;
  nomineeEmail: string;
  nomineeDescription: string;
  status: string;
  createdBy: string;
  updatedBy: string;
}
export const nominationsSchema = new mongoose.Schema(
  {
    projectId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "project",
    },
    applicationId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "application",
    },
    awardId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "awards",
    },
    nomineeName: {
      type: String,
    },
    nomineePhoneNumber: {
      type: String,
    },
    nomineeEmail: {
      type: String,
    },
    nomineeDescription: {
      type: String,
    },
    status: {
      type: String,
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    updatedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  {
    collection: "nominations",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
