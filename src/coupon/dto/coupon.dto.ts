import { Type } from "class-transformer";
import {
  IsArray,
  IsEnum,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
import { AwardsDTO } from "src/application/dto/application.dto";
import { EStatus } from "src/shared/enum/privacy.enum";

export class CouponDTO {
  @IsNotEmpty()
  @IsString()
  totalAmount: string;

  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => AwardsDTO)
  awards: AwardsDTO[];
}

export class ApplyCouponDTO {
  @IsNotEmpty()
  @IsMongoId()
  sourceId: string;

  @IsNotEmpty()
  @IsString()
  sourceType: string;

  @IsNotEmpty()
  @IsMongoId()
  userId: string;

  @IsNotEmpty()
  @IsMongoId()
  couponId: string;
}
