import { AuthModule } from "./../auth/auth.module";
import { SharedModule } from "./../shared/shared.module";
import { MongooseModule } from "@nestjs/mongoose";
import { Module } from "@nestjs/common";
import { CouponController } from "./coupon.controller";
import { CouponService } from "./coupon.service";
import { couponSchema } from "./schema/coupon.schema";
import { couponUsageSchema } from "./schema/coupon-usage.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "coupon", schema: couponSchema },
      { name: "couponUsage", schema: couponUsageSchema },
    ]),
    SharedModule,
    AuthModule,
  ],
  controllers: [CouponController],
  providers: [CouponService],
  exports: [CouponService],
})
export class CouponModule {}
