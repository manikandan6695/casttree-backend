import { HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { AppException } from "src/shared/app-exception";
import { EStatus } from "src/shared/enum/privacy.enum";
import { SharedService } from "src/shared/shared.service";
import { UserToken } from "src/user/dto/usertoken.dto";
import { ApplyCouponDTO } from "./dto/coupon.dto";
import { ICouponUsageModel } from "./schema/coupon-usage.schema";
import { ICouponModel } from "./schema/coupon.schema";
import { guard } from "@ucast/mongo2js";

@Injectable()
export class CouponService {
  constructor(
    @InjectModel("coupon")
    private readonly couponModel: Model<ICouponModel>,
    @InjectModel("couponUsage")
    private readonly couponUsageModel: Model<ICouponUsageModel>,
    private shared_service: SharedService
  ) {}

  async getCouponList(body: any) {
    try {
      let couponData = await this.formCouponValue(body.awards);

      let filter = {
        $and: [{ status: "Active" }],
      };
      let data = await this.couponModel.find(filter).lean();
      data.forEach((e) => {
        let rule = e.rule;
        const validateCoupon = guard(rule);
        let isValid = validateCoupon({
          groupCount: couponData["group"],
          individualCount: couponData["single"],
        });
        e["isEnabled"] = isValid;
      });
      return { data };
    } catch (err) {
      throw err;
    }
  }
  async formCouponValue(awards) {
    try {
      let data = awards.reduce((acc, curr) => {
        let type = acc[curr.type];
        if (type) acc[curr.type] += curr.count;
        else acc[curr.type] = curr.count;
        return acc;
      }, {});

      return data;
    } catch (err) {
      throw err;
    }
  }

  async applyCoupon(body: ApplyCouponDTO, token: UserToken) {
    try {
      let coupon = await this.couponModel.findOne({
        _id: body.couponId,
      });
      if (!coupon) {
        throw new AppException(
          "Invalid coupon code",
          HttpStatus.NOT_ACCEPTABLE
        );
      }

      if (coupon.status == EStatus.Inactive) {
        throw new AppException(
          "This coupon code is no longer available",
          HttpStatus.NOT_ACCEPTABLE
        );
      }

      if (new Date(coupon.startsAt) > new Date()) {
        let start_date = new Date(coupon.startsAt).toString();
        throw new AppException(
          `This coupon code will be available on ${start_date.substring(
            4,
            15
          )} `,
          HttpStatus.NOT_ACCEPTABLE
        );
      }

      if (new Date(coupon.endsAt) < new Date()) {
        throw new AppException(
          "This coupon code has been expired",
          HttpStatus.NOT_ACCEPTABLE
        );
      }

      let fv = {
        ...body,
        redeemedAt: new Date().toISOString(),
        appliedBy: token.id,
      };
      let data = await this.couponUsageModel.create(fv);
      return { data };
    } catch (err) {
      throw err;
    }
  }
}
