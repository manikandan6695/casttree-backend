import {
  Body,
  Controller,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { CouponService } from "./coupon.service";
import { Response } from "express";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { ApplyCouponDTO, CouponDTO } from "./dto/coupon.dto";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { UserToken } from "src/user/dto/usertoken.dto";

@Controller("coupon")
export class CouponController {
  constructor(
    private sservice: SharedService,
    private couponService: CouponService
  ) {}

  @Post("get-coupon-list")
  async getCouponList(
    @Res() res: Response,
    @Body(new ValidationPipe({ whitelist: true })) body: CouponDTO
  ) {
    try {
      let data = await this.couponService.getCouponList(body);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("apply-coupon")
  async applyCoupon(
    @Res() res: Response,
    @Body(new ValidationPipe({ whitelist: true })) body: ApplyCouponDTO,
    @GetToken() token: UserToken
  ) {
    try {
      let data = await this.couponService.applyCoupon(body, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
