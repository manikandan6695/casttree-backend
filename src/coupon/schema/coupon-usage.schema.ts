import * as mongoose from "mongoose";
export interface ICouponUsageModel extends mongoose.Document {
  sourceId: string;
  sourceType: string;
  userId: string;
  couponId: string;
  redeemedAt: Date;
  appliedBy: string;
}
export const couponUsageSchema = new mongoose.Schema(
  {
    sourceId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    sourceType: {
      type: String,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    couponId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "coupon",
    },
    redeemedAt: {
      type: Date,
    },
    appliedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  {
    collection: "couponUsage",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
