import * as mongoose from "mongoose";
export interface ICouponModel extends mongoose.Document {
  name: string;
  termsAndCondition: string;
  description: string;
  couponCode: string;
  rule: any;
  discountType: string;
  discountValue: string;
  maxDiscountValue: string;
  maxUsagePerUser: number;
  maxUsage: number;
  startsAt: Date;
  endsAt: Date;
  status: string;
}
export const couponSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    termsAndCondition: {
      type: String,
    },
    description: {
      type: String,
    },
    couponCode: {
      type: String,
    },
    rule: {
      type: mongoose.Schema.Types.Mixed,
    },
    discountType: {
      type: String,
    },
    discountValue: {
      type: String,
    },
    maxDiscountValue: {
      type: String,
    },
    maxUsagePerUser: {
      type: Number,
    },
    maxUsage: {
      type: Number,
    },
    startsAt: {
      type: Date,
    },
    endsAt: {
      type: Date,
    },
    status: {
      type: String,
    },
  },
  {
    collection: "coupon",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
