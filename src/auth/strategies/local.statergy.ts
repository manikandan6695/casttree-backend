import {
  HttpException,
  Injectable
} from "@nestjs/common";
import { HttpStatus } from "@nestjs/common/enums/http-status.enum";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AppException } from "src/shared/app-exception";
import { SharedService } from "src/shared/shared.service";
import { AuthService } from "../auth.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    private authService: AuthService,
    private sservice: SharedService
  ) {
    super({
      usernameField: "user_id",
      passwordField: "password",
      // testField: "test"
    });
  }
  async validate(username: string, password: string): Promise<any> {
    try {
      const user = await this.authService.validateUser(username, password);
      // const user = null;
      if (!user) {
        throw new AppException(
          "Credentials not matched",
          HttpStatus.UNAUTHORIZED
        );
      }
      return user;
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      throw new HttpException(response, code);
    }
  }
}
