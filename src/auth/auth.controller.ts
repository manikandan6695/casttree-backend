import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  Req,
  Request,
  Res,
  UseGuards,
  UseInterceptors,
  ValidationPipe,
} from "@nestjs/common";
import { Throttle } from "@nestjs/throttler";
import { Response } from "express";
import { CustomLogger } from "src/logger/customlogger.service";
import { AppException } from "src/shared/app-exception";
import {
  GENERATE_OTP_THROTTLE_LIMIT,
  GENERATE_OTP_THROTTLE_TTL,
  LOGIN_WITH_OTP_THROTTLE_LIMIT,
  LOGIN_WITH_OTP_THROTTLE_TTL,
} from "src/shared/app.constants";
import { GetSystemConfiguration } from "src/shared/decorator/get-system-configuration.decorator";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { SharedService } from "src/shared/shared.service";
import { UserSignUpDTO } from "src/user/dto/org-signup.dto";
import { UserToken } from "src/user/dto/usertoken.dto";
import { AuthService } from "./auth.service";
import { GetLanguage } from "./decorators/param/get-language.decorator";
import { GetOrganization } from "./decorators/param/get-organization.decorator";
import { GetProductKey } from "./decorators/param/get-product-key";
import { GoogleLoginDTO } from "./dto/google-login.dto";
import { JwtAuthGuard } from "./guards/jwt-auth.guard";
import { LocalAuthGuard } from "./guards/local-auth.guard";
import { FormSystemConfiguration } from "./interceptors/form-system-configuration.interceptor";
import { GenerateOTPDTO } from "./otp/dto/generate-otp.dto";
import { OTPLoginDTO } from "./otp/dto/otp-login.dto";
import { EOTPActivity } from "./otp/enum/otp-activity.enum";
import { EOTPChannel } from "./otp/enum/otp-channel.enum";
import { OtpService } from "./otp/otp.service";

@Controller("auth")
export class AuthController {
  constructor(
    private auth_service: AuthService,
    private otp_service: OtpService,
    private shared_service: SharedService,
    private custom_logger: CustomLogger
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post("login")
  async login(@Request() req, @Res() res: Response) {
    try {
      let token = await this.auth_service.generateToken(req.user);
      return res.status(200).send(token);
    } catch (err) {
      const { code, response } = this.shared_service.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @Throttle({
    default: {
      limit: LOGIN_WITH_OTP_THROTTLE_LIMIT,
      ttl: LOGIN_WITH_OTP_THROTTLE_TTL,
    },
  })
  @UseInterceptors(FormSystemConfiguration)
  @Post("otp-login/:channel")
  async loginWithOTP(
    @Param("channel") channel: EOTPChannel,
    @Body() body: OTPLoginDTO,
    @Res() res: Response
  ) {
    try {
      let token = await this.auth_service.loginWithOTP(
        body,
        channel,
        EOTPActivity.login
      );
      return res.status(200).send(token);
    } catch (err) {
      const { code, response } = this.shared_service.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post("user-signup")
  @UseInterceptors(FormSystemConfiguration)
  async signUp(
    @GetLanguage() language: string,
    @Body(new ValidationPipe({ whitelist: true })) body: UserSignUpDTO,
    @GetSystemConfiguration("default-user-image") default_user_image: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.auth_service.signUp(
        body,
        language,
        default_user_image
      );
      return res.json(data);
    } catch (err) {
      const { code, response } = this.shared_service.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }

  @Throttle({
    default: {
      limit: GENERATE_OTP_THROTTLE_LIMIT,
      ttl: GENERATE_OTP_THROTTLE_TTL,
    },
  })
  @Post("generate-otp/:channel")
  async generateOTP(
    @Param("channel") channel: string,
    @Param("mode") mode: string,
    @Body(new ValidationPipe()) body: GenerateOTPDTO,
    @Res() res: Response
  ) {
    try {
      let otp_channel_type: EOTPChannel =
        channel == "Email" ? EOTPChannel.email : EOTPChannel.phone;
      let data = await this.auth_service.validateOTPAction(
        body,
        otp_channel_type,
        EOTPActivity.login
      );

      return res.json(data);
    } catch (err) {
      const { code, response } = this.shared_service.processError(
        err,
        this.constructor.name
      );
      return res.status(code).send(response);
    }
  }
}
