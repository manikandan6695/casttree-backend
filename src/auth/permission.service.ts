// import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
// import { InjectModel } from "@nestjs/mongoose";
// import * as mongoose from "mongoose";
// import { Model } from "mongoose";
// import { IBranchesModel } from "src/branch/schema/branch.schema";
// import { IConfigurationModel } from "src/configuration/schema/configuration.schema";
// import { EConnectionStatus } from "src/connections/enum/connection-status.enum";
// import { IConnectionsModel } from "src/connections/schema/connections.schema";
// import {
//   EPermissionType,
//   IDataPermissionModel,
//   IPermission,
// } from "src/data-permission/schema/dataPermission.schema";
// import { IEntityModel } from "src/entity/schema/entity.schema";
// import { EDefaultValue } from "src/form/enum/default-value.enum";
// import { EDefaultFieldType } from "src/form/enum/field-type.enum";
// import { EFormType } from "src/form/enum/form-type.enum";
// import { IFieldDetailModel, IFormModel } from "src/form/schema/form.schema";
// import { IGroupMemberModel } from "src/group/schema/group-member.schema";
// import { CustomLogger } from "src/logger/customlogger.service";
// import { IOrganizationModel } from "src/organization/schema/organization.schema";
// import { IRoleAssignmentModel } from "src/role/schema/role-assignment.schema";
// import { AppException } from "src/shared/app-exception";
// import { EStatus } from "src/shared/enum/privacy.enum";
// import { ITimeZoneModel } from "src/shared/schema/time-zone.schema";
// import { app_variables } from "src/shared/shared.service";
// import { UserToken } from "src/user/dto/usertoken.dto";
// import { IUserModel } from "src/user/schema/user.schema";
// import { IUserBranchModel } from "src/user/schema/userBranch.schema";
// import { IUserOrganizationModel } from "src/user/schema/userOrganization.schema";
// import { IUserWarehouseModel } from "src/user/schema/userWarehouse.schema";
// import { IWarehouseModel } from "src/warehouse/schema/warehouse.schema";
// import { AuthService } from "./auth.service";
// import { IRelatedFormDetails } from "./decorators/check-related-forms.decorator";
// import { IFilterPreferenceDetail } from "./decorators/filter-preference.decorator";
// import { IRequestDetails } from "./decorators/request-details.decorator";
// import {
//   EActionCheck,
//   EUserActions,
//   IActionDetails,
// } from "./decorators/user-action.decorator";
// import { EProductKey } from "./enum/product-key.enum";

// @Injectable()
// export class PermissionService {
//   constructor(
//     private auth_service: AuthService,
//     @InjectModel("form")
//     private readonly formModel: Model<IFormModel>,
//     @InjectModel("roleAssignment")
//     private readonly roleAssignmentModel: Model<IRoleAssignmentModel>,
//     @InjectModel("dataPermission")
//     private readonly dataPermissionModel: Model<IDataPermissionModel>,
//     @InjectModel("connections")
//     private readonly connectionsModel: Model<IConnectionsModel>,
//     @InjectModel("user") private User: Model<IUserModel>,
//     @InjectModel("userBranch") private userBranch: Model<IUserBranchModel>,
//     @InjectModel("userWarehouse")
//     private userWarehouse: Model<IUserWarehouseModel>,
//     @InjectModel("warehouse")
//     private warehouse: Model<IWarehouseModel>,
//     @InjectModel("branch")
//     private branch: Model<IBranchesModel>,
//     @InjectModel("userOrganization")
//     private userOrganization: Model<IUserOrganizationModel>,
//     @InjectModel("organization")
//     private organizationModel: Model<IOrganizationModel>,
//     @InjectModel("timeZone") private timezone_model: Model<ITimeZoneModel>,
//     @InjectModel("configuration")
//     private ConfigurationModel: Model<IConfigurationModel>,
//     @InjectModel("groupMember")
//     private readonly groupMemberModel: Model<IGroupMemberModel>,
//     @InjectModel("entity")
//     private readonly entity_model: Model<IEntityModel>,
//     private logger: CustomLogger
//   ) {}

//   async checkUserAccess(
//     request: any,
//     organization_portal_name: string,
//     token: UserToken,
//     user_action_meta_data: IActionDetails,
//     request_meta_data: IRequestDetails,
//     check_related_forms: IRelatedFormDetails,
//     filterPreference?: IFilterPreferenceDetail
//   ) {
//     try {
//       // console.log("request is", request);

//       let org = await this.organizationModel
//         .findOne({
//           organization_portal_name: organization_portal_name,
//         })
//         .populate({ path: "organization_media.media_id", model: "media" });
//       if (!org)
//         throw new HttpException(
//           "Organization not exist",
//           HttpStatus.NOT_ACCEPTABLE
//         );
//       let organization_id: string = org._id.toString();
//       let configuration = await this.ConfigurationModel.findOne({
//         organization_id: organization_id,
//       });
//       let time_zone = await this.timezone_model.findOne({
//         _id: org.organization_time_zone,
//       });
//       request["organization"] = org;
//       request["organization_time_zone"] = time_zone;
//       request["configuration"] = configuration;
//       let product_key = request.headers["productkey"];
//       if (!product_key) {
//         throw new HttpException(
//           "Product key is required",
//           HttpStatus.BAD_REQUEST
//         );
//       }
//       request["product_key"] = product_key;
//       if (request_meta_data?.need_system_configuration) {
//         let system_configuration = await this.auth_service.getSystemConfig();
//         request["system_configuration"] = system_configuration;
//       }
//       let user = await this.userOrganization.findOne({
//         organization_id: organization_id,
//         product_key,
//         user_id: token.id,
//       });
//       if (!user)
//         throw new AppException(
//           "Forbidden - User not belongs to organization",
//           HttpStatus.FORBIDDEN
//         );
//       let form_code = "";
//       if (user_action_meta_data.formType == "explicit") {
//         form_code = this.getRequestDetails(
//           request,
//           user_action_meta_data.formDetails.formPayloadType,
//           user_action_meta_data.formDetails.formAttrKey,
//           "string"
//         );
//       } else form_code = user_action_meta_data.formCode;
//       request["sourceType"] = form_code;
//       let cur_form = await this.formModel
//         .findOne({
//           form_code: form_code, // TODO: - dynamic formDetails will take _id instead of form_code
//         })
//         .populate("entity_id");
//       // console.log("cur_form in from model", cur_form);

//       if (!cur_form)
//         throw new AppException(`Form details not found`, HttpStatus.NOT_FOUND);
//       request["form_details"] = cur_form.toJSON();
//       let fields: IFieldDetailModel[] = this.getFormFields(cur_form);
//       if (
//         cur_form.meta_data.form_type == EFormType["sub-form"] &&
//         !request_meta_data.skip_parent_check
//       ) {
//         try {
//           let parent_form = await this.formModel
//             .findOne({ _id: cur_form.meta_data.parent_form_id })
//             .populate("entity_id");
//           let parent_access: boolean = await this.checkParentPermission(
//             parent_form,
//             request_meta_data,
//             organization_id,
//             token,
//             request,
//             product_key
//           );
//           if (!parent_access) {
//             throw new AppException(
//               "Parent data access failed",
//               HttpStatus.FORBIDDEN
//             );
//           }
//         } catch (err) {
//           if (err instanceof AppException) {
//             throw err;
//           } else
//             throw new AppException(
//               "Parent data access failed",
//               HttpStatus.FORBIDDEN
//             );
//         }
//       }
//       let vali_form_perm = await this.validateFormPermission(
//         request_meta_data,
//         cur_form,
//         token,
//         request,
//         organization_id,
//         "direct_form",
//         product_key
//       );
//       let dp: IDataPermissionModel = vali_form_perm.dPermission;
//       // console.log("dp is", dp);

//       // Check whether user has permission for the desire action towards the object (form)
//       let rec_act_perm = dp.permissions.find(
//         (ele) =>
//           ele.type == EPermissionType.record &&
//           ele.activity == user_action_meta_data.action
//       );
//       // console.log("rec_act_perm", rec_act_perm);

//       if (!rec_act_perm)
//         throw new AppException(
//           "Forbidden - Record permission not exist",
//           HttpStatus.FORBIDDEN
//         );
//       let result = await this.abacCheck(
//         user_action_meta_data,
//         request_meta_data,
//         rec_act_perm,
//         cur_form,
//         organization_id,
//         token.id,
//         request,
//         "direct_form",
//         token,
//         configuration,
//         fields,
//         product_key,
//         filterPreference
//       );
//       let is_list_view =
//         user_action_meta_data.action == EUserActions.view &&
//         !user_action_meta_data.actionSubType;
//       if (check_related_forms && check_related_forms.check) {
//         let relForm = cur_form.meta_data.related_forms;
//         let rel_form_perm_obj: RelatedFormDetails = {};
//         for (let i = 0; i < relForm.length; i++) {
//           let cur_form = relForm[i];
//           try {
//             if (
//               check_related_forms &&
//               check_related_forms.prefer_values &&
//               !check_related_forms.values.includes(cur_form.key)
//             ) {
//               console.log("going to continue");
//               continue;
//             }
//             let form = await this.formModel
//               .findOne({
//                 _id: cur_form.form_id,
//               })
//               .populate("entity_id");
//             let related_form_fields = this.getFormFields(form);
//             let criteria_avail = await this.validateFormPermission(
//               request_meta_data,
//               form,
//               token,
//               request,
//               organization_id,
//               "related_form",
//               product_key
//             );

//             if (criteria_avail.hasAccess) {
//               for (let j = 0; j < cur_form.activity.length; j++) {
//                 let e = cur_form.activity[j];
//                 let key = `can_${e.type}_${cur_form.key}`;
//                 if (
//                   check_related_forms &&
//                   check_related_forms.activity_check &&
//                   !check_related_forms.activity_check.includes(key)
//                 ) {
//                   console.log("going to continue");
//                   continue;
//                 }
//                 let action = criteria_avail.dPermission.permissions.find(
//                   (perm) =>
//                     perm.activity == e.type &&
//                     perm.type == EPermissionType.record
//                 );

//                 let access = false;
//                 if (action) {
//                   if (e.is_specific && !is_list_view) {
//                     let data = await this.abacCheck(
//                       { action: EUserActions[e.type] },
//                       request_meta_data,
//                       action,
//                       form,
//                       organization_id,
//                       token.id,
//                       request,
//                       "related_form",
//                       token,
//                       configuration,
//                       related_form_fields,
//                       product_key
//                     );
//                     access = data.has_access;
//                   } else {
//                     access = true;
//                   }
//                 } else {
//                   access = false;
//                 }
//                 if (e.required_scope || is_list_view) {
//                   if (access)
//                     rel_form_perm_obj[key] = { access, scope: action.value };
//                   else rel_form_perm_obj[key] = { access, scope: -1 };
//                 } else {
//                   rel_form_perm_obj[key] = access;
//                 }
//               }
//             }
//             request["rel_form_perm"] = rel_form_perm_obj;
//           } catch (err) {
//             console.log(`err is ${cur_form.key} ${cur_form.activity}`, err);
//           }
//         }
//       }
//       return result;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async checkParentPermission(
//     form: IFormModel,
//     request_meta_data: IRequestDetails,
//     organization_id: string,
//     token: UserToken,
//     request: any,
//     product_key: string
//   ): Promise<boolean> {
//     try {
//       let has_parent_access = false;
//       let vali_form_perm = await this.validateFormPermission(
//         request_meta_data,
//         form,
//         token,
//         request,
//         organization_id,
//         "related_form",
//         product_key,
//         "primaryRefAttrKey"
//       );
//       let dp: IDataPermissionModel = vali_form_perm.dPermission;
//       // Check whether user has permission for the desire action towards the object (form)
//       let rec_act_perm = dp.permissions.find(
//         (ele) => ele.type == "record" && ele.activity == EUserActions.view
//       );
//       if (!rec_act_perm)
//         throw new AppException(
//           "Forbidden - Record permission not exist",
//           HttpStatus.FORBIDDEN
//         );
//       let parent_fields = this.getFormFields(form);
//       if (form.meta_data.participant_view && rec_act_perm.value == 0) {
//         let member_exist = await this.groupMemberModel.findOne({
//           user_id: token.id,
//           source_id: this.getRequestDetails(
//             request,
//             request_meta_data.payloadType,
//             request_meta_data.primaryRefAttrKey,
//             "string"
//           ),
//           status: "Active",
//           acceptance_status: "Accepted",
//         });
//         return member_exist ? true : false;
//       }
//       let resource_details = await this.getResourceDetails(
//         organization_id,
//         form,
//         request,
//         request_meta_data,
//         "related_form",
//         "primaryRefAttrKey",
//         parent_fields
//       );
//       let cra_access_privilage = await this.checkResourceAccess(
//         organization_id,
//         token.id,
//         rec_act_perm,
//         "CRA",
//         product_key,
//         resource_details.record_user_ids,
//         resource_details.multi,
//         resource_details.record_vendor_ids,
//         resource_details.record_customer_ids
//       );
//       has_parent_access = cra_access_privilage.hasAccess;
//       return has_parent_access;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async validateFormPermission(
//     request_meta_data: IRequestDetails,
//     form_data: IFormModel,
//     token: UserToken,
//     request: any,
//     organization_id: string,
//     type: string,
//     product_key: string,
//     primary_type: string = null
//   ) {
//     try {
//       let group_id = "";
//       let form_type = form_data.meta_data.form_type;
//       if (form_type == EFormType["group-form"]) {
//         if (request_meta_data) {
//           // parent group - form dont have id, ex auction, group
//           group_id = this.getRequestDetails(
//             request,
//             request_meta_data.payloadType,
//             primary_type
//               ? request_meta_data[primary_type]
//               : request_meta_data.primaryRefAttrKey,
//             "string"
//           );
//           if (type != "related_form")
//             request["sourceId"] = mongoose.Types.ObjectId(group_id);
//         }
//       }
//       // role based on groups or the role assigned to the user
//       let user_variables = await this.getUserVariables(
//         organization_id,
//         token.id,
//         product_key,
//         group_id
//       );
//       if (type != "related_form") {
//         request["user_variables"] = user_variables;
//       }
//       // finding the datapermission based on the form id
//       let dp_filter = {};
//       if (product_key == EProductKey.social_network) {
//         dp_filter = {
//           form_id: form_data._id,
//           product_key: EProductKey.social_network,
//         };
//       } else {
//         dp_filter = {
//           organization_id,
//           form_id: form_data._id,
//         };
//       }
//       let form_dpermission: IDataPermissionModel[] = await this.dataPermissionModel.find(
//         dp_filter
//       );
//       // finding which data permission matches to him
//       let criteria_avail = await this.validateDPCriteria(
//         user_variables,
//         form_dpermission,
//         request,
//         type
//       );
//       if (!criteria_avail.hasAccess)
//         throw new AppException(
//           "Forbidden - Data permission validation failed",
//           HttpStatus.FORBIDDEN
//         );
//       return criteria_avail;
//     } catch (err) {
//       throw err;
//     }
//   }
//   getFormFields(form: IFormModel): IFieldDetailModel[] {
//     try {
//       let fields: IFieldDetailModel[] = [];
//       form.sections
//         .filter((e) => e.status == EStatus.Active)
//         .forEach((e) => fields.push(...e.fields));
//       return fields;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async checkResourceAccess(
//     organization_id: string,
//     user_id: string,
//     perm: IPermission,
//     action_check: string,
//     product_key: string,
//     resource_id: any[] = null,
//     multi_resource: boolean = null,
//     record_vendor_ids: any[] = null,
//     record_customer_ids: any[] = null,
//     record_branch_ids: any[] = null,
//     record_warehouse_ids: any[] = null,
//     filterPreference: IFilterPreferenceDetail = null,
//     cur_form: IFormModel = null
//   ): Promise<ResourceAccessDetail> {
//     try {
//       let applicable_ids = [];
//       let applicable_vendors = [];
//       let applicable_customers = [];
//       let all_access = false;
//       let exec_query = true;

//       let connection = await this.getConnections(user_id, organization_id);
//       let userOID = mongoose.Types.ObjectId(user_id);

//       let direct_sub_ids = [];
//       let indirect_sub_ids = [];
//       direct_sub_ids = [...connection];
//       indirect_sub_ids = [...connection];
//       switch (perm.value) {
//         case 0:
//           exec_query = false;
//           break;
//         case 1:
//           applicable_ids = [userOID]; //my data
//           if (
//             user_vars[app_variables.default_customer_fcode] &&
//             user_vars[app_variables.default_customer_fcode].length
//           )
//             applicable_customers =
//               user_vars[app_variables.default_customer_fcode];
//           if (
//             user_vars[app_variables.default_vendor_fcode] &&
//             user_vars[app_variables.default_vendor_fcode].length
//           )
//             applicable_vendors = user_vars[app_variables.default_vendor_fcode];
//           break;
//         case 2:
//           applicable_ids = [...connection]; //subordinates
//           break;
//         case 3:
//           applicable_ids = [userOID, ...connection]; // my+sub data
//           if (
//             user_vars[app_variables.default_customer_fcode] &&
//             user_vars[app_variables.default_customer_fcode].length
//           )
//             applicable_customers =
//               user_vars[app_variables.default_customer_fcode];
//           if (
//             user_vars[app_variables.default_vendor_fcode] &&
//             user_vars[app_variables.default_vendor_fcode].length
//           )
//             applicable_vendors = user_vars[app_variables.default_vendor_fcode];
//           break;
//         case 4:
//           all_access = true;
//           break;
//         default:
//           break;
//       }
//       // this.logger.debug(`${user_id} applicable ids are ${applicable_ids}`);
//       let return_value = {
//         execQuery: exec_query,
//         allAccess: all_access,
//         applicable_ids: applicable_ids,
//         applicable_customers,
//         applicable_vendors,
//         indirect_sub_ids,
//         direct_sub_ids,
//         hasAccess: true,
//       };
//       if (!exec_query) {
//         return_value.hasAccess = false;
//       }
//       let s_applicable_ids = applicable_ids.map((e) => e.toString());
//       let s_customer_ids = applicable_customers.map((e) => e.toString());

//       if (exec_query && !all_access && action_check == "CRA") {
//         if (!filterPreference || filterPreference.check_against_user_id) {
//           if (multi_resource) {
//             return_value.hasAccess = resource_id.every((row) =>
//               row.some((e) => s_applicable_ids.includes(e.toString()))
//             );
//           } else {
//             return_value.hasAccess = resource_id.some((e) =>
//               s_applicable_ids.includes(e.toString())
//             );
//           }
//         } else if (
//           filterPreference &&
//           filterPreference.check_against_customer_id
//         ) {
//           // console.log("insdie else if part to check against customer");

//           return_value.hasAccess = record_customer_ids.some((e) =>
//             s_customer_ids.includes(e.toString())
//           );
//         }
//         let customer_access: boolean = true;
//         let user_access: boolean = true;
//         user_access = resource_id.some((e) =>
//           s_applicable_ids.includes(e.toString())
//         );
//         customer_access = record_customer_ids.some((e) =>
//           s_customer_ids.includes(e.toString())
//         );

//         if (!return_value.hasAccess) {
//           return_value.hasAccess = user_access || customer_access;
//         }
//       }
//       return return_value;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async getResourceDetails(
//     organization_id: string,
//     form_doc_data: IFormModel,
//     request: any,
//     request_meta_data: IRequestDetails,
//     type: string,
//     primary_key: string,
//     fields: IFieldDetailModel[],
//     filterPreference: IFilterPreferenceDetail = null,
//     user_action_meta_data: IActionDetails = null
//   ) {
//     try {
//       let collection = (form_doc_data.entity_id as IEntityModel)
//         .entity_collection_name;
//       let primary_field: IFieldDetailModel = fields.find(
//         (e) => e.is_primary_reference
//       );
//       let data;
//       if (request_meta_data) {
//         let filter = {};
//         if (!request_meta_data.skip_org_check) {
//           filter["organization_id"] = mongoose.Types.ObjectId(organization_id);
//         }
//         let condition;
//         let filter_value = this.getRequestDetails(
//           request,
//           request_meta_data.payloadType,
//           request_meta_data[primary_key],
//           primary_field && primary_field.is_multi ? "idarray" : "objectid"
//         );
//         condition =
//           primary_field && primary_field.is_multi
//             ? { $in: filter_value }
//             : filter_value;
//         filter["_id"] = condition;
//         // this.logger.debug(filter, "PERMISSION");
//         // this.logger.debug(
//         //   `collection ${collection}, ${primary_field}`,
//         //   "PERMISSION"
//         // );
//         if (primary_field && primary_field.is_multi) {
//           data = await this.User.db.collection(collection).find(filter);
//           if (!data.length)
//             throw new AppException(
//               "Forbidden - Primary attr data not exist",
//               HttpStatus.FORBIDDEN
//             );
//         } else {
//           data = await this.User.db.collection(collection).findOne(filter);
//           if (!data) {
//             throw new AppException(
//               "Forbidden - Primary attr data not exist",
//               HttpStatus.FORBIDDEN
//             );
//           }
//           if (
//             user_action_meta_data &&
//             [EUserActions.edit, EUserActions.delete].includes(
//               user_action_meta_data.action
//             )
//           ) {
//             if (data.is_system == true) {
//               throw new AppException(
//                 "Forbidden - You cant edit / delete this record",
//                 HttpStatus.FORBIDDEN
//               );
//             }
//           }
//         }
//       }

//       if (type != "related_form") request["single_data"] = data;
//       let user_fields = fields.filter((e) => e.is_user_reference);
//       let record_user_ids: any[] = [];
//       let record_vendor_ids: any[] = [];
//       let record_customer_ids: any[] = [];
//       if (filterPreference && filterPreference.check_against_customer_id) {
//         filterPreference.customer_variable_names.forEach((e) => {
//           record_customer_ids.push(data[e]);
//         });
//       } else {
//         let customer_field = fields.filter((e) => e.is_customer_reference);
//         customer_field.forEach((e) => {
//           record_customer_ids.push(data[e.field]);
//         });
//       }

//       if (!primary_field || !primary_field.is_multi) {
//         user_fields.forEach((e) => {
//           if (data[e.field]) {
//             e.is_multi
//               ? record_user_ids.push(...data[e.field])
//               : record_user_ids.push(data[e.field]);
//           }
//         });
//         record_user_ids = record_user_ids
//           .filter((e) => mongoose.Types.ObjectId.isValid(e))
//           .map((e) => mongoose.Types.ObjectId(e));
//       } else {
//         data.forEach((row) => {
//           user_fields.forEach((e) => {
//             if (row[e.field])
//               e.is_multi
//                 ? record_user_ids.push(
//                     row[e.field].map((e) => mongoose.Types.ObjectId(e))
//                   )
//                 : record_user_ids.push([mongoose.Types.ObjectId(row[e.field])]);
//           });
//         });
//       }
//       return {
//         multi: primary_field && primary_field.is_multi ? true : false,
//         record_user_ids,
//         record_customer_ids,
//         record_vendor_ids,
//       };
//     } catch (err) {
//       throw err;
//     }
//   }
//   getRequestDetails(request, type: string, key: string, fieldType: string) {
//     try {
//       let data = null;
//       if (type == "body") {
//         data = request["body"][key];
//       } else if (type == "param") {
//         data = request.params[key];
//       } else if (type == "query") {
//         data = request.query[key];
//       }
//       if (fieldType == "objectid") {
//         data = mongoose.Types.ObjectId(data);
//       }
//       if (fieldType == "idarray") {
//         data = data.map((e) => mongoose.Types.ObjectId(e));
//       }
//       if (fieldType == "string") {
//         data = data.toString();
//       }
//       return data;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async getUserVariables(
//     organizationId: string,
//     userId: string,
//     product_key: string,
//     groupId: string = null
//   ) {
//     try {
//       let role: IRoleAssignmentModel | IGroupMemberModel;
//       if (groupId) {
//         role = await this.groupMemberModel
//           .findOne({
//             user_id: userId,
//             source_id: groupId,
//             status: "Active",
//             acceptance_status: "Accepted",
//           })
//           .lean();

//         if (!role) {
//           role = await this.roleAssignmentModel
//             .findOne({
//               organization_id: organizationId,
//               product_key,
//               user_id: userId,
//             })
//             .lean();
//         }
//       } else {
//         role = await this.roleAssignmentModel
//           .findOne({
//             organization_id: organizationId,
//             product_key,
//             user_id: userId,
//           })
//           .lean();
//       }
//       if (!role) {
//         throw new AppException(
//           "Forbidden - Role not found",
//           HttpStatus.FORBIDDEN
//         );
//       }
//       /**
//        * TODO:
//        * 1. Role id should be filtered with product key
//        */
//       return {
//         [app_variables.default_role_fcode]: [role.role_id.toString()],
//         [app_variables.default_user_fcode]: [userId],
//       };
//     } catch (err) {
//       throw err;
//     }
//   }
//   async validateUserInputs(
//     request: any,
//     form_doc_data: IFormModel,
//     request_meta_data: IRequestDetails,
//     organization_id: string,
//     type: string,
//     token: UserToken,
//     configuration: IConfigurationModel,
//     fields: IFieldDetailModel[]
//   ): Promise<boolean> {
//     try {
//       let returnValue = true;
//       if (type == "direct_form") {
//         let validationRequired = fields.filter((e) => e.do_filter);
//         for (let i = 0; i < validationRequired.length; i++) {
//           let curData = validationRequired[i];
//           // pending work
//         }
//         await this.processDefaultFields(
//           organization_id,
//           token,
//           configuration,
//           fields,
//           request
//         );
//         await this.getPrimaryRefData(
//           request,
//           form_doc_data,
//           request_meta_data,
//           fields
//         );
//       }
//       return returnValue;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async getPrimaryRefData(
//     request: any,
//     form_doc_data: IFormModel,
//     request_meta_data: IRequestDetails,
//     fields: IFieldDetailModel[]
//   ) {
//     try {
//       if (request_meta_data && request_meta_data.query_ref_detail) {
//         request["psource_type"] = form_doc_data.meta_data.parent_form_code;
//         let primaryReference = fields.find((e) => e.is_primary_reference);
//         if (primaryReference) {
//           let entity = await this.entity_model.findOne({
//             _id: primaryReference.entity_id,
//           });
//           let primary_ref_data = await this.User.db
//             .collection(entity.entity_collection_name)
//             .findOne({
//               _id: this.getRequestDetails(
//                 request,
//                 request_meta_data.payloadType,
//                 request_meta_data.primaryRefAttrKey,
//                 "objectid"
//               ),
//             });
//           request["primary_ref_data"] = primary_ref_data;
//         }
//       }
//     } catch (err) {
//       throw err;
//     }
//   }
//   async processDefaultFields(
//     organization_id: string,
//     token: UserToken,
//     configuration: IConfigurationModel,
//     fields: IFieldDetailModel[],
//     request: any
//   ) {
//     try {
//       // default value adding
//       let default_value_fields = fields
//         .filter((e) => e.default_value_type == EDefaultValue.Static)
//         .reduce((a, c) => {
//           a[c.field] = c.default_value;
//           return a;
//         }, {});
//       // variables adding
//       let cd = this.getVariables(organization_id, token, configuration);
//       fields
//         .filter((e) => e.default_value_type == EDefaultValue.Variables)
//         .forEach((e) => {
//           default_value_fields[e.field] = eval(e.default_value);
//         });
//       // dynamic values adding
//       let dynamic_value_fields = fields.filter(
//         (e) => e.default_value_type == EDefaultValue.Dynamic
//       );
//       for (let i = 0; i < dynamic_value_fields.length; i++) {
//         let field: IFieldDetailModel = dynamic_value_fields[i];
//         let entity = await this.entity_model.findOne({
//           _id: field.entity_id,
//         });
//         let value = await this.User.db
//           .collection(entity.entity_collection_name)
//           .findOne({
//             organization_id: mongoose.Types.ObjectId(organization_id),
//             ...field.default_value_filter.criteria,
//           });
//         if (field.field_type == EDefaultFieldType.objectid)
//           default_value_fields[field.field] = value["_id"];
//       }
//       request["default_value_fields"] = default_value_fields;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async validateDPCriteria(
//     userVariables: {},
//     dataPermissions: IDataPermissionModel[],
//     request,
//     type
//   ) {
//     try {
//       let hasAccess = false;
//       let dPermission: IDataPermissionModel = dataPermissions.find((dp) => {
//         if (userVariables[dp.source_type])
//           return userVariables[dp.source_type].includes(
//             dp.source_id.toString()
//           );
//         return false;
//       });
//       // console.log("user variables", userVariables, dataPermissions);

//       hasAccess = dPermission ? true : false;
//       if (hasAccess && type == "direct_form") {
//         request["dp"] = dPermission;
//       }
//       return { hasAccess, dPermission: dPermission };
//     } catch (err) {
//       throw err;
//     }
//   }
//   async getConnections(
//     user_id: string,
//     organization_id: string
//   ): Promise<any[]> {
//     try {
//       let total_user = [];
//       let has_subordinate_user = [];
//       let con_data = await this.getUserConnection(user_id, organization_id);
//       total_user = total_user.concat(con_data.total_user);
//       has_subordinate_user = has_subordinate_user.concat(
//         con_data.has_subordinate_user
//       );
//       let iteration = 0;
//       while (has_subordinate_user.length && iteration < 4) {
//         iteration++;
//         let sub_user = has_subordinate_user.pop();
//         let sub_user_con_data = await this.getUserConnection(
//           sub_user,
//           organization_id
//         );
//         total_user = total_user.concat(sub_user_con_data.total_user);
//         has_subordinate_user = has_subordinate_user.concat(
//           sub_user_con_data.has_subordinate_user
//         );
//       }
//       let relatedConnection = [];
//       let unique_user_ids = [...new Set(total_user)];
//       // this.logger.debug(
//       //   `unique_user_ids is ${user_id} ${unique_user_ids}`,
//       //   "PERMISSION"
//       // );
//       relatedConnection = unique_user_ids.map((e) =>
//         mongoose.Types.ObjectId(e)
//       );
//       return relatedConnection;
//     } catch (err) {
//       throw err;
//     }
//   }

//   async getUserConnection(user_id: string, organization_id: string) {
//     try {
//       let connections = await this.connectionsModel
//         .find({
//           organization_id,
//           target_user_id: user_id,
//           connection_status: EConnectionStatus.accepted,
//           // type: {$in: [EConnectionType.relationship,EConnectionType.reporting]},
//         })
//         .populate("sub_ordinate");
//       let total_user = [];
//       let has_subordinate_user = [];
//       connections.forEach((e) => {
//         total_user.push(e.user_id);
//         if (e.sub_ordinate.length) {
//           has_subordinate_user.push(e.user_id);
//         }
//       });
//       return { total_user, has_subordinate_user };
//     } catch (err) {
//       throw err;
//     }
//   }
//   getVariables(
//     organization_id: string,
//     token: UserToken,
//     configuration: IConfigurationModel
//   ): ContextVar {
//     let default_user_image = configuration.configurations["default-user-image"];
//     let default_post_image = configuration.configurations["default-post-image"];
//     return {
//       organization_id: mongoose.Types.ObjectId(organization_id),
//       user_id: mongoose.Types.ObjectId(token.id),
//       current_date_time: new Date().toISOString(),
//       current_time_stamp: new Date().toISOString(),
//       default_user_image: mongoose.Types.ObjectId(default_user_image),
//       default_post_image: mongoose.Types.ObjectId(default_post_image),
//     };
//   }
//   async abacCheck(
//     user_action_meta_data: IActionDetails,
//     request_meta_data: IRequestDetails,
//     rec_act_perm: IPermission,
//     cur_form: IFormModel,
//     organization_id: string,
//     user_id: string,
//     request: any,
//     type: string,
//     token: UserToken,
//     configuration: IConfigurationModel,
//     fields: IFieldDetailModel[],
//     product_key: string,
//     filterPreference: IFilterPreferenceDetail = null
//   ) {
//     try {
//       // console.log("rec_act_perm", rec_act_perm);

//       let result = { has_access: false, filter_details: {}, request };

//       console.log(
//         "action is",
//         EActionCheck[
//           user_action_meta_data.actionSubType || user_action_meta_data.action
//         ]
//       );
//       switch (
//         EActionCheck[
//           user_action_meta_data.actionSubType || user_action_meta_data.action
//         ]
//       ) {
//         case "checkForAccess":
//           if (rec_act_perm.value == 0) {
//             break;
//           } else {
//             let access_perm = await this.validateUserInputs(
//               request,
//               cur_form,
//               request_meta_data,
//               organization_id,
//               type,
//               token,
//               configuration,
//               fields
//             );
//             if (cur_form.meta_data.form_type == EFormType["sub-form"]) {
//               let p_source_id = this.getRequestDetails(
//                 request,
//                 request_meta_data.payloadType,
//                 request_meta_data.primaryRefAttrKey,
//                 "string"
//               );
//               let fitler_criteria: any = [
//                 { organization_id: mongoose.Types.ObjectId(organization_id) },
//               ];
//               let raw_filter: RawFilters = {
//                 organization_id: mongoose.Types.ObjectId(organization_id),
//               };
//               raw_filter["p_source_id"] = p_source_id;
//               raw_filter["p_source_type"] = cur_form.meta_data.parent_form_code;
//               if (request_meta_data.include_sub_form_filter) {
//                 if (request_meta_data.sub_form_source_type) {
//                   fitler_criteria.push({
//                     [request_meta_data.sub_form_source_type]:
//                       cur_form.meta_data.parent_form_code,
//                   });
//                 }
//                 if (request_meta_data.sub_form_source_id) {
//                   fitler_criteria.push({
//                     [request_meta_data.sub_form_source_id]: p_source_id,
//                   });
//                 }
//               }
//               result.filter_details = {
//                 fitlerCriteria: { $and: fitler_criteria },
//                 raw_filter,
//               };
//             }
//             result.has_access = access_perm;
//             break;
//           }
//         case "returnFilters":
//           if (rec_act_perm.value == 0 && !cur_form.meta_data.participant_view) {
//             break;
//           } else {
//             result.has_access =
//               rec_act_perm.value != 0
//                 ? true
//                 : cur_form.meta_data.participant_view
//                 ? true
//                 : false;

//             let rf_access_privilage: ResourceAccessDetail;
//             if (rec_act_perm.value == 0) {
//               rf_access_privilage = {
//                 allAccess: false,
//                 execQuery: true,
//                 applicable_ids: [token.id.toString()],
//                 applicable_customers: [],
//                 applicable_vendors: [],
//                 hasAccess: true,
//                 participant_view: true,
//               };
//             } else {
//               // console.log("inside else", cur_form);
//               rf_access_privilage = await this.checkResourceAccess(
//                 organization_id,
//                 user_id,
//                 rec_act_perm,
//                 "returnFilters",
//                 product_key
//               );
//             }
//             let participant_result = await this.checkParticipants(
//               cur_form.meta_data.participant_of,
//               token.id,
//               organization_id
//             );

//             let fitler_criteria: any = [
//               { organization_id: mongoose.Types.ObjectId(organization_id) },
//             ];
//             // console.log("filter criteria",fitler_criteria);
//             let user_filter: any = [];
//             if (request_meta_data && request_meta_data.skip_org_check) {
//               fitler_criteria.pop();
//             }
//             let raw_filter: RawFilters = {
//               organization_id: mongoose.Types.ObjectId(organization_id),
//               user_id: rf_access_privilage.applicable_ids,
//             };
//             if (filterPreference && filterPreference.need_branch_id) {
//               if (
//                 participant_result &&
//                 participant_result["branch"].scope < 4
//               ) {
//                 if (filterPreference.use_dyanmic_branch_fields == false) {
//                   if (
//                     participant_result["branch"].applicable_ids &&
//                     participant_result["branch"].applicable_ids.length
//                   ) {
//                     let is_multi_branch =
//                       participant_result["branch"].applicable_ids.length > 1;

//                     // console.log("is_multi_branch", is_multi_branch);
//                     let branch_or_cond = [];
//                     filterPreference.branch_variable_names.forEach((e) => {
//                       let cond = {
//                         [e]: is_multi_branch
//                           ? { $in: participant_result["branch"].applicable_ids }
//                           : participant_result["branch"].applicable_ids[0],
//                       };
//                       // console.log("branch cond is", cond);

//                       branch_or_cond.push(cond);
//                       raw_filter[e] = cond;
//                     });
//                     fitler_criteria.push({ $or: branch_or_cond });
//                     // console.log(
//                     //   "filter criteria is",
//                     //   JSON.stringify(fitler_criteria)
//                     // );
//                   }
//                 } else {
//                   console.log("inside else");

//                   let branch_reference = fields.filter(
//                     (e) => e.is_branch_reference
//                   );
//                   let primary_filter = [];
//                   branch_reference.forEach((e) => {
//                     let fil = {};
//                     if (
//                       participant_result["branch"].applicable_ids.length > 1
//                     ) {
//                       fil[e.field] = {
//                         $in: participant_result["branch"].applicable_ids,
//                       };
//                     } else {
//                       fil[e.field] =
//                         participant_result["branch"].applicable_ids[0];
//                     }
//                     primary_filter.push(fil);
//                   });
//                   fitler_criteria.push({ $or: primary_filter });
//                 }
//               }
//             }
//             if (filterPreference && filterPreference.need_warehouse_id) {
//               if (
//                 participant_result &&
//                 participant_result["warehouse"].scope < 4
//               ) {
//                 if (filterPreference.use_dyanmic_warehouse_fields == false) {
//                   if (
//                     participant_result["warehouse"].applicable_ids &&
//                     participant_result["warehouse"].applicable_ids.length
//                   ) {
//                     let is_multi_warehouse =
//                       participant_result["warehouse"].applicable_ids.length > 1;
//                     let warehouse_or_cond = [];
//                     filterPreference.warehouse_variable_names.forEach((e) => {
//                       let cond = {
//                         [e]: is_multi_warehouse
//                           ? {
//                               $in:
//                                 participant_result["warehouse"].applicable_ids,
//                             }
//                           : participant_result["warehouse"].applicable_ids[0],
//                       };
//                       warehouse_or_cond.push(cond);

//                       raw_filter[e] = cond;
//                     });
//                     fitler_criteria.push({ $or: warehouse_or_cond });
//                   }
//                 } else {
//                   let warehouse_reference = fields.filter(
//                     (e) => e.is_warehouse_reference
//                   );
//                   let primary_filter = [];
//                   warehouse_reference.forEach((e) => {
//                     let fil = {};
//                     if (
//                       participant_result["warehouse"].applicable_ids.length > 1
//                     ) {
//                       fil[e.field] = {
//                         $in: participant_result["warehouse"].applicable_ids,
//                       };
//                     } else {
//                       fil[e.field] =
//                         participant_result["warehouse"].applicable_ids[0];
//                     }
//                     primary_filter.push(fil);
//                   });
//                   fitler_criteria.push({ $or: primary_filter });
//                 }
//               }
//             }
//             if (filterPreference && filterPreference.need_customer_id) {
//               if (filterPreference.use_dyanmic_customer_fields == false) {
//                 if (
//                   rf_access_privilage.applicable_customers &&
//                   rf_access_privilage.applicable_customers.length
//                 ) {
//                   let is_multi_cust =
//                     rf_access_privilage.applicable_customers.length > 1;
//                   let cust_or_cond = [];
//                   filterPreference.customer_variable_names.forEach((e) => {
//                     let cond = {
//                       [e]: is_multi_cust
//                         ? { $in: rf_access_privilage.applicable_customers }
//                         : rf_access_privilage.applicable_customers[0],
//                     };
//                     // cust_or_cond.push(cond);
//                     user_filter.push(cond);
//                     raw_filter[e] = cond;
//                   });
//                   // fitler_criteria.push({ $or: cust_or_cond });
//                 }
//               } else {
//                 let customer_reference = fields.filter(
//                   (e) => e.is_customer_reference
//                 );
//                 let primary_filter = [];
//                 customer_reference.forEach((e) => {
//                   let fil = {};
//                   if (rf_access_privilage.applicable_customers.length > 1) {
//                     fil[e.field] = {
//                       $in: rf_access_privilage.applicable_customers,
//                     };
//                   } else {
//                     fil[e.field] = rf_access_privilage.applicable_customers[0];
//                   }
//                   user_filter.push(fil);
//                   // primary_filter.push(fil);
//                 });
//                 // fitler_criteria.push({ $or: primary_filter });
//               }
//             }
//             if (rf_access_privilage.applicable_ids.length) {
//               if (!filterPreference || filterPreference.need_user_id) {
//                 let user_reference = fields.filter((e) => e.is_user_reference);
//                 let primary_filter = [];
//                 user_reference.forEach((e) => {
//                   let fil = {};
//                   if (rf_access_privilage.applicable_ids.length > 1) {
//                     fil[e.field] = {
//                       $in: rf_access_privilage.applicable_ids,
//                     };
//                   } else {
//                     fil[e.field] = rf_access_privilage.applicable_ids[0];
//                   }
//                   user_filter.push(fil);
//                   // primary_filter.push(fil);
//                 });
//                 // fitler_criteria.push({ $or: primary_filter });
//               }
//             }
//             if (user_filter.length) {
//               fitler_criteria.push({ $or: user_filter });
//             }
//             // console.log("filter criteria", JSON.stringify(fitler_criteria));
//             if (cur_form.meta_data.form_type == EFormType["sub-form"]) {
//               let p_source_id = "";
//               if (request_meta_data.primaryRefAttrKey) {
//                 p_source_id = this.getRequestDetails(
//                   request,
//                   request_meta_data.payloadType,
//                   request_meta_data.primaryRefAttrKey,
//                   "string"
//                 );
//               }
//               if (p_source_id) raw_filter["p_source_id"] = p_source_id;
//               raw_filter["p_source_type"] = cur_form.meta_data.parent_form_code;
//             }
//             if (
//               rec_act_perm &&
//               rec_act_perm["_doc"].hasOwnProperty("action_criteria")
//             ) {
//               let act_criteria = rec_act_perm.action_criteria;
//               if (
//                 act_criteria &&
//                 act_criteria["_doc"].hasOwnProperty("filters")
//               ) {
//                 let defined_filters = rec_act_perm.action_criteria.filters;
//                 if (defined_filters && defined_filters.length) {
//                   defined_filters.forEach((e) => {
//                     raw_filter[e.key] = e.values;
//                     if (!e.condition) {
//                       fitler_criteria.push({
//                         [e.key]:
//                           e.values.length > 1 ? { $in: e.values } : e.values[0],
//                       });
//                     } else {
//                       fitler_criteria.push({
//                         [e.key]: { [e.condition]: e.values },
//                       });
//                     }
//                   });
//                 }
//               }
//             }
//             result.filter_details = {
//               allAccess: rf_access_privilage.allAccess,
//               execQuery: rf_access_privilage.execQuery,
//               applicableIds: rf_access_privilage.applicable_ids,
//               hasAccess: rf_access_privilage.hasAccess,
//               fitlerCriteria: { $and: fitler_criteria },
//               participant_view: rf_access_privilage.participant_view,
//               raw_filter,
//             };
//             break;
//           }
//         case "checkResourceAccess":
//           if (cur_form.meta_data.participant_view && rec_act_perm.value == 0) {
//             // console.log("inside if cur form", cur_form);

//             let member_exist = await this.groupMemberModel.findOne({
//               user_id: user_id,
//               source_id: this.getRequestDetails(
//                 request,
//                 request_meta_data.payloadType,
//                 request_meta_data.primaryAttrKey,
//                 "string"
//               ),
//               status: "Active",
//               acceptance_status: "Accepted",
//             });
//             result.has_access = member_exist ? true : false;
//           } else {
//             let resource_details = await this.getResourceDetails(
//               organization_id,
//               cur_form,
//               request,
//               request_meta_data,
//               type,
//               "primaryAttrKey",
//               fields,
//               filterPreference,
//               user_action_meta_data
//             );
//             // console.log("inside else cur form", cur_form);

//             let cra_access_privilage = await this.checkResourceAccess(
//               organization_id,
//               user_id,
//               rec_act_perm,
//               "CRA",
//               product_key,
//               resource_details.record_user_ids,
//               resource_details.multi,
//               resource_details.record_vendor_ids,
//               resource_details.record_customer_ids,
//               null,
//               null,
//               filterPreference
//             );
//             result.has_access = cra_access_privilage.hasAccess;
//           }
//           break;
//         default:
//           break;
//       }
//       return result;
//     } catch (err) {
//       throw err;
//     }
//   }
//   async checkParticipants(
//     participant_of: string[],
//     user_id: string,
//     organization_id: string
//   ) {
//     try {
//       let role = await this.roleAssignmentModel.findOne({
//         user_id: user_id,
//         organization_id: organization_id,
//       });
//       let branch_form = app_variables.default_branch_fcode;
//       let warehouse_form = app_variables.default_warehouse_fcode;
//       let data_permission = await this.dataPermissionModel.find({
//         source_id: role.role_id,
//         source_type: "ROLE",
//         organization_id: organization_id,
//         form_code: { $in: [branch_form, warehouse_form] },
//       });
//       // console.log("data permission is", data_permission);

//       let participant_value = {};
//       let branches = [];
//       let warehouses = [];
//       for (let i = 0; i < participant_of.length; i++) {
//         // console.log("participant of", participant_of[i], user_id);
//         if (participant_of[i] == "branch") {
//           // console.log("branches is", user_branch);

//           let branch_permission = data_permission.find(
//             (e) => e.form_code == branch_form
//           );
//           let check_activity = branch_permission.permissions.find(
//             (e1) => e1.activity == "view"
//           );
//           // console.log("check activity is", check_activity);

//           if (check_activity) {
//             if (check_activity.value == 0) {
//               let user_branch = await this.userBranch.find({
//                 user_id: user_id,
//                 organization_id: organization_id,
//               });
//               branches = user_branch.map((e) =>
//                 mongoose.Types.ObjectId(e.branch_id)
//               );
//             } else if (check_activity.value == 1) {
//               let branch_data = await this.branch.find({
//                 created_by: user_id,
//                 organization_id: organization_id,
//               });
//               branches = branch_data.map((e) => mongoose.Types.ObjectId(e._id));
//             }
//             participant_value[participant_of[i]] = {
//               scope: check_activity.value,
//               applicable_ids: branches,
//             };
//           } else {
//             participant_value[participant_of[i]] = {
//               scope: 0,
//               applicable_ids: [],
//             };
//           }
//         }
//         if (participant_of[i] == "warehouse") {
//           let warehouse_permission = data_permission.find(
//             (e) => e.form_code == warehouse_form
//           );
//           let check_activity = warehouse_permission.permissions.find(
//             (e1) => e1.activity == "view"
//           );
//           if (check_activity) {
//             if (check_activity.value == 0) {
//               let user_warehouse = await this.userWarehouse.find({
//                 user_id: user_id,
//                 organization_id: organization_id,
//               });
//               warehouses = user_warehouse.map((e) =>
//                 mongoose.Types.ObjectId(e.warehouse_id)
//               );
//             } else if (check_activity.value == 1) {
//               let warehouse_data = await this.warehouse.find({
//                 created_by: user_id,
//                 organization_id: organization_id,
//               });
//               warehouses = warehouse_data.map((e) =>
//                 mongoose.Types.ObjectId(e._id)
//               );
//             }
//             participant_value[participant_of[i]] = {
//               scope: check_activity.value,
//               applicable_ids: warehouses,
//             };
//           } else {
//             participant_value[participant_of[i]] = {
//               scope: 0,
//               applicable_ids: [],
//             };
//           }
//         }
//       }
//       // console.log("branch", branches, warehouses);
//       // console.log("participant_value is", participant_value);

//       return participant_value;
//     } catch (err) {
//       throw err;
//     }
//   }
// }

// export class RelatedFormDetails {
//   [key: string]: boolean | RelatedFormSpecificPermission;
// }

// export class RelatedFormSpecificPermission {
//   access: boolean;
//   scope: number;
// }
// export class RFPermission {
//   has_access: boolean;
//   action_access: RFActionAccess;
// }
// export class RFActionAccess {
//   [action: string]: boolean;
// }

// export class ContextVar {
//   organization_id: mongoose.Types.ObjectId;
//   user_id: mongoose.Types.ObjectId;
//   current_date_time: String | Date;
//   current_time_stamp: String | Date;
//   default_user_image: mongoose.Types.ObjectId;
//   default_post_image: mongoose.Types.ObjectId;
// }
// export class ResourceAccessDetail {
//   execQuery: boolean;
//   allAccess: boolean;
//   applicable_ids: string[];
//   applicable_customers: string[];
//   applicable_vendors: string[];
//   hasAccess: boolean;
//   participant_view?: boolean;
//   fitlerCriteria?: any;
// }
// export class RawFilters {
//   organization_id: mongoose.Types.ObjectId | string;
//   user_id?: any[];
//   p_source_id?: string;
//   p_source_type?: string;
//   [key: string]: any;
// }

// /**
//  *
//  * checkParticipant(participant_of:[], userid:stirng){
//  * 1. loop part op
//  * if branch - userbranch using userid
//  * if warehouse - userwarhouse using userid
//  *
//  * }
//  */
