export enum EActivityAttempt {
  login_failure_attempts = "login-failure-attempts",
  reset_password_email_trigger_attempts = "reset-password-email-trigger-attempts",
  reset_password_otp_validation_attempts = "reset-password-otp-validation-attempts",
  phone_number_email_trigger_attempts = "phone-number-email-trigger-attempts",
  phone_number_otp_validation_attempts = "phone-number-otp-validation-attempts",
  onboard_otp_failure_attempts = "onboard-otp-failure-attempts",
}

export const ESActivityAttempt = [
  EActivityAttempt.login_failure_attempts,
  EActivityAttempt.onboard_otp_failure_attempts,
  EActivityAttempt.phone_number_email_trigger_attempts,
  EActivityAttempt.phone_number_otp_validation_attempts,
  EActivityAttempt.reset_password_email_trigger_attempts,
  EActivityAttempt.reset_password_otp_validation_attempts,
];
