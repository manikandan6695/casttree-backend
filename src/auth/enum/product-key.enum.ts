export enum EProductKey {
  sourcing = "sourcing",
  accounts = "accounts",
  ecommerce = "ecommerce",
  social_network = "social-network",
}

export const ESProductKey = [
  EProductKey.accounts,
  EProductKey.ecommerce,
  EProductKey.sourcing,
  EProductKey.social_network,
];
