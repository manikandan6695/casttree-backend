import * as mongoose from "mongoose";
import { ESStatus, EStatus } from "src/shared/enum/privacy.enum";
import { EOTPChannel, ESOTPChannel } from "../otp/enum/otp-channel.enum";

export interface IOTPDetailModel extends mongoose.Document {
  otp_channel: EOTPChannel;
  action_type: string;
  delivered_to: string;
  opt_generated: number;
  status: EStatus;
  created_at: string | Date;
}

export const OTPDetailSchema = new mongoose.Schema(
  {
    otp_channel: {
      type: String,
      required: true,
      enum: ESOTPChannel,
    },
    action_type: {
      type: String,
      required: true,
    },
    delivered_to: {
      type: String,
      required: true,
    },
    opt_generated: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
      enum: ESStatus,
      default: EStatus.Active,
    },
  },
  {
    collection: "otpDetail",
    timestamps: { createdAt: "created_at" },
  }
);
