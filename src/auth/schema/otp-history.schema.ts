import * as mongoose from "mongoose";
import { EOTPChannel, ESOTPChannel } from "../otp/enum/otp-channel.enum";

export interface IOTPHistoryModel extends mongoose.Document {
  otp_channel: EOTPChannel;
  delivered_to: string;
  counter: number;
  last_triggered_at: string | Date;
}

export const OTPHistorySchema = new mongoose.Schema(
  {
    otp_channel: {
      type: String,
      required: true,
      enum: ESOTPChannel,
    },
    delivered_to: {
      type: String,
      required: true,
    },
    counter: {
      type: Number,
      required: true,
    },
    last_triggered_at: {
      type: Date,
      required: true,
    },
  },
  {
    collection: "otpHistory",
  }
);
