import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { PassportModule } from "@nestjs/passport";
import { LocalStrategy } from "./strategies/local.statergy";
import { JwtModule } from "@nestjs/jwt";
import { JwtStrategy } from "./strategies/jwt.strategy";
import { SharedModule } from "src/shared/shared.module";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "src/user/schema/user.schema";
import { ConfigurationSchema } from "src/configuration/schema/configuration.schema";
import { GoogleStrategy } from "./strategies/google.strategy";
import { FacebookStrategy } from "./strategies/facebook.strategy";
import { roleAssignmentSchema } from "src/role/schema/role-assignment.schema";
import { PreferenceSchema } from "src/preferences/schema/preferences.schema";
import { LoggerModule } from "src/logger/logger.module";
import { TimeZoneSchema } from "src/shared/schema/time-zone.schema";
import { SystemConfigurationSchema } from "src/configuration/schema/system-configuration.schema";
import { OtpService } from "./otp/otp.service";
// import { ServiceProviderHelperModule } from "src/service-provider-helper/service-provider-helper.module";
import { OTPHistorySchema } from "./schema/otp-history.schema";
import { OTPDetailSchema } from "./schema/otp-detail.schema";
import { roleSchema } from "src/role/schema/role.schema";
import { ServiceProviderHelperModule } from "src/service-provider-helper/service-provider-helper.module";
import { profileSchema } from "src/profile/schema/profile.schema";

@Module({
  imports: [
    PassportModule,
    LoggerModule,
    MongooseModule.forFeature([
      { name: "user", schema: UserSchema },
      { name: "roleAssignment", schema: roleAssignmentSchema },
      { name: "configuration", schema: ConfigurationSchema },
      { name: "role", schema: roleSchema },
      { name: "preference", schema: PreferenceSchema },
      { name: "timeZone", schema: TimeZoneSchema },
      { name: "system-configuration", schema: SystemConfigurationSchema },
      { name: "otp-history", schema: OTPHistorySchema },
      { name: "otp-detail", schema: OTPDetailSchema },
      { name: "profile", schema: profileSchema },
    ]),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: process.env.JWT_EXPIRATION },
    }),
    SharedModule,
    ServiceProviderHelperModule,
  ],
  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
    GoogleStrategy,
    FacebookStrategy,
    OtpService,
  ],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
