import { HttpStatus, Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectModel } from "@nestjs/mongoose";
import * as bcrypt from "bcrypt";
import { Model } from "mongoose";
import * as mongoose from "mongoose";
import { ISystemConfigurationModel } from "src/configuration/schema/system-configuration.schema";
import { AppException } from "src/shared/app-exception";
import { EStatus } from "src/shared/enum/privacy.enum";
import { SharedService } from "src/shared/shared.service";
import { UserSignUpDetails, UserSignUpDTO } from "src/user/dto/org-signup.dto";
import { UserToken } from "src/user/dto/usertoken.dto";
import { IUserModel } from "src/user/schema/user.schema";
import { EActivityAttempt } from "./enum/activity-attempt.enum";
import { OtpService } from "./otp/otp.service";
import { EOTPChannel } from "./otp/enum/otp-channel.enum";
import { EOTPActivity } from "./otp/enum/otp-activity.enum";
import { OTPLoginDTO } from "./otp/dto/otp-login.dto";
import { GenerateOTPDTO } from "./otp/dto/generate-otp.dto";
import { IRoleModel } from "src/role/schema/role.schema";
import { EVENT_PEER_TUBE_USER_CREATED } from "src/nominations/constants/constants";
import { IProfileModel } from "src/profile/schema/profile.schema";

/**
 * To Do
 * Facebook login and google login has to accept product key while login
 */
@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private shared_service: SharedService,
    @InjectModel("user") private user_model: Model<IUserModel>,
    @InjectModel("profile")
    private profileModel: Model<IProfileModel>,
    @InjectModel("system-configuration")
    private system_configuration_model: Model<ISystemConfigurationModel>,
    private otp_service: OtpService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    try {
      const user = await this.checkUser(username, pass);
      if (!user) return null;

      return user;
      //return null - unauthorized
      //return user - next go getnerate token
    } catch (err) {
      throw err;
    }
  }
  async checkUser(user_id: string, password: string): Promise<IUserModel> {
    try {
      let user = await this.checkUserExistance(user_id);
      if (!user) {
        throw new AppException(
          "Credentials not matched",
          HttpStatus.UNAUTHORIZED
        );
      }
      if (user.status == EStatus.Inactive) {
        throw new AppException("Forbidden", HttpStatus.FORBIDDEN);
      }

      let checkPassword = await bcrypt.compareSync(password, user.password);
      if (checkPassword) return user;
    } catch (err) {
      throw err;
    }
  }
  async loginWithOTP(
    body: OTPLoginDTO,
    channel: EOTPChannel,
    mode: EOTPActivity
  ) {
    try {
      let medium;
      let devliver_to;
      if (channel == EOTPChannel.phone) {
        medium = EOTPChannel.phone;
        devliver_to = `${body.phoneNumber}`;
      }
      if (channel == EOTPChannel.email) {
        medium = EOTPChannel.email;
        devliver_to = `${body.emailId}`;
      }

      let existing_user = await this.checkUserExistance(devliver_to);
      let profile;

      await this.otp_service.validateOTP(medium, mode, devliver_to, body.otp);
      let data;
      if (!existing_user) {
        let user_fv: any = {
          phoneCountryCode: body.phoneCountryCode,
          phoneNumber: body.phoneNumber,
          emailId: null,
          userName: null,
          gender: null,
          dateOfBirth: null,
          city: null,
          state: null,
          otp: body.otp,
        };
        let user = await this.user_model.create(user_fv);
        let peertubeUserFv = {
          ...body,
          userId: user._id.toString(),
          token: { id: user._id.toString() },
        };
        await this.shared_service.trackAndEmitEvent(
          EVENT_PEER_TUBE_USER_CREATED,
          peertubeUserFv,
          true,
          {
            userId: user._id.toString(),
            resourceUri: null,
            action: null,
          }
        );
        data = await this.generateToken(user);
        return data;
        // await this.signUp(user_fv, null, default_user_image, false);
      } else {
        data = await this.generateToken(existing_user);
        return data;
      }

      // }

      // let user = await this.checkUserExistance(devliver_to);
      // if (user.status == EStatus.Inactive) {
      //   throw new AppException(
      //     "Forbidden, Kindly contact the admin",
      //     HttpStatus.FORBIDDEN
      //   );
      // }
    } catch (err) {
      throw err;
    }
  }

  async checkUserExistance(unique_value: string) {
    try {
      let user = await this.user_model.findOne({ phoneNumber: unique_value });
      return user;
    } catch (err) {
      throw err;
    }
  }

  async getDuration(attempt: number, activity: EActivityAttempt) {
    let data = await this.system_configuration_model.findOne({ key: activity });
    let configuration_data = data.value;
    for (var i = 0; i < configuration_data.length; i++) {
      let ele = configuration_data[i];
      if (attempt >= ele.condition.gte && attempt <= ele.condition.lte) {
        ele.duration;
      }
    }
    return 0;
  }

  async generateToken(user: IUserModel) {
    try {
      let data = await this.user_model
        .findOne({ _id: user._id })
        .populate("city")
        .populate("state")
        .lean();
      let profile = await this.profileModel.findOne({
        userId: user._id,
      });

      const payload: UserToken = {
        id: user._id,
        userName: user.userName,
        phoneNumber: user?.phoneNumber,
        city: user?.city,
        state: user?.state,
      };
      return {
        access_token: this.jwtService.sign(payload),
        user: data,
        profile: profile || null,
        message: "Logged in successfully!",
      };
    } catch (err) {
      throw err;
    }
  }
  async getUserNameById(id) {
    try {
      let data = await this.user_model
        .findById(id)
        .select("first_name last_name");
      return data;
    } catch (err) {
      throw err;
    }
  }

  async signUp(
    body: UserSignUpDTO,
    language: string,
    default_user_image: string,
    skip_otp_validation: boolean = false
  ) {
    try {
      console.log("inside signup");

      // let medium;
      // let devliver_to;
      // if (channel == EOTPChannel.phone) {
      //   medium = EOTPChannel.phone;
      //   devliver_to = `${body.phone_country_code}${body.phone_number}`;
      // }
      // // if (channel == EOTPChannel.email) {
      // //   medium = EOTPChannel.email;
      // //   devliver_to = `${body.user_email}`;
      // // }
      // console.log("medium is", medium, devliver_to);

      // if (!skip_otp_validation)
      //   await this.otp_service.validateOTP(
      //     medium,
      //     EOTPActivity.login,
      //     devliver_to,
      //     body.otp
      //   );

      // body.password =
      //   "" + this.shared_service.generateRandomNumber(100000, 999999);

      // let filter = {};
      // if (body.user_email) {
      //   filter["user_email.email"] = body.user_email.toLowerCase();
      // } else if (body.phone_number) {
      //   filter["user_phone_number.phone_number"] = body.phone_number;
      // }
      // console.log("later filter is", filter);
      // const user = await this.user_model.findOne(filter);
      // if (user)
      //   throw new AppException("User already exist", HttpStatus.BAD_REQUEST);

      // let salt = bcrypt.genSaltSync(10);
      // const hashed = await bcrypt.hash(body.password, salt);
      // // let user_name = await this.getUserName(body.first_name);
      // let user_phone_number;
      // let user_email;
      // if (body.user_email) {
      //   user_email = {
      //     type: "primary",
      //     email: body.user_email.toLowerCase(),
      //   };
      // } else if (body.phone_number) {
      //   user_phone_number = {};
      // }

      let fv = {
        first_name: body.first_name,
        last_name: body.last_name,
        user_name: body.first_name,
      };
      if (body.user_media.length) {
        fv["media"] = body.user_media;
      } else {
        fv["media"] = [
          {
            type: "display_picture",
            media_id: default_user_image,
          },
        ];
      }
      let createdUser = await this.user_model.updateOne(
        { _id: body.id },
        { $set: fv }
      );

      let user = await this.user_model
        .findOne({ _id: body.id })
        .populate("media.media_id", "location file_name media_type")
        .lean();

      return { message: "Created Successfully", user };
    } catch (err) {
      throw err;
    }
  }

  async getUserName(name) {
    try {
      if (!name)
        throw new AppException(
          "Failed to generate user name",
          HttpStatus.NOT_ACCEPTABLE
        );
      let nextNumber = await this.shared_service.getNextNumber("userName");
      return name + nextNumber;
    } catch (err) {
      throw err;
    }
  }
  async getSystemConfig() {
    try {
      let data = await this.system_configuration_model.find();
      return data;
    } catch (err) {
      throw err;
    }
  }

  async validateOTPAction(
    body: GenerateOTPDTO,
    otp_channel_type: EOTPChannel,
    otp_activity_type: EOTPActivity
  ) {
    try {
      let user;
      if (otp_channel_type == EOTPChannel.phone) {
        user = await this.checkUserExistance(body.phoneNumber);
      }
      if (otp_channel_type == EOTPChannel.email) {
        user = await this.checkUserExistance(body.email);
      }
      let data;
      if (otp_channel_type == EOTPChannel.phone) {
        let is_new_user;
        if (user) {
          is_new_user = false;
          // console.log("inside user is");
        } else {
          is_new_user = true;
        }
        data = await this.otp_service.sendOTPThroughSMS(
          body,
          otp_activity_type,
          is_new_user
        );
      } else if (otp_channel_type == EOTPChannel.email) {
        data = await this.otp_service.sendOTPThroughEmail(
          body,
          otp_activity_type
        );
      }

      return data;
    } catch (err) {
      throw err;
    }
  }
}
export class UserVars {
  CUST?: string[];
  VEND?: string[];
  USR?: string[];
  ROLE?: string[];
}
