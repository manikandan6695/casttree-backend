import { HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ConfigService } from "@nestjs/config";
import { ServiceProviderHelperService } from "src/service-provider-helper/service-provider-helper.service";
import { AppException } from "src/shared/app-exception";
import { EStatus } from "src/shared/enum/privacy.enum";
import { SharedService } from "src/shared/shared.service";
import { EOTPChannel } from "./enum/otp-channel.enum";
import { IOTPDetailModel } from "../schema/otp-detail.schema";
import { IOTPHistoryModel } from "../schema/otp-history.schema";
import { GenerateOTPDTO } from "./dto/generate-otp.dto";
import { EOTPActivity, SIGNUP_OTP_MESSAGE } from "./enum/otp-activity.enum";
import { Logform } from "winston";

@Injectable()
export class OtpService {
  constructor(
    private service_provider_helper_service: ServiceProviderHelperService,
    private shared_service: SharedService,
    private configService: ConfigService,
    @InjectModel("otp-detail") private otp_detail_model: Model<IOTPDetailModel>,
    @InjectModel("otp-history")
    private otp_history_model: Model<IOTPHistoryModel>
  ) {}

  async sendOTPThroughSMS(
    data: GenerateOTPDTO,
    action_type: EOTPActivity,
    is_new_user: boolean
  ) {
    try {
      let code = this.shared_service.generateRandomNumber(1000, 9999);
      let delivered_to = data.extension + data.phoneNumber;
      let message_content = eval(SIGNUP_OTP_MESSAGE);
      if (process.env.NODE_ENV === "production") {
        await this.service_provider_helper_service.sendSystemSMS({
          message_content,
          phone_number: delivered_to,
          otp: code,
        });
      } else {
        code = 1111;
      }
      let phone_number = data.phoneNumber.toString();
      await this.inactivateOldOTP(EOTPChannel.phone, action_type, phone_number);

      let otpCreated = await this.otp_detail_model.create({
        otp_channel: EOTPChannel.phone,
        action_type,
        delivered_to: phone_number,
        opt_generated: code,
        created_at: new Date(),
      });
      console.log("otpCreated", otpCreated);

      await this.otp_history_model.findOneAndUpdate(
        {
          otp_channel: EOTPChannel.phone,
          delivered_to: phone_number,
        },
        {
          $inc: { counter: 1 },
          $set: { last_triggered_at: new Date() },
        },
        { upsert: true }
      );
      return { message: "OTP triggered successfully", is_new_user };
    } catch (err) {
      throw err;
    }
  }

  async sendOTPThroughEmail(data: GenerateOTPDTO, action_type: EOTPActivity) {
    try {
      let code = this.shared_service.generateRandomNumber(1000, 9999);
      let delivered_to = data.email;
      let template = this.configService.get("OTP_TEMPLATE");
      let subject = this.configService.get("OTP_SUBJECT");
      let description = this.configService.get("OTP_DESCRIPTION");
      let params = {
        email: delivered_to,
        otp: code,
        template: template,
        subject: subject,
        description: description,
      };

      // await this.service_provider_helper_service.sendSystemMail(params);
      await this.inactivateOldOTP(EOTPChannel.email, action_type, delivered_to);
      await this.otp_detail_model.create({
        otp_channel: EOTPChannel.email,
        action_type,
        delivered_to,
        opt_generated: code,
        created_at: new Date(),
      });
      await this.otp_history_model.findOneAndUpdate(
        {
          otp_channel: EOTPChannel.email,
          delivered_to,
        },
        {
          $inc: { counter: 1 },
          $set: { last_triggered_at: new Date() },
        },
        { upsert: true }
      );
      return { message: "OTP triggered successfully" };
    } catch (err) {
      throw err;
    }
  }

  async inactivateOldOTP(
    otp_channel: EOTPChannel,
    action_type: EOTPActivity,
    delivered_to: string
  ) {
    try {
      await this.otp_detail_model.updateMany(
        {
          otp_channel,
          action_type,
          delivered_to,
        },
        { status: EStatus.Inactive }
      );
    } catch (err) {
      throw err;
    }
  }

  async deleteOTP(
    otp_channel: EOTPChannel,
    action_type: EOTPActivity,
    delivered_to: string
  ) {
    try {
      await this.otp_detail_model.deleteMany({
        otp_channel,
        action_type,
        delivered_to,
      });
    } catch (err) {
      throw err;
    }
  }

  async validateOTP(
    otp_channel: EOTPChannel,
    action_type: EOTPActivity,
    delivered_to: string,
    input_otp: number
  ) {
    try {
      let active_otp = await this.otp_detail_model.findOne({
        otp_channel,
        action_type,
        delivered_to,
        status: EStatus.Active,
      });
      if (!active_otp)
        throw new AppException(
          "No active otp found! Kindly generate again",
          HttpStatus.NOT_FOUND
        );
      if (active_otp.opt_generated != input_otp) {
        throw new AppException("OTP not matched", HttpStatus.NOT_ACCEPTABLE);
      }
      await this.deleteOTP(otp_channel, action_type, delivered_to);

      return true;
    } catch (err) {
      throw err;
    }
  }
}
