export enum EOTPChannel {
  email = "Email",
  phone = "Phone",
}

export const ESOTPChannel = [EOTPChannel.email, EOTPChannel.phone];
