export enum EOTPActivity {
  signup = "sign-up",
  login = "login",
}

export const SIGNUP_OTP_MESSAGE =
  "`Your OTP to Sign in to Tecxprt Social App is ${code}. The OTP will expire in 15 Minutes.`";
