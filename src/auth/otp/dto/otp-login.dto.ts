import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateIf,
} from "class-validator";

export class OTPLoginDTO {
  @ValidateIf((o) => !o.email)
  @IsNotEmpty()
  @IsString()
  country: string;

  @ValidateIf((o) => !o.email)
  @IsNotEmpty()
  @IsString()
  phoneCountryCode: string;

  @ValidateIf((o) => !o.email)
  @IsNotEmpty()
  @IsString()
  phoneNumber: string;

  @ValidateIf((o) => !o.phone_number)
  @IsNotEmpty()
  @IsString()
  emailId: string;

  @IsNotEmpty()
  @IsNumber()
  otp: number;
}
