import { IsNotEmpty, IsOptional, IsString, ValidateIf } from "class-validator";

export class GenerateOTPDTO {
  @ValidateIf((o) => !o.email)
  @IsNotEmpty()
  @IsString()
  country: string;

  @ValidateIf((o) => !o.email)
  @IsNotEmpty()
  @IsString()
  extension: string;

  @ValidateIf((o) => !o.email)
  @IsNotEmpty()
  @IsString()
  phoneNumber: string;

  @ValidateIf((o) => !o.phoneNumber)
  @IsNotEmpty()
  @IsString()
  email: string;
}
