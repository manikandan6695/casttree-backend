import { Test, TestingModule } from '@nestjs/testing';
import { CtApiController } from './ct-api.controller';

describe('CtApiController', () => {
  let controller: CtApiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CtApiController],
    }).compile();

    controller = module.get<CtApiController>(CtApiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
