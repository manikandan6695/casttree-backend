import { Test, TestingModule } from '@nestjs/testing';
import { CtApiService } from './ct-api.service';

describe('CtApiService', () => {
  let service: CtApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CtApiService],
    }).compile();

    service = module.get<CtApiService>(CtApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
