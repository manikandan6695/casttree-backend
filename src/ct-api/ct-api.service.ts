import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { HttpService } from "@nestjs/axios";
import { SharedService } from "src/shared/shared.service";
import { PeerTubeUserDTO } from "./dto/peer-tube.dto";
import { UserToken } from "src/user/dto/usertoken.dto";
import { InjectModel } from "@nestjs/mongoose";
import { IUserModel } from "src/user/schema/user.schema";
import { Model } from "mongoose";
import { AppException } from "src/shared/app-exception";
const qs = require("qs");
@Injectable()
export class CtApiService {
  constructor(
    @InjectModel("user") private userModel: Model<IUserModel>,
    private http_service: HttpService,
    private configService: ConfigService
  ) {}

  async peerTubeTokenGeneration(
    body: PeerTubeUserDTO,
    password: string,
    token?: UserToken
  ) {
    try {
      let requestBody = {
        client_id: this.configService.get("client_id"),
        client_secret: this.configService.get("client_secret"),
        grant_type: body.grantType || "password",
      };
      if (body?.grantType === "refresh_token") {
        requestBody["refresh_token"] = body.refreshToken;
      } else {
        let user;
        if (token) {
          user = await this.userModel.findOne({ _id: token.id });
        }
        requestBody["username"] =
          body.username || `user${body.phoneNumber.trim()}@casttree.com`;
        requestBody["password"] =
          password || token.id.concat(user.created_at.getTime().toString());
      }
      let fv = qs.stringify(requestBody);
      try {
        let data = await this.http_service
          .post(
            `${this.configService.get("PEERTUBE_BASE_URL")}/api/v1/users/token`,
            fv,
            {
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
              },
            }
          )
          .toPromise();
        return data.data;
      } catch (err) {
        throw new AppException(err.response.statusText, err.response.status);
      }
    } catch (err) {
      throw err;
    }
  }
}
