import {
  Body,
  Controller,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { CtApiService } from "./ct-api.service";
import { Response } from "express";
import { PeerTubeUserDTO } from "./dto/peer-tube.dto";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { GetToken } from "src/shared/decorator/getuser.decorator";
import { UserToken } from "src/user/dto/usertoken.dto";

@Controller("peertube")
export class CtApiController {
  constructor(
    private sservice: SharedService,
    private ctService: CtApiService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post("token")
  async peerTubeTokenGeneration(
    @Res() res: Response,
    @GetToken() token: UserToken,
    @Body(new ValidationPipe({ whitelist: true })) body: PeerTubeUserDTO
  ) {
    try {
      let data = await this.ctService.peerTubeTokenGeneration(body, null, token);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
