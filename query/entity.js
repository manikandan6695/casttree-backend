db.entity.insertMany([
  {
    _id: ObjectId("601bd7cc275273ad9658dbe0"),
    entity_code: "ITM",
    entity_description: "Items entity",
    entity_type: "static",
    entity_collection_name: "item",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("601d2cb30062d25ce4b822f5"),
    entity_code: "AUC",
    entity_description: "Auction",
    entity_type: "static",
    entity_collection_name: "auction",
    is_system: true,
    status: "Active",
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("6024cd1d944e7b32ea01d83c"),
    entity_code: "AINV",
    entity_description: "Auction invitation",
    entity_type: "static",
    entity_collection_name: "groupMember",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("6035cdfc4f0231c43098ade0"),
    entity_code: "BID",
    entity_description: "Bid entity to store bids",
    entity_type: "static",
    entity_collection_name: "bid",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("605eb49d2136497906b8aa70"),
    entity_code: "MSG",
    entity_description: "Message schema",
    entity_type: "static",
    entity_collection_name: "message",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("606ee5e550ac49ce3aa7de17"),
    entity_code: "ARTCL",
    entity_description: "ARTICLE",
    entity_type: "static",
    entity_collection_name: "article",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("6070254ed9686f176f4db3e8"),
    entity_code: "CAT",
    entity_description: "category",
    entity_type: "static",
    entity_collection_name: "category",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("60750f1c619f86ecd778812e"),
    entity_code: "MDLPRMSN",
    entity_description: "module permission",
    entity_type: "static",
    entity_collection_name: "modulePermission",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
  {
    _id: ObjectId("608195387dcf07f02e095921"),
    entity_code: "ROOM",
    entity_description: "room collection",
    entity_type: "static",
    entity_collection_name: "room",
    status: "Active",
    is_system: true,
    created_by: null,
    updated_by: null,
  },
]);
