db.configuration.insertOne({
  _id: ObjectId("5fb24681adc68df07964f5d7"),
  organization_id: ObjectId("604895fea012922385731ed0"),
  configurations: {
    loginFailureAttempts: [
      {
        condition: {
          gte: 3,
          lte: 5,
        },
        duration: 900000,
      },
      {
        condition: {
          gte: 6,
          lte: 10,
        },
        duration: 7200000,
      },
      {
        condition: {
          gte: 11,
          lte: Infinity,
        },
        duration: 86400000,
      },
    ],
    resetPasswordEmailTrigger: [
      {
        condition: {
          gte: 3,
          lte: 5,
        },
        duration: 900000,
      },
      {
        condition: {
          gte: 6,
          lte: 10,
        },
        duration: 7200000,
      },
      {
        condition: {
          gte: 11,
          lte: Infinity,
        },
        duration: 86400000,
      },
    ],
    resetPasswordOTPValidation: [
      {
        condition: {
          gte: 3,
          lte: 5,
        },
        duration: 900000,
      },
      {
        condition: {
          gte: 6,
          lte: 10,
        },
        duration: 7200000,
      },
      {
        condition: {
          gte: 11,
          lte: Infinity,
        },
        duration: 86400000,
      },
    ],
    phoneNumberEmailTrigger: [
      {
        condition: {
          gte: 3,
          lte: 5,
        },
        duration: 900000,
      },
      {
        condition: {
          gte: 6,
          lte: 10,
        },
        duration: 7200000,
      },
      {
        condition: {
          gte: 11,
          lte: Infinity,
        },
        duration: 86400000,
      },
    ],
    phoneNumberOTPValidation: [
      {
        condition: {
          gte: 3,
          lte: 5,
        },
        duration: 900000,
      },
      {
        condition: {
          gte: 6,
          lte: 10,
        },
        duration: 7200000,
      },
      {
        condition: {
          gte: 11,
          lte: Infinity,
        },
        duration: 86400000,
      },
    ],
    onBoardOTPFailureAttempts: [
      {
        condition: {
          gte: 3,
          lte: 5,
        },
        duration: 900000,
      },
      {
        condition: {
          gte: 6,
          lte: 10,
        },
        duration: 7200000,
      },
      {
        condition: {
          gte: 11,
          lte: Infinity,
        },
        duration: 86400000,
      },
    ],
    user_prefix: "MAFIX",
    user_suffix: "BLR",
    user_preference: {
      time_zone: "GMT+5:30",
      currency: "INR",
      language: "en",
      date_time_format: "dd/mm/yyyy/hr/min/sec",
    },
    default_user_image: ObjectId("6018eb02c564f03cc08c4a32"),
    default_post_image: ObjectId("60212cced21fde97a019ed32"),
  },
  status: "Active",
});
