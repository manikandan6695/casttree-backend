db.user.insertOne({
  _id: ObjectId("60489c5ba012922385731ed5"),
  user_department: [],
  user_groups: [],
  first_name: [
    {
      description: "Master",
      language: "en",
    },
  ],
  last_name: [
    {
      description: "User",
      language: "en",
    },
  ],
  user_name: "MasterUser",
  user_id: "NOT0001BLR",
  user_email: {
    visibility: "Private",
    email: "developer@nextontop.com",
    addedOn: ISODate("2020-11-07T07:38:56.369Z"),
    verification: false,
  },
  user_phone_number: {
    type: "Primary",
    visibility: "Private",
    country: "IND",
    ext: "+91",
    phone_number: "9688338339",
    addedOn: ISODate("2020-11-07T07:38:56.372Z"),
    verification: false,
  },
  alternate_email: [],
  alternate_phone_number: [],
  user_address: [],
  user_password_history: [],
  user_about: [],
  user_media: [
    {
      visibility: "Public",
      type: "display_picture",
      media_id: ObjectId("6018eb02c564f03cc08c4a32"),
    },
  ],
  created_at: ISODate("2020-11-07T07:39:01.051Z"),
  updated_at: ISODate("2020-11-07T07:39:01.705Z"),
  password: "$2b$10$1Y8A9bfO/YePLeILm3k5meecKlZ3qnKQrUU/wT6u43/X5QsQWcYCO",
  user_password_secret: 278758,
  user_password_secret_expires: 1614852427845,
});
