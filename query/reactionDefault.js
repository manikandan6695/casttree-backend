db.reactionDefault.insertOne({
  _id: ObjectId("5ff6b510207179a8af08d779"),
  reaction_type: "Like",
  reaction_icon_default: "",
  reaction_icon_activated: "",
  reaction_description: "Like represent the reaction like towards the post",
  status: "Active",
  reaction_ui_detail: {
    default: "heart-outline",
    default_class: "",
    activated: "heart",
    activated_class: "likedactive",
  },
});
