db.scope.insertMany([
  {
    scope_id: 0,
    scope_description: "No Data",
    applicable_scope: [0],
  },
  {
    scope_id: 1,
    scope_description: "My Data",
    applicable_scope: [1, 0],
  },
  {
    scope_id: 2,
    scope_description: "Sub Data",
    applicable_scope: [2, 0],
  },
  {
    scope_id: 3,
    scope_description: "My Data and Sub Data",
    applicable_scope: [3, 2, 1, 0],
  },
  {
    scope_id: 4,
    scope_description: "All Data",
    applicable_scope: [4, 3, 2, 1, 0],
  },
]);
