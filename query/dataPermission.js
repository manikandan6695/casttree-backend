// super admin
db.dataPermission.insertMany([
  {
    _id: ObjectId("60800c3de767608119dfed93"),
    form_id: ObjectId("60750fc7619f86ecd7788130"),
    form_code: "MDLPRMSN",
    module_id: ObjectId("60750f85619f86ecd778812f"),
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 1,
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606170bbec015401063c6906")],
        },
      ],
    },
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("60586356a4bc87e891831042"),
    form_code: "DSHBRD",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("6034eb920724ddbe25186f8e"),
    form_code: "UINV",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("6035d9ac28a94f49cdfd68db"),
    form_code: "USR",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("601bd7e0275273ad9658dbe1"),
    form_code: "ITM",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("601d2cc70062d25ce4b822f6"),
    form_code: "AUC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "status",
              values: ["Active"],
            },
          ],
        },
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("60349b086cbc93c03406d386"),
    form_code: "VEND",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("603dcffb0d18707acf7be231"),
    form_code: "ORG",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("603dfc4883d841770ac5a710"),
    form_code: "ROLE",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("60473c39ad3cecc03d305911"),
    form_code: "UNIT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("60508214364ccc38dd6db7d3"),
    form_code: "SPT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    form_id: ObjectId("60599489c396f18fc12f21a2"),
    form_code: "TAG",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6035d0304f0231c43098ade1"),
    form_code: "ABID",
    permissions: [
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6906"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6024cfe9944e7b32ea01d83d"),
    form_code: "AINV",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: null,
      },
    ],
  },
  {
    _id: ObjectId("606eea3c50ac49ce3aa7de19"),
    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("5fd49b5d94e7211e3930434c"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("5fd49b5d94e7211e3930434c")],
        },
      ],
    },
  },
]);

// vendor permission
db.dataPermission.insertMany([
  {
    _id: ObjectId("606fed0bd9686f176f4db3e2"),
    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("606170bbec015401063c6907"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "tags",
              values: ["Notes", "Pivot"],
            },
          ],
        },
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606170bbec015401063c6907")],
        },
      ],
    },
  },
  {
    source_id: ObjectId("606170bbec015401063c6907"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6024cfe9944e7b32ea01d83d"),
    form_code: "AINV",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 1,
        action_criteria: null,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6907"),
    source_type: "ROLE",
    form_id: ObjectId("601bd7e0275273ad9658dbe1"),
    form_code: "ITM",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6907"),
    source_type: "ROLE",
    form_id: ObjectId("601d2cc70062d25ce4b822f6"),
    form_code: "AUC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 0,
        action_criteria: {
          filters: [
            {
              key: "status",
              values: ["Active"],
            },
          ],
        },
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6907"),
    source_type: "ROLE",
    form_id: ObjectId("60473c39ad3cecc03d305911"),
    form_code: "UNIT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6907"),
    source_type: "ROLE",
    form_id: ObjectId("60508214364ccc38dd6db7d3"),
    form_code: "SPT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 0,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6907"),
    source_type: "ROLE",
    form_id: ObjectId("605eb4ee2136497906b8aa71"),
    form_code: "STC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "message_type",
              values: ["conversation"],
            },
          ],
        },
      },
      {
        type: "record",
        activity: "create",
        value: 1,
      },
    ],
  },
]);

// auctioneer

db.dataPermission.insertMany([
  {
    _id: ObjectId("606fed0bd9686f176f4db3e3"),

    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("606170bbec015401063c6908"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "tags",
              values: ["Notes", "Pivot"],
            },
          ],
        },
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606170bbec015401063c6908")],
        },
      ],
    },
  },
  {
    source_id: ObjectId("606170bbec015401063c6908"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6024cfe9944e7b32ea01d83d"),
    form_code: "AINV",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: null,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c6908"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6035d0304f0231c43098ade1"),
    form_code: "ABID",
    permissions: [
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
]);

// bidder

db.dataPermission.insertMany([
  {
    source_id: ObjectId("606170bbec015401063c6909"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6035d0304f0231c43098ade1"),
    form_code: "ABID",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "bid_status",
              values: "Spam",
              condition: "$ne",
            },
          ],
        },
      },
    ],
  },
]);

// Admin
db.dataPermission.insertMany([
  {
    _id: ObjectId("60751224619f86ecd7788132"),
    form_id: ObjectId("60750fc7619f86ecd7788130"),
    form_code: "MDLPRMSN",
    module_id: ObjectId("60750f85619f86ecd778812f"),
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 1,
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606170bbec015401063c690a")],
        },
      ],
    },
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("601bd7e0275273ad9658dbe1"),
    form_code: "ITM",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
      {
        type: "record",
        activity: "create",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("601d2cc70062d25ce4b822f6"),
    form_code: "AUC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "status",
              values: ["Active"],
            },
          ],
        },
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("60349b086cbc93c03406d386"),
    form_code: "VEND",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("6034eb920724ddbe25186f8e"),
    form_code: "UINV",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("6035d9ac28a94f49cdfd68db"),
    form_code: "USR",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("603dcffb0d18707acf7be231"),
    form_code: "ORG",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("603dfc4883d841770ac5a710"),
    form_code: "ROLE",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("60473c39ad3cecc03d305911"),
    form_code: "UNIT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("60508214364ccc38dd6db7d3"),
    form_code: "SPT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 0,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("60586356a4bc87e891831042"),
    form_code: "DSHBRD",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("60599489c396f18fc12f21a2"),
    form_code: "TAG",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6024cfe9944e7b32ea01d83d"),
    form_code: "AINV",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: null,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    module_id: null,
    form_id: ObjectId("6035d0304f0231c43098ade1"),
    form_code: "ABID",
    permissions: [
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690a"),
    source_type: "ROLE",
    form_id: ObjectId("605eb4ee2136497906b8aa71"),
    form_code: "STC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
      {
        type: "record",
        activity: "create",
        value: 1,
      },
    ],
  },
  {
    _id: ObjectId("606eea3c50ac49ce3aa7de1a"),
    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("6034e92a0724ddbe25186f8c"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("6034e92a0724ddbe25186f8c")],
        },
      ],
    },
  },
]);

// module admin

db.dataPermission.insertMany([
  {
    _id: ObjectId("606fed0bd9686f176f4db3e5"),
    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("606170bbec015401063c690c"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "tags",
              values: ["Notes", "Pivot"],
            },
          ],
        },
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606170bbec015401063c690c")],
        },
      ],
    },
  },
  {
    source_id: ObjectId("606170bbec015401063c690c"),
    source_type: "ROLE",
    form_id: ObjectId("60473c39ad3cecc03d305911"),
    form_code: "UNIT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690c"),
    source_type: "ROLE",
    form_id: ObjectId("60349b086cbc93c03406d386"),
    form_code: "VEND",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690c"),
    source_type: "ROLE",
    form_id: ObjectId("601d2cc70062d25ce4b822f6"),
    form_code: "AUC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 1,
        action_criteria: {
          filters: [
            {
              key: "status",
              values: ["Active"],
            },
          ],
        },
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690c"),
    source_type: "ROLE",
    form_id: ObjectId("601bd7e0275273ad9658dbe1"),
    form_code: "ITM",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690c"),
    source_type: "ROLE",
    form_id: ObjectId("601bd7e0275273ad9658dbe1"),
    form_code: "ITM",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
]);

// master admin

db.dataPermission.insertMany([
  {
    _id: ObjectId("60751224619f86ecd7788131"),
    form_id: ObjectId("60750fc7619f86ecd7788130"),
    form_code: "MDLPRMSN",
    module_id: ObjectId("60750f85619f86ecd778812f"),
    source_id: ObjectId("604896c5a012922385731ed1"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 4,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("604896c5a012922385731ed1")],
        },
      ],
    },
  },
  {
    _id: ObjectId("606fde5ed9686f176f4db3dd"),

    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("606170bbec015401063c690b"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606170bbec015401063c690b")],
        },
      ],
    },
  },
  {
    source_id: ObjectId("606170bbec015401063c690b"),
    source_type: "ROLE",
    form_id: ObjectId("6048995fa012922385731ed3"),
    form_code: "MDLSTR",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690b"),
    source_type: "ROLE",
    form_id: ObjectId("604a0a977d48166165fdfe3a"),
    form_code: "FRM",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690b"),
    source_type: "ROLE",
    form_id: ObjectId("604b58d6e50924ef20ff16ff"),
    form_code: "DTPRM",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606170bbec015401063c690b"),
    source_type: "ROLE",
    form_id: ObjectId("60586356a4bc87e891831042"),
    form_code: "DSHBRD",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    form_id: ObjectId("605eb4ee2136497906b8aa71"),
    form_code: "STC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
      {
        type: "record",
        activity: "create",
        value: 1,
      },
    ],
    source_id: ObjectId("6034e92a0724ddbe25186f8c"),
    source_type: "ROLE",
  },
  {
    form_id: ObjectId("605eb4ee2136497906b8aa71"),
    form_code: "STC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "message_type",
              values: ["conversation"],
            },
          ],
        },
      },
      {
        type: "record",
        activity: "create",
        value: 1,
      },
    ],
    source_id: ObjectId("6023b3e40005a7d91e1b768b"),
    source_type: "ROLE",
  },
]);

// Support manager
db.dataPermission.insertMany([
  {
    _id: ObjectId("606fed0bd9686f176f4db3e6"),

    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("606708c59d5e40e63248de51"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "tags",
              values: ["Notes", "Pivot"],
            },
          ],
        },
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606708c59d5e40e63248de51")],
        },
      ],
    },
  },
  {
    form_id: ObjectId("60508214364ccc38dd6db7d3"),
    form_code: "SPT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 4,
      },
      {
        type: "record",
        activity: "delete",
        value: 0,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
    source_id: ObjectId("606708c59d5e40e63248de51"),
    source_type: "ROLE",
  },
  {
    form_id: ObjectId("605eb4ee2136497906b8aa71"),
    form_code: "STC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
      {
        type: "record",
        activity: "create",
        value: 1,
      },
    ],
    source_id: ObjectId("606708c59d5e40e63248de51"),
    source_type: "ROLE",
  },
  {
    source_id: ObjectId("606708c59d5e40e63248de51"),
    source_type: "ROLE",
    form_id: ObjectId("60586356a4bc87e891831042"),
    form_code: "DSHBRD",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606708c59d5e40e63248de51"),
    source_type: "ROLE",
    form_id: ObjectId("60599489c396f18fc12f21a2"),
    form_code: "TAG",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 0,
      },
      {
        type: "record",
        activity: "edit",
        value: 0,
      },
      {
        type: "record",
        activity: "delete",
        value: 0,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
]);

// data permission for support executive

db.dataPermission.insertMany([
  {
    _id: ObjectId("606fed0bd9686f176f4db3e7"),

    form_id: ObjectId("606ee62c50ac49ce3aa7de18"),
    form_code: "ARTCL",
    module_id: ObjectId("606ef404f38988d518a1c375"),
    source_id: ObjectId("606ab38f2703ea0f887b9522"),
    source_type: "ROLE",
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
        action_criteria: {
          filters: [
            {
              key: "tags",
              values: ["Notes", "Pivot"],
            },
          ],
        },
      },
    ],
    permission_criteria: {
      type: "matchAll",
      criteria: [
        {
          key: "role_id",
          value: [ObjectId("606ab38f2703ea0f887b9522")],
        },
      ],
    },
  },
  {
    form_id: ObjectId("60508214364ccc38dd6db7d3"),
    form_code: "SPT",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 4,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 0,
      },
      {
        type: "record",
        activity: "view",
        value: 1,
      },
    ],
    source_id: ObjectId("606ab38f2703ea0f887b9522"),
    source_type: "ROLE",
  },
  {
    form_id: ObjectId("605eb4ee2136497906b8aa71"),
    form_code: "STC",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "view",
        value: 4,
      },
      {
        type: "record",
        activity: "create",
        value: 1,
      },
    ],
    source_id: ObjectId("606ab38f2703ea0f887b9522"),
    source_type: "ROLE",
  },
  {
    source_id: ObjectId("606ab38f2703ea0f887b9522"),
    source_type: "ROLE",
    form_id: ObjectId("60586356a4bc87e891831042"),
    form_code: "DSHBRD",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 1,
      },
      {
        type: "record",
        activity: "edit",
        value: 1,
      },
      {
        type: "record",
        activity: "delete",
        value: 1,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
  {
    source_id: ObjectId("606ab38f2703ea0f887b9522"),
    source_type: "ROLE",
    form_id: ObjectId("60599489c396f18fc12f21a2"),
    form_code: "TAG",
    module_id: null,
    permissions: [
      {
        type: "record",
        activity: "create",
        value: 0,
      },
      {
        type: "record",
        activity: "edit",
        value: 0,
      },
      {
        type: "record",
        activity: "delete",
        value: 0,
      },
      {
        type: "record",
        activity: "view",
        value: 4,
      },
    ],
  },
]);
