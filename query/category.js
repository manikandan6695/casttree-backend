// for sports
db.category.insertOne({
  _id: ObjectId("5fd9dbc814c58e3e64aa4e74"),
  parent_category: null,
  tags: [],
  status: "Active",
  category_name: [
    {
      _id: ObjectId("5fd9dbc814c58e3e64aa4e75"),
      language: "en",
      name: "Sports",
    },
    {
      _id: ObjectId("5fd9dbc814c58e3e64aa4e76"),
      language: "ta",
      name: "மட்டைப்பந்து",
    },
  ],
  category_desc: [
    {
      _id: ObjectId("5fd9dbc814c58e3e64aa4e77"),
      language: "en",
      description: " All sports patent category",
    },
  ],
  category_characteristic: [
    {
      _id: ObjectId("5fd9dbc814c58e3e64aa4e78"),
      category_name: "SPORTS",
      category_value: 1,
    },
  ],
  category_icon: [
    {
      _id: ObjectId("5fd9dbc814c58e3e64aa4e79"),
      type: "primary",
      icon_class: "md-facebook",
    },
    {
      _id: ObjectId("5fd9dbc814c58e3e64aa4e7a"),
      type: "secondary",
      icon_class: "md-facebook-o",
    },
  ],
  media: null,
  created_at: ISODate("2020-12-16T10:04:56.537Z"),
  updated_at: ISODate("2020-12-16T10:04:56.537Z"),
  __v: 0,
});
// data
db.category.insertMany([
  {
    _id: ObjectId("5fd9de5d8804281d9cf4f414"),
    tags: [],
    status: "Active",
    category_name: [
      {
        _id: ObjectId("5fd9de5d8804281d9cf4f415"),
        language: "en",
        name: "Tennis",
      },
      {
        _id: ObjectId("5fd9de5d8804281d9cf4f416"),
        language: "ta",
        name: "மட்டைப்பந்து",
      },
    ],
    category_desc: [
      {
        _id: ObjectId("5fd9de5d8804281d9cf4f417"),
        language: "en",
        description: "Tennis tournament in nearby places",
      },
    ],
    parent_category: ObjectId("5fd9dbc814c58e3e64aa4e74"),
    category_characteristic: [
      {
        _id: ObjectId("5fd9de5d8804281d9cf4f418"),
        category_name: "Tennis",
        category_value: 3,
      },
    ],
    category_icon: [
      {
        _id: ObjectId("5fd9de5d8804281d9cf4f419"),
        type: "primary",
        icon_class: "md-facebook",
      },
      {
        _id: ObjectId("5fd9de5d8804281d9cf4f41a"),
        type: "secondary",
        icon_class: "md-facebook-o",
      },
    ],
    media: [
      {
        privacy: "public",
        _id: ObjectId("5fd9de5d8804281d9cf4f41b"),
        type: "primary",
        media_id: ObjectId("5fd9b780d42ebc297480b4bd"),
      },
      {
        privacy: "public",
        _id: ObjectId("5fd9de5d8804281d9cf4f41c"),
        type: "cover",
        media_id: ObjectId("5fd9b780d42ebc297480b4bd"),
      },
    ],
    created_at: ISODate("2020-12-16T10:15:57.138Z"),
    updated_at: ISODate("2020-12-16T10:15:57.138Z"),
    __v: 0,
  },
  {
    _id: ObjectId("5fd9de918804281d9cf4f41d"),
    tags: [],
    status: "Active",
    category_name: [
      {
        _id: ObjectId("5fd9de918804281d9cf4f41e"),
        language: "en",
        name: "Basketball",
      },
      {
        _id: ObjectId("5fd9de918804281d9cf4f41f"),
        language: "ta",
        name: "மட்டைப்பந்து",
      },
    ],
    category_desc: [
      {
        _id: ObjectId("5fd9de918804281d9cf4f420"),
        language: "en",
        description: "Basket  tournament in nearby places",
      },
    ],
    parent_category: ObjectId("5fd9dbc814c58e3e64aa4e74"),
    category_characteristic: [
      {
        _id: ObjectId("5fd9de918804281d9cf4f421"),
        category_name: "Basketball",
        category_value: 2,
      },
    ],
    category_icon: [
      {
        _id: ObjectId("5fd9de918804281d9cf4f422"),
        type: "primary",
        icon_class: "md-facebook",
      },
      {
        _id: ObjectId("5fd9de918804281d9cf4f423"),
        type: "secondary",
        icon_class: "md-facebook-o",
      },
    ],
    media: [
      {
        privacy: "public",
        _id: ObjectId("5fd9de918804281d9cf4f424"),
        type: "primary",
        media_id: ObjectId("5fd9b6ccd42ebc297480b4b2"),
      },
      {
        privacy: "public",
        _id: ObjectId("5fd9de918804281d9cf4f425"),
        type: "cover",
        media_id: ObjectId("5fd9b6ccd42ebc297480b4b2"),
      },
    ],
    created_at: ISODate("2020-12-16T10:16:49.785Z"),
    updated_at: ISODate("2020-12-16T10:16:49.785Z"),
    __v: 0,
  },
  {
    _id: ObjectId("5fd9deaa8804281d9cf4f426"),
    tags: [],
    status: "Active",
    category_name: [
      {
        _id: ObjectId("5fd9deaa8804281d9cf4f427"),
        language: "en",
        name: "Cricket",
      },
      {
        _id: ObjectId("5fd9deaa8804281d9cf4f428"),
        language: "ta",
        name: "மட்டைப்பந்து",
      },
    ],
    category_desc: [
      {
        _id: ObjectId("5fd9deaa8804281d9cf4f429"),
        language: "en",
        description: "cricket tournament in nearby places",
      },
    ],
    parent_category: ObjectId("5fd9dbc814c58e3e64aa4e74"),
    category_characteristic: [
      {
        _id: ObjectId("5fd9deaa8804281d9cf4f42a"),
        category_name: "Cricket",
        category_value: 1,
      },
    ],
    category_icon: [
      {
        _id: ObjectId("5fd9deaa8804281d9cf4f42b"),
        type: "primary",
        icon_class: "md-facebook",
      },
      {
        _id: ObjectId("5fd9deaa8804281d9cf4f42c"),
        type: "secondary",
        icon_class: "md-facebook-o",
      },
    ],
    media: [
      {
        privacy: "public",
        _id: ObjectId("5fd9deaa8804281d9cf4f42d"),
        type: "primary",
        media_id: ObjectId("5fd9b5b3d42ebc297480b4a7"),
      },
      {
        privacy: "public",
        _id: ObjectId("5fd9deaa8804281d9cf4f42e"),
        type: "cover",
        media_id: ObjectId("5fd9b5b3d42ebc297480b4a7"),
      },
    ],
    created_at: ISODate("2020-12-16T10:17:14.256Z"),
    updated_at: ISODate("2020-12-16T10:17:14.256Z"),
    __v: 0,
  },
  {
    _id: ObjectId("5fdb341c04b79d3e84f22418"),
    tags: [],
    is_system: false,
    status: "Active",
    category_name: [
      {
        _id: ObjectId("5fdb341c04b79d3e84f22419"),
        language: "en",
        name: "Cricket",
      },
      {
        _id: ObjectId("5fdb341c04b79d3e84f2241a"),
        language: "ta",
        name: "மட்டைப்பந்து",
      },
    ],
    category_desc: [
      {
        _id: ObjectId("5fdb341c04b79d3e84f2241b"),
        language: "en",
        description: "cricket tournament in nearby places",
      },
    ],
    parent_category: ObjectId("5fd9dbc814c58e3e64aa4e74"),
    category_characteristic: [
      {
        _id: ObjectId("5fdb341c04b79d3e84f2241c"),
        category_name: "Cricket",
        category_value: 1,
      },
    ],
    category_icon: [
      {
        _id: ObjectId("5fdb341c04b79d3e84f2241d"),
        type: "primary",
        icon_class: "md-facebook",
      },
      {
        _id: ObjectId("5fdb341c04b79d3e84f2241e"),
        type: "secondary",
        icon_class: "md-facebook-o",
      },
    ],
    media: [
      {
        privacy: "public",
        _id: ObjectId("5fdb341c04b79d3e84f2241f"),
        type: "primary",
        media_id: ObjectId("5fd9b5b3d42ebc297480b4a7"),
      },
      {
        privacy: "public",
        _id: ObjectId("5fdb341c04b79d3e84f22420"),
        type: "cover",
        media_id: ObjectId("5fd9b5b3d42ebc297480b4a7"),
      },
    ],
    created_by: ObjectId("5fcc66e2e9bbcf072bf8f3bb"),
    organization_id: ObjectId("5fa64f158e53183a76ff6dec"),
    created_at: ISODate("2020-12-17T10:34:04.875Z"),
    updated_at: ISODate("2020-12-17T10:34:04.875Z"),
    __v: 0,
  },
  {
    _id: ObjectId("5fdc4f4d43a3a4217c7d6f35"),
    tags: [],
    is_system: false,
    status: "Active",
    category_name: [
      {
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f36"),
        language: "en",
        name: "hockey",
      },
      {
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f37"),
        language: "ta",
        name: "மட்டைப்பந்து",
      },
    ],
    category_desc: [
      {
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f38"),
        language: "en",
        description: "hockey tournament in nearby places",
      },
    ],
    parent_category: ObjectId("5fd9dbc814c58e3e64aa4e74"),
    category_characteristic: [
      {
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f39"),
        category_name: "hockey",
        category_value: 3,
      },
    ],
    category_icon: [
      {
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f3a"),
        type: "primary",
        icon_class: "md-facebook",
      },
      {
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f3b"),
        type: "secondary",
        icon_class: "md-facebook-o",
      },
    ],
    media: [
      {
        privacy: "public",
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f3c"),
        type: "primary",
        media_id: ObjectId("5fd9b6ccd42ebc297480b4b2"),
      },
      {
        privacy: "public",
        _id: ObjectId("5fdc4f4d43a3a4217c7d6f3d"),
        type: "cover",
        media_id: ObjectId("5fd9b5b3d42ebc297480b4a7"),
      },
    ],
    created_by: ObjectId("5fcc66e2e9bbcf072bf8f3bb"),
    organization_id: ObjectId("5fa64f158e53183a76ff6dec"),
    created_at: ISODate("2020-12-18T06:42:21.211Z"),
    updated_at: ISODate("2020-12-18T07:05:11.969Z"),
    __v: 0,
    updated_by: ObjectId("5fcc66e2e9bbcf072bf8f3bb"),
  },
  {
    _id: ObjectId("5fe2cc03bdeb5b33bc60382a"),
    tags: [],
    is_system: false,
    status: "Active",
    category_name: [
      {
        _id: ObjectId("5fe2cc03bdeb5b33bc60382b"),
        language: "en",
        name: "Football",
      },
      {
        _id: ObjectId("5fe2cc03bdeb5b33bc60382c"),
        language: "ta",
        name: "மட்டைப்பந்து",
      },
    ],
    category_desc: [
      {
        _id: ObjectId("5fe2cc03bdeb5b33bc60382d"),
        language: "en",
        description: "football tournament in nearby places",
      },
    ],
    parent_category: ObjectId("5fd9dbc814c58e3e64aa4e74"),
    category_characteristic: [
      {
        _id: ObjectId("5fe2cc03bdeb5b33bc60382e"),
        category_name: "Football",
        category_value: 1,
      },
    ],
    category_icon: [
      {
        _id: ObjectId("5fe2cc03bdeb5b33bc60382f"),
        type: "primary",
        icon_class: "md-facebook",
      },
      {
        _id: ObjectId("5fe2cc03bdeb5b33bc603830"),
        type: "secondary",
        icon_class: "md-facebook-o",
      },
    ],
    media: [
      {
        privacy: "public",
        _id: ObjectId("5fe2cc03bdeb5b33bc603831"),
        type: "primary",
        media_id: ObjectId("5fd9b5b3d42ebc297480b4a7"),
      },
      {
        privacy: "public",
        _id: ObjectId("5fe2cc03bdeb5b33bc603832"),
        type: "cover",
        media_id: ObjectId("5fd9b5b3d42ebc297480b4a7"),
      },
    ],
    created_by: ObjectId("5fcc66e2e9bbcf072bf8f3bb"),
    organization_id: ObjectId("5fa64f158e53183a76ff6dec"),
    created_at: ISODate("2020-12-23T04:48:03.351Z"),
    updated_at: ISODate("2020-12-23T04:48:03.351Z"),
    __v: 0,
  },
]);
