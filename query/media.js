//primary
db.media.insertOne({
  _id: ObjectId("6018eb02c564f03cc08c4a32"),
  privacy: "public-read",
  status: "Active",
  created_by: ObjectId("6013d9a73e0bf6671c85c486"),
  media_type: "image",
  media_format: "jpeg",
  media_size: "30713",
  media_url:
    "https://mafix.nyc3.digitaloceanspaces.com/public-read/1612786756339image%20%2814%29.png",
  location:
    "https://mafix.nyc3.digitaloceanspaces.com/public-read/1612786756339image%20%2814%29.png",
  file_name: "1612245766354profile_default.jpg",
  description: [
    {
      status: "Active",
      _id: ObjectId("6018eb02c564f03cc08c4a33"),
      content: "Profile image",
      version: "1",
      created_at: ISODate("2021-02-02T06:02:42.598Z"),
      updated_at: ISODate("2021-02-02T06:02:42.598Z"),
    },
  ],
  created_at: ISODate("2021-02-02T06:02:42.599Z"),
  updated_at: ISODate("2021-02-02T06:02:42.599Z"),
  __v: 0,
});

//post image
db.media.insertOne({
  _id: ObjectId("60212cced21fde97a019ed32"),
  privacy: "public-read",
  status: "Active",
  created_by: ObjectId("601be43e5102413871ce5d23"),
  media_type: "image",
  media_format: "png",
  media_size: "16450",
  media_url:
    "https://mafix.nyc3.digitaloceanspaces.com/public-read/1612786898319default-post-icon.png",
  location:
    "https://mafix.nyc3.digitaloceanspaces.com/public-read/1612786898319default-post-icon.png",
  file_name: "1612786898319default-post-icon.png",
  description: [
    {
      status: "Active",
      _id: ObjectId("60212cced21fde97a019ed33"),
      content: "",
      version: "",
      created_at: ISODate("2021-02-08T12:21:34.483Z"),
      updated_at: ISODate("2021-02-08T12:21:34.483Z"),
    },
  ],
  created_at: ISODate("2021-02-08T12:21:34.483Z"),
  updated_at: ISODate("2021-02-08T12:21:34.483Z"),
  __v: 0,
});
